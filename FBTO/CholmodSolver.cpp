#include "CholmodSolver.h"
#include <iostream>
#ifdef WITH_CHOLMOD
#include <Eigen/CholmodSupport>

namespace FBTO {
struct CholmodEnv {
  CholmodEnv() {
    cholmod_start(&_c);
  }
  ~CholmodEnv() {
    cholmod_finish(&_c);
  }
  cholmod_common _c;
};
CholmodEnv env;
//CholmodSolver
CholmodSolver::CholmodSolver(const SMatT& A):_L(NULL) {
  compute(A);
}
CholmodSolver::CholmodSolver():_L(NULL) {}
CholmodSolver::~CholmodSolver() {
  clear();
}
std::shared_ptr<CholmodSolver> CholmodSolver::copy() const {
  std::shared_ptr<CholmodSolver> sol(new CholmodSolver);
#ifdef DEBUG_REFERENCE_SOLVER
  sol->_A0=_A0;
#endif
  sol->_info=_info;
  sol->_L=cholmod_copy_factor(_L,&env._c);
  return sol;
}
void CholmodSolver::compute(const SMatT& A) {
  clear();
  //allocate
#ifdef FORCE_ADD_DOUBLE_PRECISION
  Eigen::SparseMatrix<double,0,int> ADouble=A.cast<double>();
  cholmod_sparse AChol=Eigen::viewAsCholmod(const_cast<const Eigen::SparseMatrix<double,0,int>&>(ADouble).template selfadjointView<1>());
#else
  cholmod_sparse AChol=Eigen::viewAsCholmod(A.template selfadjointView<1>());
#endif
  //symbolic factorize
  _L=cholmod_analyze(&AChol,&env._c);
  if(env._c.status!=CHOLMOD_OK) {
    _info=Eigen::InvalidInput;
    return;
  }
  //numerical factorize
  double beta[2]= {0,0};
  cholmod_factorize_p(&AChol,beta,NULL,0,_L,&env._c);
  if(env._c.status!=CHOLMOD_OK) {
    std::cout << "Cholmod return code=" << env._c.status << std::endl;
    _info=Eigen::NumericalIssue;
    return;
  }
  _info=Eigen::Success;
#ifdef DEBUG_REFERENCE_SOLVER
  _A0=A;
#endif
}
CholmodSolver::MatT CholmodSolver::solve(const MatT& b) const {
#ifdef FORCE_ADD_DOUBLE_PRECISION
  Eigen::Matrix<double,-1,-1> bDouble=b.cast<double>();
  cholmod_dense B=Eigen::viewAsCholmod(bDouble);
#else
  MatT bTmp=b;
  cholmod_dense B=Eigen::viewAsCholmod(bTmp);
#endif
  cholmod_dense* X=cholmod_solve(CHOLMOD_A,_L,&B,(cholmod_common*)&env._c);
  MatT ret=Eigen::Map<Eigen::Matrix<double,-1,-1>>((double*)X->x,B.nrow,B.ncol).cast<T>();
  ASSERT_MSG(env._c.status==CHOLMOD_OK,"Cholmod solve failed!")
  cholmod_free_dense(&X,(cholmod_common*)&env._c);
  return ret;
}
Eigen::ComputationInfo CholmodSolver::info() const {
  return _info;
}
void CholmodSolver::clear() {
  if(_L)
    cholmod_free_factor(&_L,&env._c);
  _L=NULL;
}
}

#endif
