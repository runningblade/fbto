#ifndef TIMING_H
#define TIMING_H

#include <iostream>

namespace FBTO {
//timing
extern void saveTimingSetting();
extern void loadTimingSetting();
extern void enableTiming();
extern void disableTiming();

extern void TBEG();
extern void TBEG(const std::string& name);
extern void TEND(std::ostream& os);
extern void TEND();
extern double TENDVPeek();
extern double TENDV();
}

#endif
