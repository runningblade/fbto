#include "KnitroSolver.h"
#ifdef KNITRO_SUPPORT
#include "Timing.cuh"

namespace FBTO {
template <int DIM,int POWRHO>
SQPSolver<DIM,POWRHO>::SQPSolver(T lambda,T mu,T minFill,T sumFillRel,int x,int y,int z)
  :KNProblem(x*y*z,1),_sol(lambda,mu,minFill,sumFillRel,x,y,z) {}
template <int DIM,int POWRHO>
int SQPSolver<DIM,POWRHO>::solve(std::function<VecDT(Eigen::Matrix<int,3,1>)> force,Vec& xC) {
  _f=Vec::Zero(_sol.cols());
  for(int x=0; x<_sol.xU(); x++)
    for(int y=0; y<_sol.yU(); y++)
      for(int z=0; z<_sol.zU(); z++)
        _f.template segment<DIM>(_sol.strideU().dot(Eigen::Matrix<int,3,1>(x,y,z))*DIM)=force(Eigen::Matrix<int,3,1>(x,y,z));
  initProblem();
  //Create a solver
  knitro::KNSolver solver(this);
  solver.setParam(KN_PARAM_CG_MAXIT,50);
  solver.setParam(KN_PARAM_MAXIT,_sol.maxIter());
  solver.setParam(KN_PARAM_HESSOPT,KN_HESSOPT_BFGS);
  solver.setParam(KN_PARAM_ALGORITHM,KN_ALG_BAR_CG);
  solver.setParam(KN_PARAM_DERIVCHECK,KN_DERIVCHECK_NONE);
  solver.setParam(KN_PARAM_BAR_FEASIBLE,KN_BAR_FEASIBLE_GET_STAY);
  //Solve
  solver.initProblem();
  solver.setUserParams(this);
  TBEG("SQP-Solve");
  int solveStatus=solver.solve();
  TEND();
  std::vector<double> x;
  std::vector<double> lambda;
  solver.getSolution(x,lambda);
  xC=_sol.filterForward(Eigen::Map<const Vec>(x.data(),x.size()));
  return solveStatus;
}
template <int DIM,int POWRHO>
FBTOSolver<DIM,POWRHO>& SQPSolver<DIM,POWRHO>::sol() {
  return _sol;
}
template <int DIM,int POWRHO>
int SQPSolver<DIM,POWRHO>::callbackEvalF
(KN_context_ptr             kc,
 CB_context_ptr             cb,
 KN_eval_request_ptr const  evalRequest,
 KN_eval_result_ptr  const  evalResult,
 void              * const  userParams) {
  const double *x;
  double *obj;
  if(evalRequest->type!=KN_RC_EVALFC) {
    printf("*** callbackEvalF incorrectly called with eval type %d\n",evalRequest->type);
    return -1;
  }
  x=evalRequest->x;
  obj=evalResult->obj;
  SQPSolver<DIM,POWRHO>* sol=reinterpret_cast<SQPSolver<DIM,POWRHO>*>(userParams);
  computeVar(sol,Eigen::Map<const Eigen::Matrix<double,-1,1>>(x,sol->_sol.colsFill()).template cast<T>());
  *obj=sol->_u.dot(sol->_f)/2;
  return 0;
}
template <int DIM,int POWRHO>
int SQPSolver<DIM,POWRHO>::callbackEvalG
(KN_context_ptr             kc,
 CB_context_ptr             cb,
 KN_eval_request_ptr const  evalRequest,
 KN_eval_result_ptr  const  evalResult,
 void              * const  userParams) {
  const double *x;
  double *objGrad;
  if(evalRequest->type!=KN_RC_EVALGA) {
    printf("*** callbackEvalG incorrectly called with eval type %d\n",
           evalRequest->type);
    return -1;
  }
  x=evalRequest->x;
  objGrad=evalResult->objGrad;
  SQPSolver<DIM,POWRHO>* sol=reinterpret_cast<SQPSolver<DIM,POWRHO>*>(userParams);
  computeVar(sol,Eigen::Map<const Eigen::Matrix<double,-1,1>>(x,sol->_sol.colsFill()).template cast<T>());
  Eigen::Map<Eigen::Matrix<double,-1,1>>(objGrad,sol->_sol.colsFill())=sol->_diff.template cast<double>();
  return 0;
}
template <int DIM,int POWRHO>
int SQPSolver<DIM,POWRHO>::callbackEvalH
(KN_context_ptr             kc,
 CB_context_ptr             cb,
 KN_eval_request_ptr const  evalRequest,
 KN_eval_result_ptr  const  evalResult,
 void              * const  userParams) {
  if(evalRequest->type != KN_RC_EVALH && evalRequest->type != KN_RC_EVALH_NO_F) {
    printf("*** callbackEvalH incorrectly called with eval type %d\n",
           evalRequest->type);
    return -1;
  }
  return 0;
}
template <int DIM,int POWRHO>
void SQPSolver<DIM,POWRHO>::computeVar(SQPSolver<DIM,POWRHO>* sol,const Vec& x) {
  if(sol->_xV.size()!=x.size() || sol->_xV!=x) {
    sol->_xV=x;
    sol->_xC=sol->_sol.filterForward(sol->_xV);
    sol->_u=Vec::Zero(sol->_sol.cols());
    sol->_sol.solveK(sol->_u,sol->_xC,sol->_f);
    sol->_diffC=-sol->_sol.diffK(sol->_xC,sol->_u,VecDT::Zero());
    sol->_diff=sol->_sol.filterBackward(sol->_diffC);
  }
}
template <int DIM,int POWRHO>
void SQPSolver<DIM,POWRHO>::initProblem() {
  //Variables
  std::vector<double> lbv,ubv,init;
  for(int x=0; x<_sol.x(); x++)
    for(int y=0; y<_sol.y(); y++)
      for(int z=0; z<_sol.z(); z++) {
        if(_sol.isFMasked(x,y,z)) {
          lbv.push_back(0);
          ubv.push_back(0);
          init.push_back(0);
        } else {
          lbv.push_back(_sol.minFill());
          ubv.push_back(1);
          init.push_back(_sol.sumFill()/_sol.nrValidCell());
        }
      }
  this->setVarLoBnds(lbv);
  this->setVarUpBnds(ubv);
  this->setXInitial(init);
  //total constraint
  std::vector<int> vss;
  std::vector<double> css;
  for(int i=0; i<_sol.colsFill(); i++) {
    vss.push_back(i);
    css.push_back(1);
  }
  KNLinearStructure lc(vss,css);
  this->setConUpBnds({{_sol.sumFill()}});
  this->getConstraintsLinearParts().add(0,lc);
  //Evaluations callbacks
  this->setObjEvalCallback(&SQPSolver::callbackEvalF);
  this->setGradEvalCallback(&SQPSolver::callbackEvalG);
  this->setHessEvalCallback(&SQPSolver::callbackEvalH);
}
//instance
template class SQPSolver<2,1>;
template class SQPSolver<2,2>;
template class SQPSolver<2,3>;
template class SQPSolver<2,4>;
template class SQPSolver<3,1>;
template class SQPSolver<3,2>;
template class SQPSolver<3,3>;
template class SQPSolver<3,4>;
}
#endif
