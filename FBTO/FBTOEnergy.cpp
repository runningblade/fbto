#include "FBTOEnergy.h"

namespace FBTO {
template <int DIM>
FBTOEnergy<DIM>::FBTOEnergy(T lambda,T mu,int deg):_lambda(lambda),_mu(mu),_deg(deg) {}
template <int DIM>
typename FBTOEnergy<DIM>::MatDT FBTOEnergy<DIM>::F(const Vec3T& pos,const VecDDT& v) const {
#define VD(I) v.template segment<DIM>(I*DIM)
  T coefs[1<<DIM];
  MatDT ret;
  if(DIM==2) {
    stencil2DGradXStencil(coefs,pos[0],pos[1]);
    for(int d=0; d<(1<<DIM); d++)
      ret.col(0)+=VD(d)*coefs[d];
    stencil2DGradYStencil(coefs,pos[0],pos[1]);
    for(int d=0; d<(1<<DIM); d++)
      ret.col(1)+=coefs[d]*VD(d);
  } else {
    stencil3DGradXStencil(coefs,pos[0],pos[1],pos[2]);
    for(int d=0; d<(1<<DIM); d++)
      ret.col(0)+=coefs[d]*VD(d);
    stencil3DGradYStencil(coefs,pos[0],pos[1],pos[2]);
    for(int d=0; d<(1<<DIM); d++)
      ret.col(1)+=coefs[d]*VD(d);
    stencil3DGradZStencil(coefs,pos[0],pos[1],pos[2]);
    for(int d=0; d<(1<<DIM); d++)
      ret.col(2)+=coefs[d]*VD(d);
  }
  return ret;
#undef VD
}
template <int DIM>
typename FBTOEnergy<DIM>::AD FBTOEnergy<DIM>::E(const Vec3T& pos,const VecDDT& v) const {
  MatDT f=F(pos,v),eps=(f+f.transpose())/2-MatDT::Identity();
  return _mu*eps.squaredNorm()+_lambda/2*eps.trace()*eps.trace();
}
template <int DIM>
typename FBTOEnergy<DIM>::AD FBTOEnergy<DIM>::EI(const VecDDT& v) const {
  AD ret;
  Eigen::Matrix<AD,1<<DIM,1> bary;
  std::function<AD(Vec3T)> f=[&](Vec3T ret)->AD {
    ret=(ret+Vec3T::Ones())*0.5f;
    return E(ret,v);
  };
  if(DIM==2)
    GaussLegendreIntegral<AD,T>::integrate2D(f,_deg,ret);
  else GaussLegendreIntegral<AD,T>::integrate3D(f,_deg,ret);
  ret/=T(1<<DIM);
  return ret;
}
template <int DIM>
typename FBTOEnergy<DIM>::MatT FBTOEnergy<DIM>::Hessian() const {
  VecDDT v;
  v.setZero();
  std::function<AD(const VecDDT&)> EIStatic=[&](const VecDDT& v)->AD {
    return EI(v);
  };
  return hessian(EIStatic,autodiff::wrt(v),autodiff::at(v)).template cast<T>();
}
//instance
template class FBTOEnergy<2>;
template class FBTOEnergy<3>;
}
