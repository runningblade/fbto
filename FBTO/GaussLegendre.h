#ifndef GAUSS_LEGENDRE_H
#define GAUSS_LEGENDRE_H

#include <Eigen/Dense>
#include "Pragma.h"

namespace FBTO {
class GaussLegendre {
 public:
  static const double WEIGHT[64][64];
  static const double POINT[64][64];
};
class DunavantTriangle {
 public:
  static const double DUNAVANT_WEIGHT[20][79];
  static const double DUNAVANT_POINT[20][158];
  static const int DUNAVANT_NR[20];
};
template <typename RET_TYPE,typename T>
class GaussLegendreIntegral : private GaussLegendre, private DunavantTriangle {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  template <typename FUNC_TYPE>
  static void integrate1D(FUNC_TYPE f,int deg,RET_TYPE& ret) {
    ASSERT(deg>=0 && deg<64)
    const double* weights=WEIGHT[deg];
    const double* points=POINT[deg];
    for(int i=0; i<=deg; i++)
      ret+=f(Vec3T(T(points[i]),0.0f,0.0f))*T(weights[i]);
  }
  template <typename FUNC_TYPE>
  static void integrate2D(FUNC_TYPE f,int deg,RET_TYPE& ret) {
    ASSERT(deg>=0 && deg<64)
    const double* weights=WEIGHT[deg];
    const double* points=POINT[deg];
    for(int i=0; i<=deg; i++)
      for(int j=0; j<=deg; j++)
        ret+=f(Vec3T(T(points[i]),T(points[j]),0.0f))*T(weights[i]*weights[j]);
  }
  template <typename FUNC_TYPE>
  static void integrate3D(FUNC_TYPE f,int deg,RET_TYPE& ret) {
    ASSERT(deg>=0 && deg<64)
    const double* weights=WEIGHT[deg];
    const double* points=POINT[deg];
    for(int i=0; i<=deg; i++)
      for(int j=0; j<=deg; j++)
        for(int k=0; k<=deg; k++)
          ret+=f(Vec3T(T(points[i]),T(points[j]),T(points[k])))*T(weights[i]*weights[j]*weights[k]);
  }
  //the famous Dunavant Quadrature rule see: "High Degree Efficient Symmetrical Gaussian Quadrature Rules for the Triangle" by David Dunavant
  template <typename FUNC_TYPE>
  static void integrateTri(FUNC_TYPE f,int deg,RET_TYPE& ret) {
    ASSERT(deg>=0 && deg<20)
    const double* weights=DUNAVANT_WEIGHT[deg];
    const double* points=DUNAVANT_POINT[deg];
    for(int i=0; i<DUNAVANT_NR[deg]; i++) {
      T c0=points[i*2],c1=points[i*2+1];
      ret+=f(Vec3T(c0,c1,1.0f-c0-c1))*weights[i];
    }
  }
};
}

#endif
