#ifndef DEBUG_GRADIENT_H
#define DEBUG_GRADIENT_H

#include <iostream>

//numeric delta
#define DEFINE_NUMERIC_DELTA_T(T) T DELTA=1e-8;
//gradient debug
#define DEBUG_GRADIENT(NAME,A,B) \
if(fabs(B) > sqrt(DELTA)) { \
  std::cout << "\033[31m" << NAME << ": " << A << " Err: " << B << "\033[30m" << std::endl; \
} else { \
  std::cout << NAME << ": " << A << " Err: " << B << std::endl; \
}

#endif
