#include "ALMSolver.h"
#ifdef KNITRO_SUPPORT
#include "DebugGradient.h"
#include "Timing.cuh"
#include "KNSolver.h"
#include <iomanip>

namespace FBTO {
template <int DIM,int POWRHO>
ALMSolver<DIM,POWRHO>::ALMSolver(T lambda,T mu,T minFill,T sumFillRel,int x,int y,int z)
  :KNProblem(x*y*z+(x+1)*(y+1)*(DIM==2?1:z+1)*DIM,1),_sol(lambda,mu,minFill,sumFillRel,x,y,z) {}
template <int DIM,int POWRHO>
typename ALMSolver<DIM,POWRHO>::T ALMSolver<DIM,POWRHO>::eval(const Vec& u,const Vec& v,VecM DLDu,VecM DLDv) const {
  Vec Cv=_sol.filterForward(v);
  Vec Kvu=_sol.mulK(Cv,u);
  Vec err=Kvu-_f;
  if(DLDu.data())
    DLDu.setZero();
  if(DLDv.data())
    DLDv.setZero();
  //0.5*<u,f>=0.5*<u,K(C*v)*u>
  T L=u.dot(Kvu)/2;
  if(DLDu.data())
    DLDu+=Kvu;
  if(DLDv.data())
    DLDv+=_sol.diffK(Cv,u,VecDT::Zero());
  //<lambda,K(C*v)*u-f>
  L+=_lambda.dot(err);
  if(DLDu.data())
    DLDu+=_sol.mulK(Cv,_lambda);
  if(DLDv.data())
    DLDv+=_sol.diffK(Cv,_lambda,u,VecDT::Zero());
  //mu/2*<K(C*v)*u-f,K(C*v)*u-f>
  L+=_mu/2*err.squaredNorm();
  if(DLDu.data())
    DLDu+=_sol.mulK(Cv,err)*_mu;
  if(DLDv.data())
    DLDv+=_sol.diffK(Cv,err,u,VecDT::Zero())*_mu;
  if(DLDv.data())
    DLDv=_sol.filterBackward(DLDv);
  return L;
}
template <int DIM,int POWRHO>
typename ALMSolver<DIM,POWRHO>::T ALMSolver<DIM,POWRHO>::eval(const Vec& uv,Vec* DLDuv) const {
  int nU=_f.size(),nV=uv.size()-_f.size();
  Eigen::Map<Vec> DLDu(DLDuv?DLDuv->data():NULL,nU);
  Eigen::Map<Vec> DLDv(DLDuv?DLDuv->data()+nU:NULL,nV);
  return eval(uv.segment(0,nU),uv.segment(nU,nV),DLDu,DLDv);
}
template <int DIM,int POWRHO>
FBTOSolver<DIM,POWRHO>& ALMSolver<DIM,POWRHO>::sol() {
  return _sol;
}
template <int DIM,int POWRHO>
void ALMSolver<DIM,POWRHO>::solve(std::function<VecDT(Eigen::Matrix<int,3,1>)> force,Vec& u,Vec& v) {
  _f=Vec::Zero(_sol.cols());
  for(int x=0; x<_sol.xU(); x++)
    for(int y=0; y<_sol.yU(); y++)
      for(int z=0; z<_sol.zU(); z++)
        _f.template segment<DIM>(_sol.strideU().dot(Eigen::Matrix<int,3,1>(x,y,z))*DIM)=force(Eigen::Matrix<int,3,1>(x,y,z));
  Vec uv=initProblem();
  u=uv.segment(0,_sol.cols());
  v=uv.segment(_sol.cols(),_sol.colsFill());
  Vec uLast=u,vLast=v;
  TBEG("ALM-Solve");
  _nrEval=0;
  _mu=1e3; //initialize to a small mu
  _lambda.setZero(u.size());
  int nU=u.size(),nV=v.size();
  for(int i=0; i<_sol.maxIter(); i++) {
    uv.resize(u.size()+v.size());
    uv << u,v;
    std::cout << "Solving ALM inner problem with mu=" << std::setw(15) << _mu << std::endl;
    std::cout << " L3=" << std::setw(15) << eval(uv,NULL) << std::endl;
    _uv.resize(0);  //flag recomputation
    bool succ=solveInnerAlglib(uv);
    if(!succ)
      break;
    //termination
    u=uv.segment(0,nU);
    v=uv.segment(nU,nV);
    T uDiff=(u-uLast).cwiseAbs().maxCoeff();
    T vDiff=(v-vLast).cwiseAbs().maxCoeff();
    std::cout << "uDiff=" << std::setw(15) << uDiff << " vDiff=" << std::setw(15) << vDiff << std::endl;
    if(uDiff<_sol.tol() && vDiff<_sol.tol())
      break;
    uLast=u;
    vLast=v;
    //update
    std::cout << "L0=" << std::setw(15) << eval(uv,NULL);
    _lambda+=_mu*(_sol.mulK(_sol.filterForward(v),u)-_f);  //update lambda
    std::cout << " L1=" << std::setw(15) << eval(uv,NULL);
    _mu*=2; //update mu
    std::cout << " L2=" << std::setw(15) << eval(uv,NULL) << std::endl;
  }
  TEND();
  u=uv.segment(0,nU);
  v=_sol.filterForward(uv.segment(nU,nV));
}
template <int DIM,int POWRHO>
void ALMSolver<DIM,POWRHO>::debugEval(std::function<VecDT(Eigen::Matrix<int,3,1>)> force) {
  Vec u,v;
  _f=Vec::Zero(_sol.cols());
  for(int x=0; x<_sol.xU(); x++)
    for(int y=0; y<_sol.yU(); y++)
      for(int z=0; z<_sol.zU(); z++)
        _f.template segment<DIM>(_sol.strideU().dot(Eigen::Matrix<int,3,1>(x,y,z))*DIM)=force(Eigen::Matrix<int,3,1>(x,y,z));
  u.setRandom(_sol.cols());
  v.setRandom(_sol.colsFill());
  v=(v+Vec::Ones(v.size()))/2;
  //debug
  DEFINE_NUMERIC_DELTA_T(T)
  Vec uv,grad,duv;
  uv.resize(u.size()+v.size());
  uv << u,v;
  duv.setRandom(u.size()+v.size());
  grad.setZero(u.size()+v.size());
  _lambda.setRandom(u.size());
  _mu=(T)rand()/(T)RAND_MAX;
  T L=eval(uv,&grad);
  T L2=eval(uv+duv*DELTA,NULL);
  DEBUG_GRADIENT("DLDuv",grad.dot(duv),grad.dot(duv)-(L2-L)/DELTA)
}
template <int DIM,int POWRHO>
void ALMSolver<DIM,POWRHO>::funcReport
(const alglib::real_1d_array&,
 double func,
 void* ptr) {
  //ALMSolver<DIM,POWRHO>* sol=reinterpret_cast<ALMSolver<DIM,POWRHO>*>(ptr);
  //std::cout << "mu=" << std::setw(15) << sol->_mu << " func=" << std::setw(15) << func << std::endl;
}
template <int DIM,int POWRHO>
void ALMSolver<DIM,POWRHO>::funcGrad
(const alglib::real_1d_array& x,
 double& func,
 alglib::real_1d_array& grad,
 void* ptr) {
  ALMSolver<DIM,POWRHO>* sol=reinterpret_cast<ALMSolver<DIM,POWRHO>*>(ptr);
  //std::cout << "mu=" << std::setw(15) << sol->_mu << " func=" << std::setw(15) << func << std::endl;
  computeVar(sol,Eigen::Map<const Eigen::Matrix<double,-1,1>>(x.getcontent(),x.length()).template cast<T>());
  func=sol->_L;
  for(int i=0; i<x.length(); i++)
    grad[i]=sol->_DLDuv[i];
}
template <int DIM,int POWRHO>
int ALMSolver<DIM,POWRHO>::callbackEvalF
(KN_context_ptr             kc,
 CB_context_ptr             cb,
 KN_eval_request_ptr const  evalRequest,
 KN_eval_result_ptr  const  evalResult,
 void              * const  userParams) {
  const double *x;
  double *obj;
  if(evalRequest->type!=KN_RC_EVALFC) {
    printf("*** callbackEvalF incorrectly called with eval type %d\n",evalRequest->type);
    return -1;
  }
  x=evalRequest->x;
  obj=evalResult->obj;
  ALMSolver<DIM,POWRHO>* sol=reinterpret_cast<ALMSolver<DIM,POWRHO>*>(userParams);
  computeVar(sol,Eigen::Map<const Eigen::Matrix<double,-1,1>>(x,sol->_sol.cols()+sol->_sol.colsFill()).template cast<T>());
  *obj=sol->_L;
  return 0;
}
template <int DIM,int POWRHO>
int ALMSolver<DIM,POWRHO>::callbackEvalG
(KN_context_ptr             kc,
 CB_context_ptr             cb,
 KN_eval_request_ptr const  evalRequest,
 KN_eval_result_ptr  const  evalResult,
 void              * const  userParams) {
  const double *x;
  double *objGrad;
  if(evalRequest->type!=KN_RC_EVALGA) {
    printf("*** callbackEvalG incorrectly called with eval type %d\n",
           evalRequest->type);
    return -1;
  }
  x=evalRequest->x;
  objGrad=evalResult->objGrad;
  ALMSolver<DIM,POWRHO>* sol=reinterpret_cast<ALMSolver<DIM,POWRHO>*>(userParams);
  computeVar(sol,Eigen::Map<const Eigen::Matrix<double,-1,1>>(x,sol->_sol.cols()+sol->_sol.colsFill()).template cast<T>());
  Eigen::Map<Eigen::Matrix<double,-1,1>>(objGrad,sol->_DLDuv.size())=sol->_DLDuv.template cast<double>();
  return 0;
}
template <int DIM,int POWRHO>
int ALMSolver<DIM,POWRHO>::callbackEvalH
(KN_context_ptr             kc,
 CB_context_ptr             cb,
 KN_eval_request_ptr const  evalRequest,
 KN_eval_result_ptr  const  evalResult,
 void              * const  userParams) {
  if(evalRequest->type != KN_RC_EVALH && evalRequest->type != KN_RC_EVALH_NO_F) {
    printf("*** callbackEvalH incorrectly called with eval type %d\n",
           evalRequest->type);
    return -1;
  }
  return 0;
}
template <int DIM,int POWRHO>
void ALMSolver<DIM,POWRHO>::computeVar(ALMSolver<DIM,POWRHO>* sol,const Vec& uv) {
  if(sol->_uv.size()!=uv.size() || sol->_uv!=uv) {
    sol->_uv=uv;
    sol->_DLDuv.resize(uv.size());
    sol->_L=sol->eval(sol->_uv,&(sol->_DLDuv));
    if((sol->_nrEval%sol->_sol.writeInterval())==0) {
      Vec u=uv.segment(0,sol->_sol.cols());
      Vec v=uv.segment(sol->_sol.cols(),sol->_sol.colsFill());
      Vec Cv=sol->_sol.filterForward(v);
      int k2=sol->_nrEval/sol->_sol.writeInterval();
      const auto& vCPU=OUTPUT_FILTERED?Cv:v;
      sol->_sol.writeVTK(sol->_sol.writeMul(),&u,&vCPU,sol->_sol.writePath()+"/iter"+std::to_string(k2++)+".vtk");
    }
    if((sol->_nrEval%std::abs(sol->_sol.reportInterval()))==0) {
      Vec u=uv.segment(0,sol->_sol.cols());
      Vec v=uv.segment(sol->_sol.cols(),sol->_sol.colsFill());
      sol->_sol.solveK(u,sol->_sol.filterForward(v),sol->_f);
      std::cout << "nrEval=" << std::setw(5) << sol->_nrEval
                << " energy=" << std::setw(15) << u.dot(sol->_f)/2
                << " L=" << std::setw(15) << sol->_L << std::endl;
    }
    sol->_nrEval++;
  }
}
template <int DIM,int POWRHO>
bool ALMSolver<DIM,POWRHO>::solveInnerKnitro(Vec& uv) {
  //Create a solver
  std::vector<double> init;
  for(int i=0; i<uv.size(); i++)
    init.push_back(uv[i]);
  this->setXInitial(init);
  knitro::KNSolver solver(this);
  solver.setParam(KN_PARAM_CG_MAXIT,50);
  solver.setParam(KN_PARAM_MAXIT,_sol.maxIter());
  solver.setParam(KN_PARAM_HESSOPT,KN_HESSOPT_BFGS);
  solver.setParam(KN_PARAM_ALGORITHM,KN_ALG_BAR_CG);
  solver.setParam(KN_PARAM_DERIVCHECK,KN_DERIVCHECK_NONE);
  solver.setParam(KN_PARAM_BAR_FEASIBLE,KN_BAR_FEASIBLE_GET_STAY);
  solver.setParam(KN_PARAM_OPTTOL,_sol.tol());
  solver.setParam(KN_PARAM_XTOL,0);
  solver.setParam(KN_PARAM_FTOL,0);
  //Solve
  solver.initProblem();
  solver.setUserParams(this);
  TBEG("SQP-Solve");
  int solveStatus=solver.solve();
  TEND();
  std::vector<double> x;
  std::vector<double> y;
  solver.getSolution(x,y);
  uv=Eigen::Map<const Vec>(x.data(),x.size());
  std::cout << "Knitro termination code=" << solveStatus << std::endl;
  return solveStatus>=-199;
}
template <int DIM,int POWRHO>
bool ALMSolver<DIM,POWRHO>::solveInnerAlglib(Vec& uv) {
  alglib::real_1d_array uvALG,lbALG,ubALG;
  uvALG.setlength(uv.size());
  lbALG.setlength(uv.size());
  ubALG.setlength(uv.size());
  for(int i=0; i<uv.size(); i++)
    uvALG[i]=uv[i];
  for(int x=0,i=0; x<_sol.xU(); x++)
    for(int y=0; y<_sol.yU(); y++)
      for(int z=0; z<_sol.zU(); z++)
        for(int d=0; d<DIM; d++,i++)
          if(_sol.isMasked(x,y,z)) {
            lbALG[i]=0;
            ubALG[i]=0;
          } else {
            lbALG[i]=-1e6;
            ubALG[i]= 1e6;
          }
  for(int x=0,i=_sol.cols(); x<_sol.x(); x++)
    for(int y=0; y<_sol.y(); y++)
      for(int z=0; z<_sol.z(); z++,i++)
        if(_sol.isFMasked(x,y,z)) {
          lbALG[i]=0;
          ubALG[i]=0;
        } else {
          lbALG[i]=_sol.minFill();
          ubALG[i]=1;
        }
  alglib::real_2d_array cALG;
  alglib::integer_1d_array ctALG;
  cALG.setlength(1,uv.size()+1);
  for(int i=0; i<_sol.cols(); i++)
    cALG(0,i)=0;
  for(int i=_sol.cols(); i<uv.size(); i++)
    cALG(0,i)=1;
  cALG(0,uv.size())=_sol.sumFill();
  ctALG.setlength(1);
  ctALG[0]=-1;
  //setup optimizer
  alglib::minbleicreport rep;
  alglib::minbleicstate state;
  alglib::minbleiccreate(uvALG,state);
  alglib::minbleicsetlc(state,cALG,ctALG);
  alglib::minbleicsetbc(state,lbALG,ubALG);
  computeVar(this,uv);
  alglib::minbleicsetcond(state,_sol.tol()*std::max<T>(1,_DLDuv.norm()),0,0,_sol.maxIter());
  alglib::minbleicoptimize(state,&funcGrad,NULL,this);
  alglib::minbleicresults(state,uvALG,rep);
  for(int i=0; i<uv.size(); i++)
    uv[i]=uvALG[i];
  int errCode=rep.terminationtype;
  std::cout << "ALGLIB termination code=" << errCode << std::endl;
  return errCode==1 || errCode==2 || errCode==4 || errCode==5;
}
template <int DIM,int POWRHO>
typename ALMSolver<DIM,POWRHO>::Vec ALMSolver<DIM,POWRHO>::initProblem() {
  //Variables
  std::vector<double> lbv,ubv,init;
  for(int x=0; x<_sol.xU(); x++)
    for(int y=0; y<_sol.yU(); y++)
      for(int z=0; z<_sol.zU(); z++)
        for(int d=0; d<DIM; d++)
          if(_sol.isMasked(x,y,z)) {
            lbv.push_back(0);
            ubv.push_back(0);
            init.push_back(0);
          } else {
            lbv.push_back(-KN_INFINITY);
            ubv.push_back( KN_INFINITY);
            init.push_back(0);
          }
  for(int x=0; x<_sol.x(); x++)
    for(int y=0; y<_sol.y(); y++)
      for(int z=0; z<_sol.z(); z++) {
        if(_sol.isFMasked(x,y,z)) {
          lbv.push_back(0);
          ubv.push_back(0);
          init.push_back(0);
        } else {
          lbv.push_back(_sol.minFill());
          ubv.push_back(1);
          init.push_back(_sol.sumFill()/_sol.nrValidCell());
        }
      }
  this->setVarLoBnds(lbv);
  this->setVarUpBnds(ubv);
  this->setXInitial(init);
  //total constraint
  std::vector<int> vss;
  std::vector<double> css;
  for(int i=0; i<_sol.colsFill(); i++) {
    vss.push_back(i+_sol.cols());
    css.push_back(1);
  }
  KNLinearStructure lc(vss,css);
  this->setConUpBnds({{_sol.sumFill()}});
  this->getConstraintsLinearParts().add(0,lc);
  //Evaluations callbacks
  this->setObjEvalCallback(&ALMSolver::callbackEvalF);
  this->setGradEvalCallback(&ALMSolver::callbackEvalG);
  this->setHessEvalCallback(&ALMSolver::callbackEvalH);
  return Eigen::Map<const Vec>(init.data(),init.size());
}
//instance
template class ALMSolver<2,1>;
template class ALMSolver<2,2>;
template class ALMSolver<2,3>;
template class ALMSolver<2,4>;
template class ALMSolver<3,1>;
template class ALMSolver<3,2>;
template class ALMSolver<3,3>;
template class ALMSolver<3,4>;
}
#endif
