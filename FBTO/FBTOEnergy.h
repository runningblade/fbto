#ifndef FBTO_ENERGY_H
#define FBTO_ENERGY_H

#include "Utils.h"
#include "Interp.h"
#include "GaussLegendre.h"
#include <autodiff/forward/dual.hpp>
#include <autodiff/forward/dual/eigen.hpp>

namespace FBTO {
template <int DIM>
class FBTOEnergy {
 public:
  typedef SCALAR_TYPE T;
  DECL_MAT_VEC_MAP_TYPES_T
  //autodiff
  typedef autodiff::dual2nd AD;
  typedef Eigen::Matrix<AD,DIM,1> VecDT;
  typedef Eigen::Matrix<AD,DIM,DIM> MatDT;
  typedef Eigen::Matrix<AD,DIM*(1<<DIM),1> VecDDT;
  typedef Eigen::Matrix<AD,DIM*(1<<DIM),DIM*(1<<DIM)> MatDDT;
  FBTOEnergy(T lambda,T mu,int deg);
  MatDT F(const Vec3T& pos,const VecDDT& v) const;
  AD E(const Vec3T& pos,const VecDDT& v) const;
  AD EI(const VecDDT& v) const;
  MatT Hessian() const;
 private:
  T _lambda,_mu;
  int _deg;
};
}

#endif
