#ifndef OPTIMALITY_CRITERIA_METHOD_H
#define OPTIMALITY_CRITERIA_METHOD_H

#include "FBTO.h"

namespace FBTO {
template <int DIM,int POWRHO>
class OCMSolver {
public:
  typedef SCALAR_TYPE T;
  DECL_MAT_VEC_MAP_TYPES_T
  //Dense
  typedef Eigen::Matrix<T,DIM,1> VecDT;
  typedef Eigen::Matrix<T,DIM,DIM> MatDT;
  typedef Eigen::Matrix<T,DIM*(1<<DIM),1> VecDDT;
  typedef Eigen::Matrix<T,DIM*(1<<DIM),DIM*(1<<DIM)> MatDDT;
  OCMSolver(T lambda,T mu,T minFill,T sumFillRel,int x,int y,int z=1);
  const Vec& fill() const;
  FBTOSolver<DIM,POWRHO>& sol();
  void solve(std::function<VecDT(Eigen::Matrix<int,3,1>)> force,T move=0.2,T tolBinarySearch=1e-4);
private:
  void initProblem();
  Vec gradient();
  //problem data
  FBTOSolver<DIM,POWRHO> _sol;
  Vec _lbv,_ubv,_f;
  //temporary data
  Vec _fill,_fillC,_u;
  bool _useGPU;
};
}

#endif
