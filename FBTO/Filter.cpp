#include "Filter.h"
#include "FilterGPU.cuh"
#include "DebugGradient.h"

namespace FBTO {
template <int DIM>
Filter<DIM>::Filter(T thickness) {
  _hsize=floor(thickness);
  //center
  T total=0;
  for(int x=-_hsize; x<=_hsize; x++) {
    _stencil.push_back(stencil(fabs(x)/thickness));
    total+=_stencil.back();
  }
  for(auto& s:_stencil)
    s/=total;
  //left
  for(int x=-_hsize,i=0; x<=0; x++,i++)
    _stencilL.push_back(_stencil[i]);
  while(_stencilL.size()<_stencil.size()) {
    total=0;
    for(int i=(int)_stencilL.size(); i<(int)_stencil.size(); i++)
      total+=_stencil[i];
    _stencilL.push_back(total);
  }
  //right
  _stencilR.assign(_stencilL.begin(),_stencilL.end());
  std::reverse(_stencilR.begin(),_stencilR.end());
}
template <int DIM>
void Filter<DIM>::setFMask(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,std::shared_ptr<Eigen::Matrix<bool,-1,1>> fMask) {
  _fMask=fMask;
  for(int d=0; d<DIM; d++) {
    if(!_fMask)
      _C[d].resize(0,0);
    else {
      _C[d].resize(X*Y*Z,X*Y*Z);
      std::vector<Eigen::Triplet<T,int>> trips;
      Eigen::Matrix<int,3,1> minC(0,0,0),maxC(X-1,Y-1,Z-1);
      OMP_PARALLEL_FOR_
      for(int x=0; x<X; x++)
        for(int y=0; y<Y; y++)
          for(int z=0; z<Z; z++) {
            Eigen::Matrix<int,3,1> id(x,y,z);
            int offset=stride.dot(id);
            if((*fMask)[offset])
              OMP_CRITICAL_
              trips.push_back(Eigen::Triplet<T,int>(offset,offset,1));
            else {
              Eigen::Matrix<int,3,1> idLeft=id,idRight=id,idNext,idOffset,idClamp;
              //identify left
              for(int i=0; i<_hsize; i++) {
                idNext=idLeft-Eigen::Matrix<int,3,1>::Unit(d);
                if((idNext.array()>=minC.array()).all() && (idNext.array()<=maxC.array()).all() && !(*fMask)[stride.dot(idNext)])
                  idLeft=idNext;
                else break;
              }
              //identify right
              for(int i=0; i<_hsize; i++) {
                idNext=idRight+Eigen::Matrix<int,3,1>::Unit(d);
                if((idNext.array()>=minC.array()).all() && (idNext.array()<=maxC.array()).all() && !(*fMask)[stride.dot(idNext)])
                  idRight=idNext;
                else break;
              }
              //convolve
              for(int dd=-_hsize,i=0; dd<=_hsize; dd++,i++) {
                idOffset=id+Eigen::Matrix<int,3,1>::Unit(d)*dd;
                idClamp=idOffset.cwiseMax(idLeft).cwiseMin(idRight);
                ASSERT(!(*_fMask)[stride.dot(idClamp)])
                OMP_CRITICAL_
                trips.push_back(Eigen::Triplet<T,int>(offset,stride.dot(idClamp),_stencil[i]));
              }
            }
          }
      _C[d].setFromTriplets(trips.begin(),trips.end());
    }
  }
}
template <int DIM>
template <int dim>
void Filter<DIM>::forwardDIM(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const Vec& u,Vec& Fu) const {
  if(_C[dim].size()>0)
    Fu=_C[dim]*u;
  else {
    Eigen::Matrix<int,3,1> minC(0,0,0),maxC(X-1,Y-1,Z-1);
    OMP_PARALLEL_FOR_
    for(int x=0; x<X; x++)
      for(int y=0; y<Y; y++)
        for(int z=0; z<Z; z++) {
          Eigen::Matrix<int,3,1> id(x,y,z),idOffset,idClamp;
          T& total=Fu[stride.dot(id)]=0;
          for(int dd=-_hsize,i=0; dd<=_hsize; dd++,i++) {
            idOffset=id+Eigen::Matrix<int,3,1>::Unit(dim)*dd;
            idClamp=idOffset.cwiseMax(minC).cwiseMin(maxC);
            total+=u[stride.dot(idClamp)]*_stencil[i];
          }
        }
  }
}
template <int DIM>
template <int dim>
void Filter<DIM>::backwardDIM(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const Vec& DEDFu,Vec& DEDu) const {
  if(_C[dim].size()>0)
    DEDu=_C[dim].transpose()*DEDFu;
  else {
    Eigen::Matrix<int,3,1> minC(0,0,0),maxC(X-1,Y-1,Z-1);
    OMP_PARALLEL_FOR_
    for(int x=0; x<X; x++)
      for(int y=0; y<Y; y++)
        for(int z=0; z<Z; z++) {
          Eigen::Matrix<int,3,1> id(x,y,z),idOffset,idClamp;
          T& total=DEDu[stride.dot(id)]=0;
          for(int dd=-_hsize,i=0; dd<=_hsize; dd++,i++) {
            idOffset=id+Eigen::Matrix<int,3,1>::Unit(dim)*dd;
            idClamp=idOffset.cwiseMax(minC).cwiseMin(maxC);
            if(id[dim]==0)
              total+=DEDFu[stride.dot(idClamp)]*_stencilL[i];
            else if(id[dim]==maxC[dim])
              total+=DEDFu[stride.dot(idClamp)]*_stencilR[i];
            else if(idOffset[dim]==idClamp[dim])
              total+=DEDFu[stride.dot(idClamp)]*_stencil[i];
          }
        }
  }
}
template <int DIM>
void Filter<DIM>::forward(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const Vec& u,Vec& Fu) {
  _FuX.resize(u.size());
  _FuY.resize(u.size());
  if(DIM==2) {
    forwardDIM<0>(X,Y,Z,stride,u,_FuX);
    forwardDIM<1>(X,Y,Z,stride,_FuX,Fu);
  } else {
    forwardDIM<0>(X,Y,Z,stride,u,_FuX);
    forwardDIM<1>(X,Y,Z,stride,_FuX,_FuY);
    forwardDIM<2>(X,Y,Z,stride,_FuY,Fu);
  }
}
template <int DIM>
void Filter<DIM>::backward(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const Vec& DEDFu,Vec& DEDu) {
  _FuX.resize(DEDFu.size());
  _FuY.resize(DEDFu.size());
  if(DIM==2) {
    backwardDIM<1>(X,Y,Z,stride,DEDFu,_FuX);
    backwardDIM<0>(X,Y,Z,stride,_FuX,DEDu);
  } else {
    backwardDIM<2>(X,Y,Z,stride,DEDFu,_FuY);
    backwardDIM<1>(X,Y,Z,stride,_FuY,_FuX);
    backwardDIM<0>(X,Y,Z,stride,_FuX,DEDu);
  }
}
template <int DIM>
void Filter<DIM>::debug(int X,int Y,int Z) {
  ASSERT((Z==1)+(DIM!=2)==1);
  DEFINE_NUMERIC_DELTA_T(T)
  Vec du=Vec::Random(X*Y*Z);
  Vec u=Vec::Random(X*Y*Z),Fu=u,Fu2=u;
  Vec DEDFu=Vec::Random(X*Y*Z),DEDu=DEDFu;
  Eigen::Matrix<int,3,1> stride(Y*Z,Z,1);
  forward(X,Y,Z,stride,u,Fu);
  forward(X,Y,Z,stride,u+du*DELTA,Fu2);
  backward(X,Y,Z,stride,DEDFu,DEDu);
  //check diffN,diffA
  T diffA=DEDu.dot(du);
  T diffN=(Fu2.dot(DEDFu)-Fu.dot(DEDFu))/DELTA;
  DEBUG_GRADIENT("FilterGradient",diffA,diffA-diffN)
}
template <int DIM>
void Filter<DIM>::debugGPU(int X,int Y,int Z) {
  ASSERT((Z==1)+(DIM!=2)==1);
  DEFINE_NUMERIC_DELTA_T(T)
  Eigen::Matrix<int,3,1> stride(Y*Z,Z,1);

  GPU::initFilter(DIM,_hsize);
  GPU::assignStencil(_stencil,_stencilL,_stencilR);
  if(_C[0].size()>0)
    GPU::assignC(_C);
  Vec u=Vec::Random(X*Y*Z),FuRef=u,Fu=u;
  forward(X,Y,Z,stride,u,FuRef);
  GPU::forward(X,Y,Z,stride,u,Fu);
  DEBUG_GRADIENT("FuGPU",Fu.norm(),(Fu-FuRef).norm())

  Vec DEDFu=Vec::Random(X*Y*Z),DEDuRef=DEDFu,DEDu=DEDFu;
  backward(X,Y,Z,stride,DEDFu,DEDuRef);
  GPU::backward(X,Y,Z,stride,DEDFu,DEDu);
  DEBUG_GRADIENT("DEDuGPU",DEDu.norm(),(DEDu-DEDuRef).norm())
  GPU::finalizeFilter();
}
template <int DIM>
void Filter<DIM>::beginGPU() {
  GPU::initFilter(DIM,_hsize);
  GPU::assignStencil(_stencil,_stencilL,_stencilR);
  if(_C[0].size()>0)
    GPU::assignC(_C);
}
template <int DIM>
void Filter<DIM>::endGPU() {
  GPU::finalizeFilter();
}
template <int DIM>
typename Filter<DIM>::T Filter<DIM>::stencil(T r) const {
  //1−6*r^2+8*r^3−3*r^4
  if(r>1)
    return 0;
  T r2=r*r,r3=r2*r,r4=r3*r;
  return 1-6*r2+8*r3-3*r4;
}
template class Filter<2>;
template class Filter<3>;
}
