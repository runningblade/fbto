#ifndef UTILS_H
#define UTILS_H

#include "Pragma.h"
#include "ParallelVector.h"
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <experimental/filesystem>

typedef double SCALAR_TYPE;
#define NORM_TYPE 2//Eigen::Infinity
#define DEFAULT_PRESERVE_SUM true
#define DEFAULT_CG_TOL_INIT 1e-8
#define DEFAULT_CG_TOL 1e-8
#define OUTPUT_FILTERED 0

namespace FBTO {
template <typename T>
T mu(T young,T poisson) {
  return young/(2.0f*(1.0f+poisson));
}
template <typename T>
T lambda(T young,T poisson) {
  return poisson*young/((1.0f+poisson)*(1.0f-2.0f*poisson));
}
template <typename T>
struct TriangleHash {
  static size_t hashCombine(size_t h1,size_t h2) {
    return h1^(h2<<1);
  }
  size_t operator()(const Eigen::Matrix<T,3,1>& key) const {
    std::hash<T> h;
    return hashCombine(hashCombine(h(key[0]),h(key[1])),h(key[2]));
  }
  bool operator()(const Eigen::Matrix<T,3,1>& a,const Eigen::Matrix<T,3,1>& b) const {
    for(int i=0; i<3; i++)
      if(a[i]<b[i])
        return true;
      else if(a[i]>b[i])
        return false;
    return false;
  }
};
template <int DIM,int POWRHO>
class FBTOSolver;
template <int DIM,int POWRHO>
struct MulK {
  typedef SCALAR_TYPE T;
  DECL_MAT_VEC_MAP_TYPES_T
  MulK(const FBTOSolver<DIM,POWRHO>& sol,const Vec& fill):_sol(sol),_fill(fill) {}
  Vec operator*(const Vec& b) const {
    return _sol.mulK(_fill,b);
  }
  Vec solve(const Vec& b) const {
    return b;
  }
  int cols() const {
    return _sol.cols();
  }
  const FBTOSolver<DIM,POWRHO>& _sol;
  const Vec& _fill;
};
template <typename Derived>
void addBlock(ParallelVector<Eigen::Triplet<typename Derived::Scalar,int>>& H,int r,int c,const Eigen::MatrixBase<Derived>& coef) {
  int nrR=coef.rows();
  int nrC=coef.cols();
  for(int i=0; i<nrR; i++)
    for(int j=0; j<nrC; j++)
      H.push_back(Eigen::Triplet<typename Derived::Scalar,int>(r+i,c+j,coef(i,j)));
}
template <typename T>
void addBlockId(ParallelVector<Eigen::Triplet<T,int>>& H,int r,int c,int nr,T coefId) {
  for(int i=0; i<nr; i++)
    H.push_back(Eigen::Triplet<T,int>(r+i,c+i,coefId));
}
//filesystem
inline bool notDigit(char c) {
  return !std::isdigit(c);
}
inline bool lessDirByNumber(std::experimental::filesystem::v1::path A,std::experimental::filesystem::v1::path B) {
  std::string strA=A.string();
  std::string::iterator itA=std::remove_if(strA.begin(),strA.end(),notDigit);
  strA.erase(itA,strA.end());

  std::string strB=B.string();
  std::string::iterator itB=std::remove_if(strB.begin(),strB.end(),notDigit);
  strB.erase(itB,strB.end());

  int idA,idB;
  std::istringstream(strA) >> idA;
  std::istringstream(strB) >> idB;
  return idA < idB;
}
inline bool exists(const std::experimental::filesystem::v1::path& path) {
  return std::experimental::filesystem::v1::exists(path);
}
inline void removeDir(const std::experimental::filesystem::v1::path& path) {
  if(std::experimental::filesystem::v1::exists(path))
    try {
      std::experimental::filesystem::v1::remove_all(path);
    } catch(...) {}
}
inline void create(const std::experimental::filesystem::v1::path& path) {
  if(!std::experimental::filesystem::v1::exists(path))
    try {
      std::experimental::filesystem::v1::create_directory(path);
    } catch(...) {}
}
inline void recreate(const std::experimental::filesystem::v1::path& path) {
  removeDir(path);
  try {
    std::experimental::filesystem::v1::create_directory(path);
  } catch(...) {}
}
inline std::vector<std::experimental::filesystem::v1::path> files(const std::experimental::filesystem::v1::path& path) {
  std::vector<std::experimental::filesystem::v1::path> ret;
  for(std::experimental::filesystem::v1::directory_iterator beg(path),end; beg!=end; beg++)
    if(std::experimental::filesystem::v1::is_regular_file(*beg))
      ret.push_back(*beg);
  return ret;
}
inline std::vector<std::experimental::filesystem::v1::path> directories(const std::experimental::filesystem::v1::path& path) {
  std::vector<std::experimental::filesystem::v1::path> ret;
  for(std::experimental::filesystem::v1::directory_iterator beg(path),end; beg!=end; beg++)
    if(std::experimental::filesystem::v1::is_directory(*beg))
      ret.push_back(*beg);
  return ret;
}
inline void sortFilesByNumber(std::vector<std::experimental::filesystem::v1::path>& files) {
  sort(files.begin(),files.end(),lessDirByNumber);
}
inline bool isDir(const std::experimental::filesystem::v1::path& path) {
  return std::experimental::filesystem::v1::is_directory(path);
}
inline size_t fileSize(const std::experimental::filesystem::v1::path& path) {
  return (size_t)std::experimental::filesystem::v1::file_size(path);
}
}

#endif
