#include "cuda.h"
#include "cuda_runtime_api.h"
#include <iostream>
#include <iomanip>

namespace FBTO {
namespace GPU {
void memoryCost() {
  int numGPUs,id;
  size_t free,total;
  cudaGetDeviceCount(&numGPUs);
  for(int gpuId=0; gpuId<numGPUs; gpuId++) {
    cudaSetDevice(gpuId);
    cudaGetDevice(&id);
    cudaMemGetInfo(&free,&total);
    std::cout << "GPU " << id << " used memory: " << std::setw(15) << ((total-free)/1024/1024) << "mb" << std::endl;
  }
}
}
}
