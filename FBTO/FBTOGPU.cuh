#include <unordered_set>
#include <Eigen/Dense>
#include "Utils.h"

namespace FBTO {
namespace GPU {
extern int DIM;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
//utils
#define NUM_BLOCK(n,blkSz) int(((n)+(blkSz)-1)/(blkSz))
#define ROUND_UP(n,blkSz) NUM_BLOCK(n,blkSz)*(blkSz)
#define SAFE_CUDA_CALL(A) {cudaError_t e=A;if(e!=cudaSuccess) {std::cout << "cudaErrorCode: " << e << " at " << __LINE__ << " of " << __FUNCTION__ << std::endl;}}
//interface
extern void init(int dim,int powRho,T minFill,T sumFill,int x,int y,int z,int D,int maxIter,int reportInterval,T tol,T tolX,const Eigen::Matrix<bool,3,1>& symmetry);
extern void initWrite(const std::string& writePath,int writeInterval,T writeMul,
                      std::function<void(T,const Vec*,const Vec*,const std::string&,int,int)> cb);
extern void setCBSolver(std::function<void(Vec&,const Vec&,const Vec&)> cbSol);
extern void finalize();
extern void assignK(const MatT& K);
extern void setMask(const std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash<int>>& fix,std::shared_ptr<Eigen::Matrix<bool,-1,1>> fMask);
extern Vec mulK(const Vec& fill,const Vec& b);
extern int solveK(Vec& u,const Vec& fill,const Vec& b,T tol,int iters=10E4);
extern void solveKGD(Vec& u,const Vec& fill,const Vec& b,T beta);
extern void solveKKrylov(Vec& u,const Vec& fill,const Vec& b,T beta,int n);
extern Vec modifyF(const Vec& force,const Vec& sf,const Vec& fill);
extern Vec diffK(const Vec& fill,const std::vector<Vec>& u,const MatT& sf,bool preserveSum);
extern Vec projX(const Vec& fill);
extern Vec solveUnilevel(const MatT& f,const MatT& sf,T alpha0,T m);
extern Vec solveBilevel(const MatT& f,const MatT& sf,T beta0,T alpha0,T m);
}
}
