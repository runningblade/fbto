#ifndef CHOLMOD_SOLVER_H
#define CHOLMOD_SOLVER_H
#ifdef WITH_CHOLMOD

#include "Utils.h"
#include "cholmod.h"
#include <deque>
#include <iomanip>
#include <memory>

namespace FBTO {
//#define DEBUG_REFERENCE_SOLVER
#define FORCE_ADD_DOUBLE_PRECISION
class CholmodSolver {
 public:
  typedef SCALAR_TYPE T;
  DECL_MAT_VEC_MAP_TYPES_T
  typedef Eigen::Triplet<T,int> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  CholmodSolver(const SMatT& A);
  CholmodSolver();
  ~CholmodSolver();
  std::shared_ptr<CholmodSolver> copy() const;
  virtual void compute(const SMatT& A);
  virtual MatT solve(const MatT& b) const;
  virtual Eigen::ComputationInfo info() const;
 private:
  void clear();
  Eigen::ComputationInfo _info;
  cholmod_factor* _L;
#ifdef DEBUG_REFERENCE_SOLVER
  SMatT _A0;
#endif
};
}

#endif
#endif
