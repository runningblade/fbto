#ifndef ALM_SOLVER_H
#define ALM_SOLVER_H
#ifdef KNITRO_SUPPORT

#include "FBTO.h"
#include "KNProblem.h"
#include "optimization.h"

using namespace knitro;

namespace FBTO {
template <int DIM,int POWRHO>
class ALMSolver : public knitro::KNProblem {
 public:
  typedef SCALAR_TYPE T;
  DECL_MAT_VEC_MAP_TYPES_T
  //Dense
  typedef Eigen::Matrix<T,DIM,1> VecDT;
  typedef Eigen::Matrix<T,DIM,DIM> MatDT;
  typedef Eigen::Matrix<T,DIM*(1<<DIM),1> VecDDT;
  typedef Eigen::Matrix<T,DIM*(1<<DIM),DIM*(1<<DIM)> MatDDT;
  //argmin_{u,v} 0.5*<u,f>
  //  s.t.       \|K(C*v)*u-f\|^2/2
  //the augmented Lagrangian function:
  //L=0.5*<u,f>+lambda^T[K(C*v)*u-f]+mu/2*\|K(C*v)*u-f\|^2
  //the gradient is:
  //\FPP{L}{u}=0.5*f+K(C*v)*[lambda+mu*(K(C*v)*u-f)]
  //\FPP{L}{v}=lambda^T*\FPP{K}{v}(C*v)*u+mu*<K(C*v)*u-f,\FPP{K}{v}(C*v)*u-f>
  ALMSolver(T lambda,T mu,T minFill,T sumFillRel,int x,int y,int z=1);
  T eval(const Vec& u,const Vec& v,VecM DLDu,VecM DLDv) const;
  T eval(const Vec& uv,Vec* DLDuv) const;
  FBTOSolver<DIM,POWRHO>& sol();
  void solve(std::function<VecDT(Eigen::Matrix<int,3,1>)> force,Vec& u,Vec& v);
  void debugEval(std::function<VecDT(Eigen::Matrix<int,3,1>)> force);
 private:
  static void funcReport(const alglib::real_1d_array&,
                         double func,void* ptr);
  static void funcGrad(const alglib::real_1d_array& x,
                       double& func,alglib::real_1d_array& grad,
                       void* ptr);
  static int callbackEvalF(KN_context_ptr             kc,
                           CB_context_ptr             cb,
                           KN_eval_request_ptr const  evalRequest,
                           KN_eval_result_ptr  const  evalResult,
                           void              * const  userParams);
  static int callbackEvalG(KN_context_ptr             kc,
                           CB_context_ptr             cb,
                           KN_eval_request_ptr const  evalRequest,
                           KN_eval_result_ptr  const  evalResult,
                           void              * const  userParams);
  static int callbackEvalH(KN_context_ptr             kc,
                           CB_context_ptr             cb,
                           KN_eval_request_ptr const  evalRequest,
                           KN_eval_result_ptr  const  evalResult,
                           void              * const  userParams);
  static void computeVar(ALMSolver<DIM,POWRHO>* sol,const Vec& uv);
  bool solveInnerKnitro(Vec& uv);
  bool solveInnerAlglib(Vec& uv);
  Vec initProblem();
  //data
  FBTOSolver<DIM,POWRHO> _sol;
  Vec _lambda,_f,_uv,_DLDuv;
  int _nrEval;
  T _mu,_L;
};
}

#endif
#endif
