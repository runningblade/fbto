#ifndef HOUSEHOLDER_QR_H
#define HOUSEHOLDER_QR_H

#include <Eigen/Dense>

namespace FBTO {
template <typename T>
void computeQR(Eigen::Matrix<T,-1,-1>& a,Eigen::Matrix<T,-1,1>& d) {
  //an implementation of
  //Algorithms for the QR-Decomposition
  //https://people.inf.ethz.ch/gander/papers/qrneu.pdf
  d.resize(a.cols());
  for(int j=0,rj=a.rows(); j<a.cols(); j++,rj--) {
    T s=sqrt(a.col(j).tail(rj).squaredNorm());
    d[j]=a(j,j)>0?-s:s;
    T fak=sqrt(s*(s+abs(a(j,j))));
    a(j,j)-=d[j];
    a.col(j).tail(rj)/=fak;
    for(int i=j+1; i<a.cols(); i++) {
      s=a.col(j).tail(rj).dot(a.col(i).tail(rj));
      a.col(i).tail(rj)-=a.col(j).tail(rj)*s;
    }
  }
}
template <typename T>
void mulQT(const Eigen::Matrix<T,-1,-1>& a,Eigen::Matrix<T,-1,1>& b) {
  for(int j=0,rj=a.rows(); j<a.cols(); j++,rj--) {
    T s=a.col(j).tail(rj).dot(b.tail(rj));
    b.tail(rj)-=a.col(j).tail(rj)*s;
  }
  b=b.head(a.cols()).eval();
}
template <typename T>
void solveR(const Eigen::Matrix<T,-1,-1>& a,const Eigen::Matrix<T,-1,1>& d,Eigen::Matrix<T,-1,1>& b) {
  for(int j=a.cols()-1; j>=0; j--) {
    b[j]/=d[j];
    b.head(j)-=a.col(j).head(j)*b[j];
  }
}
template <typename T>
void solveQR(Eigen::Matrix<T,-1,-1>& a,Eigen::Matrix<T,-1,1>& b) {
  Eigen::Matrix<T,-1,1> d;
  computeQR(a,d);
  mulQT(a,b);
  solveR(a,d,b);
}
template <typename T>
void solveRef(const Eigen::Matrix<T,-1,-1>& a,Eigen::Matrix<T,-1,1>& b) {
  b=(a.transpose()*a).ldlt().solve(a.transpose()*b).eval();
}
}

#endif
