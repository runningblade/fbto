#include "Symmetry.cuh"
#include "FBTOGPU.cuh"
#include "DebugGradient.h"
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

namespace FBTO {
namespace GPU {
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
template <int dim>
__global__ void applySymmetry(int X,int Y,int Z,Eigen::Matrix<int,3,1> stride,T* diffK) {
  int DIM[3]= {X,Y,Z};
  int DIMS[3]= {X,Y,Z};
  DIMS[dim]=(DIMS[dim]+1)/2;
  //current dimension
  int x=blockIdx.x*blockDim.x+threadIdx.x;
  int y=blockIdx.y*blockDim.y+threadIdx.y;
  int z=blockIdx.z*blockDim.z+threadIdx.z;
  if(x>=DIMS[0] || y>=DIMS[1] || z>=DIMS[2])
    return;
  //apply
  Eigen::Matrix<int,3,1> id(x,y,z);
  int id0=stride.dot(id);
  id[dim]=DIM[dim]-1-id[dim];
  int id1=stride.dot(id);
  diffK[id0]=diffK[id1]=(diffK[id0]+diffK[id1])/2;
}
template <int dim>
void applySymmetry(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,thrust::device_vector<T>& diffK) {
#define BLK 4
  int DIMS[3]= {X,Y,Z};
  DIMS[dim]=(DIMS[dim]+1)/2;
  int BLK3D=Z==1?1:BLK;
  dim3 nrBlk(NUM_BLOCK(DIMS[0],BLK),NUM_BLOCK(DIMS[1],BLK),NUM_BLOCK(DIMS[2],BLK3D));
  dim3 szBlk(BLK,BLK,BLK3D);
  applySymmetry<dim><<<nrBlk,szBlk>>>(X,Y,Z,stride,thrust::raw_pointer_cast(diffK.data()));
#undef BLK
}
template <int dim>
void applySymmetry(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,Vec& diffK) {
  thrust::device_vector<T> diffKGPU(diffK.begin(),diffK.end());
  applySymmetry<dim>(X,Y,Z,stride,diffKGPU);
  thrust::copy(diffKGPU.begin(),diffKGPU.end(),diffK.begin());
}
void applySymmetry(const Eigen::Matrix<bool,3,1>& mask,int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,thrust::device_vector<T>& diffK) {
  if(mask[0])
    applySymmetry<0>(X,Y,Z,stride,diffK);
  if(mask[1])
    applySymmetry<1>(X,Y,Z,stride,diffK);
  if(mask[2])
    applySymmetry<2>(X,Y,Z,stride,diffK);
}
}
//CPU
void Symmetry::applySymmetry(const Eigen::Matrix<bool,3,1>& mask,int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,Vec& diffK) {
  if(mask[0])
    applySymmetryX(X,Y,Z,stride,diffK);
  if(mask[1])
    applySymmetryY(X,Y,Z,stride,diffK);
  if(mask[2])
    applySymmetryZ(X,Y,Z,stride,diffK);
}
template <int dim>
void Symmetry::applySymmetry<dim>(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,Vec& diffK) {
  int DIM[3]= {X,Y,Z};
  int DIMS[3]= {X,Y,Z};
  DIMS[dim]=(DIMS[dim]+1)/2;
  OMP_PARALLEL_FOR_
  for(int x=0; x<DIMS[0]; x++)
    for(int y=0; y<DIMS[1]; y++)
      for(int z=0; z<DIMS[2]; z++) {
        Eigen::Matrix<int,3,1> id(x,y,z);
        int id0=stride.dot(id);
        id[dim]=DIM[dim]-1-id[dim];
        int id1=stride.dot(id);
        diffK[id0]=diffK[id1]=(diffK[id0]+diffK[id1])/2;
      }
}
void Symmetry::applySymmetryX(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,Vec& diffK) {
  applySymmetry<0>(X,Y,Z,stride,diffK);
}
void Symmetry::applySymmetryY(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,Vec& diffK) {
  applySymmetry<1>(X,Y,Z,stride,diffK);
}
void Symmetry::applySymmetryZ(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,Vec& diffK) {
  applySymmetry<2>(X,Y,Z,stride,diffK);
}
void Symmetry::debugGPU(int X,int Y,int Z) {
  DEFINE_NUMERIC_DELTA_T(T)
  Eigen::Matrix<int,3,1> stride(Y*Z,Z,1);
  Vec diffK=Vec::Random(X*Y*Z),diffKRef=diffK;
  //X
  applySymmetryX(X,Y,Z,stride,diffKRef);
  GPU::applySymmetry<0>(X,Y,Z,stride,diffK);
  DEBUG_GRADIENT("SymmetryXGPU",diffK.norm(),(diffK-diffKRef).norm())
  //Y
  applySymmetryY(X,Y,Z,stride,diffKRef);
  GPU::applySymmetry<1>(X,Y,Z,stride,diffK);
  DEBUG_GRADIENT("SymmetryYGPU",diffK.norm(),(diffK-diffKRef).norm())
  //Z
  if(Z>1) {
    applySymmetryZ(X,Y,Z,stride,diffKRef);
    GPU::applySymmetry<2>(X,Y,Z,stride,diffK);
    DEBUG_GRADIENT("SymmetryZGPU",diffK.norm(),(diffK-diffKRef).norm())
  }
}
}
