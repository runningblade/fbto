#include "FBTO.h"
#include "FBTOGPU.cuh"
#include "Symmetry.cuh"
#include <qpOASES.hpp>
#include "VTKWriter.h"
#include "FBTOEnergy.h"
#include "ParallelVector.h"
#include "HouseholderQR.h"
#include "DebugGradient.h"
#include "Timing.cuh"
#include <iostream>
#include <iomanip>

namespace FBTO {
template <int DIM,int POWRHO>
FBTOSolver<DIM,POWRHO>::FBTOSolver(T lambda,T mu,T minFill,T sumFillRel,int x,int y,int z)
  :_minFill(minFill),_sumFill(sumFillRel*x*y*z),_x(x),_y(y),_z(z),_D(20),_symmetry(false,false,false),_fp(NULL) {
  ASSERT((z==1)+(DIM!=2)==1)
  _strideF=Eigen::Matrix<int,3,1>(y*z,z,1);
  _strideU=Eigen::Matrix<int,3,1>(yU()*zU(),zU(),1);
  _maxIter=500;
  _reportInterval=100;
  _tol=1e-2;
  _tolX=1e-4;
  //K
  FBTOEnergy<DIM> e(lambda,mu,3);
  _K=e.Hessian();
}
template <int DIM,int POWRHO>
FBTOSolver<DIM,POWRHO>::~FBTOSolver() {
  if(_fp)
    fclose(_fp);
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::setMask(std::function<bool(Eigen::Matrix<int,3,1>)> fix) {
  _mask.clear();
  std::cout << "Masking: ";
  for(int x=0; x<xU(); x++)
    for(int y=0; y<yU(); y++)
      for(int z=0; z<zU(); z++)
        if(fix(Eigen::Matrix<int,3,1>(x,y,z))) {
          std::cout << "(" << x << "," << y << "," << z << ") ";
          _mask.insert(Eigen::Matrix<int,3,1>(x,y,z));
        }
  std::cout << std::endl;
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::setFMask(std::function<bool(Eigen::Matrix<int,3,1>)> fix) {
  _fMask.reset(new Eigen::Matrix<bool,-1,1>);
  _fMask->setZero(_x*_y*_z);
  for(int x=0; x<_x; x++)
    for(int y=0; y<_y; y++)
      for(int z=0; z<_z; z++) {
        Eigen::Matrix<int,3,1> id(x,y,z);
        if(fix(id))
          _fMask->coeffRef(_strideF.dot(id))=1;
      }
  //identify must-mask nodes
  Eigen::Matrix<bool,-1,1> mask;
  mask.setOnes(xU()*yU()*zU());
  for(int x=0; x<_x; x++)
    for(int y=0; y<_y; y++)
      for(int z=0; z<_z; z++)
        if(!fix(Eigen::Matrix<int,3,1>(x,y,z)))
          for(int d=0; d<(1<<DIM); d++) {
            Eigen::Matrix<int,3,1> id(x,y,z);
            if(d&1)
              id[0]++;
            if(d&2)
              id[1]++;
            if(d&4)
              id[2]++;
            mask[_strideU.dot(id)]=0;
          }
  //mask remaining nodes
  for(int x=0; x<xU(); x++)
    for(int y=0; y<yU(); y++)
      for(int z=0; z<zU(); z++) {
        Eigen::Matrix<int,3,1> id(x,y,z);
        if(mask[_strideU.dot(id)])
          _mask.insert(id);
      }
  if(_filter)
    _filter->setFMask(_x,_y,_z,_strideF,_fMask);
  _sumFill=_sumFill*(_fMask->size()-_fMask->cast<int>().sum())/(T)_fMask->size();
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::setFilter(std::shared_ptr<Filter<DIM>> filter) {
  _filter=filter;
  if(_filter)
    _filter->setFMask(_x,_y,_z,_strideF,_fMask);
}
template <int DIM,int POWRHO>
bool FBTOSolver<DIM,POWRHO>::isFMasked(int x,int y,int z) const {
  if(!_fMask)
    return false;
  return (*_fMask)[_strideF.dot(Eigen::Matrix<int,3,1>(x,y,z))];
}
template <int DIM,int POWRHO>
bool FBTOSolver<DIM,POWRHO>::isMasked(int x,int y,int z) const {
  return _mask.find(Eigen::Matrix<int,3,1>(x,y,z))!=_mask.end();
}
template <int DIM,int POWRHO>
Eigen::Matrix<int,3,1> FBTOSolver<DIM,POWRHO>::strideF() const {
  return _strideF;
}
template <int DIM,int POWRHO>
Eigen::Matrix<int,3,1> FBTOSolver<DIM,POWRHO>::strideU() const {
  return _strideU;
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::nrValidCell() const {
  if(_fMask)
    return _fMask->size()-_fMask->cast<int>().sum();
  else return _x*_y*_z;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::T FBTOSolver<DIM,POWRHO>::minFill() const {
  return _minFill;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::T FBTOSolver<DIM,POWRHO>::sumFill() const {
  return _sumFill;
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::x() const {
  return _x;
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::y() const {
  return _y;
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::z() const {
  return _z;
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::xU() const {
  return _x+1;
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::yU() const {
  return _y+1;
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::zU() const {
  return DIM==2?1:_z+1;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::T FBTOSolver<DIM,POWRHO>::tol() const {
  return _tol;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::T& FBTOSolver<DIM,POWRHO>::tol() {
  return _tol;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::T FBTOSolver<DIM,POWRHO>::tolX() const {
  return _tolX;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::T& FBTOSolver<DIM,POWRHO>::tolX() {
  return _tolX;
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::D() const {
  return _D;
}
template <int DIM,int POWRHO>
int& FBTOSolver<DIM,POWRHO>::D() {
  return _D;
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::maxIter() const {
  return _maxIter;
}
template <int DIM,int POWRHO>
int& FBTOSolver<DIM,POWRHO>::maxIter() {
  return _maxIter;
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::reportInterval() const {
  return _reportInterval;
}
template <int DIM,int POWRHO>
int& FBTOSolver<DIM,POWRHO>::reportInterval() {
  return _reportInterval;
}
template <int DIM,int POWRHO>
const Eigen::Matrix<bool,3,1>& FBTOSolver<DIM,POWRHO>::symmetry() const {
  return _symmetry;
}
template <int DIM,int POWRHO>
Eigen::Matrix<bool,3,1>& FBTOSolver<DIM,POWRHO>::symmetry() {
  return _symmetry;
}
template <int DIM,int POWRHO>
const std::string& FBTOSolver<DIM,POWRHO>::writePath() const {
  return _writePath;
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::writeInterval() const {
  return _writeInterval;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::T FBTOSolver<DIM,POWRHO>::writeMul() const {
  return _writeMul;
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::setWrite(const std::string& writePath,int writeInterval,T writeMul,T writeBoundary) {
  if(writePath!=_writePath && !writePath.empty()) {
    //close old file
    if(_fp)
      fclose(_fp);
    //open new file
    ASSERT(writeInterval>0 && writeMul>=0);
    recreate(writePath);
    _fp=freopen((writePath+"/output.txt").c_str(),"w",stdout);
  }
  _writePath=writePath;
  _writeInterval=writeInterval;
  _writeMul=writeMul;
  _writeBoundary=writeBoundary;
}
//multiply-K
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::debugBuildK() {
  randomizeMask();
  Vec fill=Vec::Random(colsFill());
  Vec b=Vec::Random(cols());
  Vec Kb=mulK(fill,b),KbBF=mulKBF(fill,b);
  DEFINE_NUMERIC_DELTA_T(T)
  DEBUG_GRADIENT("buildK",Kb.norm(),(Kb-KbBF).norm())
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::debugBuildKGPU() {
  randomizeMask();
  Vec fill=Vec::Random(colsFill());
  Vec b=Vec::Random(cols());
  Vec KbRef=mulK(fill,b);

  GPU::init(DIM,POWRHO,_minFill,_sumFill,_x,_y,_z,_D,_maxIter,_reportInterval,_tol,_tolX,_symmetry);
  GPU::assignK(_K);
  GPU::setMask(_mask,_fMask);
  Vec Kb=GPU::mulK(fill,b);
  DEFINE_NUMERIC_DELTA_T(T)
  DEBUG_GRADIENT("buildKGPU",Kb.norm(),(Kb-KbRef).norm())
  GPU::finalize();
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::mulK(const Vec& fill,const Vec& b) const {
  ParallelMatrix<Vec> Kb(Vec::Zero(b.size()));
  OMP_PARALLEL_FOR_
  for(int x=0; x<_x; x++)
    for(int y=0; y<_y; y++)
      for(int z=0; z<_z; z++) {
        T f=fill[_strideF.dot(Eigen::Matrix<int,3,1>(x,y,z))];
        T fPow=std::pow(f,POWRHO);
        addBlk(Kb.getMatrixI(),_K*getBlk(b,x,y,z)*fPow,x,y,z);
      }
  return mask(Kb.getMatrix());
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::mulKBF(const Vec& fill,const Vec& b,SMatT* KRet) const {
  STrips trips;
  OMP_PARALLEL_FOR_
  for(int x=0; x<_x; x++)
    for(int y=0; y<_y; y++)
      for(int z=0; z<_z; z++) {
        T f=fill[_strideF.dot(Eigen::Matrix<int,3,1>(x,y,z))];
        T fPow=std::pow(f,POWRHO);
        addBlk(trips,fPow,x,y,z);
      }
  for(auto m:_mask)
    addBlockId<T>(trips,_strideU.dot(m)*DIM,_strideU.dot(m)*DIM,DIM,1);
  SMatT K;
  K.resize(b.size(),b.size());
  K.setFromTriplets(trips.begin(),trips.end());
  if(KRet)
    *KRet=K;
  return mask(K*mask(b));
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::cols() const {
  return xU()*yU()*zU()*DIM;
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::colsFill() const {
  return _x*_y*_z;
}
template <int DIM,int POWRHO>
int FBTOSolver<DIM,POWRHO>::solveK(Vec& u,const Vec& fill,const Vec& b,T tol) const {
  if(tol<=0) {
    SMatT K;
    mulKBF(fill,b,&K);
    SparseSolver sol(K);
    //std::cout << "Factorizing a " << K.rows() << "X" << K.rows() << " matrix!" << std::endl;
    if(sol.info()!=Eigen::Success) {
      std::cout << "Cholesky failed!" << std::endl;
      exit(EXIT_FAILURE);
    }
    u=mask(sol.solve(b));
    return -1;
  } else {
    long int iters=1e6;
    MulK<DIM,POWRHO> mulK(*this,fill);
    Eigen::internal::conjugate_gradient(mulK,b,u,mulK,iters,tol);
    u=mask(u);
    return iters;
  }
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::solveKGD(Vec& u,const Vec& fill,const Vec& b,T beta) const {
  u=mask(u-beta*(mulK(fill,u)-b));
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::solveKKrylov(Vec& u,const Vec& fill,const Vec& b,T beta,int n) const {
  static MatT Kb,KbTmp;
  Kb.resize(b.size(),n);
  Vec RHS=mulK(fill,u)-b,coef=Vec::Zero(n);
  //we use coef to normalize columns of K^i*RHS, avoiding infinite values
  Kb.col(0)=mulK(fill,RHS);
  coef[0]=1/RHS.norm();
  Kb.col(0)*=coef[0];
  for(int i=1; i<Kb.cols(); i++) {
    Kb.col(i)=mulK(fill,Kb.col(i-1));
    coef[i]=1/Kb.col(i-1).norm();
    Kb.col(i)*=coef[i];
  }
  //compute coefficient
  Vec c;
  solveQR(KbTmp=Kb,c=RHS);//Kb.householderQr().solve(RHS);
  Vec sol=RHS*c[0]*coef[0];
  for(int i=1; i<Kb.cols(); i++)
    sol+=Kb.col(i-1)*c[i]*coef[i];
  u-=beta*sol;
  RHS=mulK(fill,u)-b;
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::debugSolveKGPU() {
  for(int type=0; type<3; type++) {
    randomizeMask();
    Vec fill=(Vec::Random(colsFill())+Vec::Ones(colsFill()))/2;
    Vec b=mask(Vec::Random(cols())),xRef=Vec::Zero(cols()),x=Vec::Zero(cols());
    switch(type) {
    case 0:
      solveK(xRef,fill,b,DEFAULT_CG_TOL_INIT);
      break;
    case 1:
      solveKGD(xRef,fill,b,0.5);
      break;
    case 2:
      solveKKrylov(xRef,fill,b,0.5,10);
      break;
    }

    GPU::init(DIM,POWRHO,_minFill,_sumFill,_x,_y,_z,_D,_maxIter,_reportInterval,_tol,_tolX,_symmetry);
    GPU::assignK(_K);
    GPU::setMask(_mask,_fMask);
    switch(type) {
    case 0:
      GPU::solveK(x,fill,b,DEFAULT_CG_TOL_INIT);
      break;
    case 1:
      GPU::solveKGD(x,fill,b,0.5);
      break;
    case 2:
      GPU::solveKKrylov(x,fill,b,0.5,10);
      break;
    }
    DEFINE_NUMERIC_DELTA_T(T)
    DEBUG_GRADIENT("solveKGPU",x.norm(),(x-xRef).norm())
    GPU::finalize();
  }
}
//differentiation
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::modifyF(const Vec& f,const VecDT& sf,const Vec& fill) const {
  if(sf.isZero())
    return f;
  Vec ret=f;
  OMP_PARALLEL_FOR_
  for(int x=0; x<_x; x++)
    for(int y=0; y<_y; y++)
      for(int z=0; z<_z; z++) {
        T fillI=std::pow(fill[_strideF.dot(Eigen::Matrix<int,3,1>(x,y,z))],POWRHO);
        for(int d=0; d<(1<<DIM); d++) {
          Eigen::Matrix<int,3,1> id(x,y,z);
          if(d&1)
            id[0]++;
          if(d&2)
            id[1]++;
          if(d&4)
            id[2]++;
          if(_mask.find(id)==_mask.end())
            ret.template segment<DIM>(_strideU.dot(id)*DIM)+=sf*fillI;
        }
      }
  return ret;
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::debugModifyFGPU() {
  randomizeMask();
  VecDT sf=VecDT::Random();
  Vec fill=Vec::Random(colsFill());
  Vec force=Vec::Random(cols());
  Vec modifiedFRef=modifyF(force,sf,fill);

  GPU::init(DIM,POWRHO,_minFill,_sumFill,_x,_y,_z,_D,_maxIter,_reportInterval,_tol,_tolX,_symmetry);
  GPU::assignK(_K);
  GPU::setMask(_mask,_fMask);
  Vec modifiedF=GPU::modifyF(force,sf,fill);
  DEFINE_NUMERIC_DELTA_T(T)
  DEBUG_GRADIENT("modifyFGPU",modifiedF.norm(),(modifiedF-modifiedFRef).norm())
  GPU::finalize();
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::diffK(const Vec& fill,const Vec& u,const VecDT& sf,bool preserveSum) const {
  Vec ret=Vec::Zero(colsFill());
  OMP_PARALLEL_FOR_
  for(int x=0; x<_x; x++)
    for(int y=0; y<_y; y++)
      for(int z=0; z<_z; z++) {
        T f=fill[_strideF.dot(Eigen::Matrix<int,3,1>(x,y,z))];
        T fPowDiff=std::pow(f,POWRHO-1)*POWRHO;
        T& retCell=ret[_strideF.dot(Eigen::Matrix<int,3,1>(x,y,z))];
        //retCell=0.5*u^T*DKDv*u
        retCell=getBlk(u,x,y,z).dot(_K*getBlk(u,x,y,z))*fPowDiff/2;
        //retCell-=u^T*DfDv
        if(sf.isZero())
          continue;
        for(int d=0; d<(1<<DIM); d++) {
          Eigen::Matrix<int,3,1> id(x,y,z);
          if(d&1)
            id[0]++;
          if(d&2)
            id[1]++;
          if(d&4)
            id[2]++;
          if(_mask.find(id)==_mask.end())
            retCell-=u.template segment<DIM>(_strideU.dot(id)*DIM).dot(sf)*fPowDiff;
        }
      }
  if(preserveSum)
    if(fill.sum()<_sumFill-1) {
      ret=compactFMask(ret);
      ret.array()-=ret.mean();
      ret=uncompactFMask(ret);
    }
  return ret;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::diffK(const Vec& fill,const Vec& u,const Vec& u2,const VecDT&) const {
  Vec ret=Vec::Zero(colsFill());
  OMP_PARALLEL_FOR_
  for(int x=0; x<_x; x++)
    for(int y=0; y<_y; y++)
      for(int z=0; z<_z; z++) {
        T f=fill[_strideF.dot(Eigen::Matrix<int,3,1>(x,y,z))];
        T fPowDiff=std::pow(f,POWRHO-1)*POWRHO;
        ret[_strideF.dot(Eigen::Matrix<int,3,1>(x,y,z))]=getBlk(u,x,y,z).dot(_K*getBlk(u2,x,y,z))*fPowDiff;
      }
  return ret;
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::debugDiffKGPU(int nU) {
  randomizeMask();
  std::vector<Vec> u(nU);
  MatDXT sf=MatDXT::Zero(DIM,nU);
  Vec fill=Vec::Random(colsFill());
  Vec diffRef=Vec::Zero(colsFill());
  for(int fIndex=0; fIndex<nU; fIndex++) {
    u[fIndex]=mask(Vec::Random(cols()));
    sf.col(fIndex)=VecDT::Random();
    diffRef+=diffK(fill,u[fIndex],sf.col(fIndex),true);
  }
  diffRef/=nU;

  GPU::init(DIM,POWRHO,_minFill,_sumFill,_x,_y,_z,_D,_maxIter,_reportInterval,_tol,_tolX,_symmetry);
  GPU::assignK(_K);
  GPU::setMask(_mask,_fMask);
  Vec diff=GPU::diffK(fill,u,sf,true);
  DEFINE_NUMERIC_DELTA_T(T)
  DEBUG_GRADIENT("diffKGPU",diff.norm(),(diff-diffRef).norm())
  GPU::finalize();
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::debugDiffK(int nU,bool useSF) {
  randomizeMask();
  MatT f=MatT::Random(cols(),nU);
  MatDXT sf=MatDXT::Zero(DIM,nU);
  Vec fill=(Vec::Random(colsFill())+Vec::Ones(colsFill()))/2;
  Vec diffRef=Vec::Zero(colsFill());
  if(useSF)
    sf.setRandom();
  for(int fIndex=0; fIndex<nU; fIndex++) {
    Vec u=Vec::Zero(cols());
    solveK(u,fill,modifyF(f.col(fIndex),sf.col(fIndex),fill));
    diffRef+=diffK(fill,u,sf.col(fIndex),true);
  }
  diffRef/=nU;

  DEFINE_NUMERIC_DELTA_T(T)
  Vec DFill=Vec::Random(colsFill());
  T e=energy(f,sf,fill);
  T e2=energy(f,sf,fill+DFill*DELTA);
  DEBUG_GRADIENT("diffK",diffRef.dot(DFill),diffRef.dot(DFill)+(e2-e)/DELTA)
}
//projection
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::projX(const Vec& fill) const {
  //handle: lambda,left
  std::vector<std::pair<T,bool>> vss;
  for(auto f:fill) {
    vss.push_back(std::make_pair(_minFill-f,true));
    vss.push_back(std::make_pair(1-f,false));
  }
  int active=1;
  T total=_minFill*fill.size();
  std::sort(vss.begin(),vss.end(),[&](std::pair<T,bool> a,std::pair<T,bool> b) {
    return a.first<b.first;
  });
  for(int i=1; i<(int)vss.size(); i++) {
    //std::cout << "activeCPU[" << i << "]=" << active << std::endl;
    total+=active*(vss[i].first-vss[i-1].first);
    //std::cout << "totalCPU[" << i << "]=" << total << std::endl;
    if(total>=_sumFill) {
      T lambda=(_sumFill-total)/active+vss[i].first;
      //std::cout << "lambdaCPU=" << lambda << std::endl;
      return (fill.array()+lambda).max(_minFill).min(1).matrix();
    } else if(vss[i].second)
      active++;
    else active--;
  }
  return fill.cwiseMax(_minFill).cwiseMin(1);
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::projXBF(const Vec& fill) const {
  qpOASES::QProblem sol(fill.size(),1,qpOASES::HST_IDENTITY);
  qpOASES::Options op=sol.getOptions();
  op.printLevel=qpOASES::PL_NONE;
  sol.setOptions(op);
  Eigen::Matrix<double,-1,1> g=-fill.template cast<double>();
  Eigen::Matrix<double,-1,1> lb,ub,ubA,A;
  lb.setConstant(fill.size(),_minFill);
  ub.setOnes(fill.size());
  ubA.setConstant(1,_sumFill);
  A.setOnes(fill.size());
  int nWSR=1e7;
  qpOASES::returnValue ret=sol.init(NULL,g.data(),A.data(),lb.data(),ub.data(),NULL,ubA.data(),nWSR);
  if(ret!=qpOASES::SUCCESSFUL_RETURN) {
    std::cout << "qpOASES failed ret=" << ret << "!" << std::endl;
    exit(EXIT_FAILURE);
  }
  //get solution
  Eigen::Matrix<double,-1,1> x;
  x.resize(fill.size());
  sol.getPrimalSolution(x.data());
  return x.template cast<T>();
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::projXFMask(const Vec& fill) const {
  Vec ret=compactFMask(fill);
  ret=projX(ret);
  return uncompactFMask(ret);
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::debugProjX(const Vec* at) const {
  Vec fill,fill2;
  if(at) {
    fill=fill2=*at;
  } else {
    fill=(Vec::Random(colsFill())+Vec::Ones(colsFill()))/2;
    while(fill.sum()<=_sumFill)
      fill*=2;
    fill2=fill;
  }
  fill=projX(fill);
  fill2=projXBF(fill2);
  std::cout << "projXNorm=" << fill.norm() << " projXNormErr=" << (fill-fill2).norm() << std::endl;
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::debugProjXGPU(const Vec* at) {
  Vec fill,fill2;
  if(at) {
    fill=fill2=*at;
  } else {
    fill=(Vec::Random(colsFill())+Vec::Ones(colsFill()))/2;
    while(fill.sum()<=_sumFill)
      fill*=2;
    fill2=fill;
  }
  fill=projX(fill);

  GPU::init(DIM,POWRHO,_minFill,_sumFill,_x,_y,_z,_D,_maxIter,_reportInterval,_tol,_tolX,_symmetry);
  GPU::assignK(_K);
  GPU::setMask(_mask,_fMask);
  fill2=GPU::projX(fill2);
  std::cout << "projXNorm=" << fill.norm() << " projXNormErr=" << (fill-fill2).norm() << std::endl;
  GPU::finalize();
}
//main routine
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::beginGPU() const {
  if(_filter)
    _filter->beginGPU();
  GPU::init(DIM,POWRHO,_minFill,_sumFill,_x,_y,_z,_D,_maxIter,_reportInterval,_tol,_tolX,_symmetry);
  GPU::initWrite(_writePath,_writeInterval,_writeMul,[&](T mul,const Vec* u,const Vec* f,const std::string& path,int k2,int fIndex) {
    writeVTK(mul,u,f,path+"/iter"+std::to_string(k2)+"_"+std::to_string(fIndex)+".vtk");
    if(fIndex==0)
      writeVTKCellCenter(f,path+"/iterC"+std::to_string(k2)+".vtk");
  });
  GPU::assignK(_K);
  GPU::setMask(_mask,_fMask);
  GPU::setCBSolver([&](Vec& u,const Vec& fill,const Vec& b) {
    solveK(u,fill,b,0);
  });
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::endGPU() const {
  GPU::finalize();
  if(_filter)
    _filter->endGPU();
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::initX() const {
  Vec ret=compactFMask(Vec::Zero(_x*_y*_z));
  ret.setConstant(_sumFill/ret.size());
  return uncompactFMask(ret);
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::filterForward(const Vec& f) const {
  Vec Cf=f;
  if(!_filter)
    return Cf;
  _filter->forward(x(),y(),z(),_strideF,f,Cf);
  return Cf;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::filterBackward(const Vec& diffCf) const {
  Vec difff=diffCf;
  if(!_filter)
    return difff;
  _filter->backward(x(),y(),z(),_strideF,diffCf,difff);
  return difff;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::solveUnilevel(const MatT& f,const MatDXT& sf,T alpha0,T m) const {
  Vec x=initX();
  Vec xLast,xProj,xC=filterForward(x);
  std::vector<Vec,Eigen::aligned_allocator<Vec>> u(f.cols(),Vec::Zero(cols()));
  TBEG("Grand-Timing");
  for(int k=1,k2=0;; k++) {
    TBEG("Unilevel-Iteration");
    T alphak=alpha0/std::pow<T>(k,m);
    int iters=0;
    for(int fIndex=0; fIndex<f.cols(); fIndex++)
      iters+=solveK(u[fIndex],xC,modifyF(f.col(fIndex),sf.col(fIndex),xC),DEFAULT_CG_TOL);
    xLast=x;
    xProj=Vec::Zero(colsFill());
    for(int fIndex=0; fIndex<f.cols(); fIndex++)
      xProj+=diffK(xC,u[fIndex],sf.col(fIndex));    //reuse xProj
    xProj/=f.cols();                                //average gradient
    xC=filterBackward(xProj);                       //reuse xC
    x=x+alphak*xC;
    Symmetry::applySymmetry(_symmetry,_x,_y,_z,_strideF,x);
    xProj=projXFMask(x);
    xProj.swap(x);
    xC=filterForward(x);
    TEND();
    if(!_writePath.empty() && (k%_writeInterval)==0) {
      const auto& xCPU=OUTPUT_FILTERED?xC:x;
      for(int fIndex=0; fIndex<f.cols(); fIndex++)
        writeVTK(_writeMul,&u[fIndex],&xCPU,_writePath+"/iter"+std::to_string(k2)+"_"+std::to_string(fIndex)+".vtk");
      writeVTKCellCenter(&xCPU,_writePath+"/iterC"+std::to_string(k2++)+".vtk");
    }
    if((k%std::abs(_reportInterval))==0) {
      T relDiff=(x-xLast).template lpNorm<NORM_TYPE>()/alphak;
      T relDiffCellwise=(NORM_TYPE==2?relDiff:(x-xLast).template lpNorm<2>()/alphak)/(_x*_y*_z);
      std::cout << "Iter=" << std::setw(7) << k <<
                " alpha=" << std::setw(15) << alphak <<
                " lowLevelIter=" << std::setw(15) << (iters<0?std::string("Direct"):std::to_string(iters)) <<
                " relDifference=" << std::setw(15) << relDiff <<
                " relDifferenceCellwise=" << std::setw(15) << relDiffCellwise <<
                " elapsedTime=" << std::setw(15) << TENDVPeek();
      if(_reportInterval<0) {
        T E=0;
        for(int fIndex=0; fIndex<f.cols(); fIndex++) {
          Vec uTmp=u[fIndex];
          solveK(uTmp,xC,f.col(fIndex));
          E+=uTmp.dot(f.col(fIndex))/2;
        }
        std::cout << " energy=" << std::setw(15) << E;
      }
      std::cout << std::endl;
      if(relDiff*alphak<_tolX || k>=_maxIter)
        break;
    }
  }
  TENDV();
  return xC;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::solveUnilevel
(std::function<VecDT(Eigen::Matrix<int,3,1>,int)> force,
 std::function<VecDT(int)> selfForce,int nCase,T alpha0,T m) const {
  MatT f=MatT::Zero(cols(),nCase);
  MatDXT sf=MatDXT::Zero(DIM,nCase);
  for(int fIndex=0; fIndex<nCase; fIndex++) {
    for(int x=0; x<xU(); x++)
      for(int y=0; y<yU(); y++)
        for(int z=0; z<zU(); z++)
          f.col(fIndex).template segment<DIM>(_strideU.dot(Eigen::Matrix<int,3,1>(x,y,z))*DIM)=force(Eigen::Matrix<int,3,1>(x,y,z),fIndex);
    sf.col(fIndex)=selfForce(fIndex);
  }
  return solveUnilevel(f,sf,alpha0,m);
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::solveUnilevelGPU(const MatT& f,const MatDXT& sf,T alpha0,T m) const {
  beginGPU();
  Vec xC=GPU::solveUnilevel(f,sf,alpha0,m);
  endGPU();
  return xC;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::solveUnilevelGPU
(std::function<VecDT(Eigen::Matrix<int,3,1>,int)> force,
 std::function<VecDT(int)> selfForce,int nCase,T alpha0,T m) const {
  MatT f=MatT::Zero(cols(),nCase);
  MatDXT sf=MatDXT::Zero(DIM,nCase);
  for(int fIndex=0; fIndex<nCase; fIndex++) {
    for(int x=0; x<xU(); x++)
      for(int y=0; y<yU(); y++)
        for(int z=0; z<zU(); z++)
          f.col(fIndex).template segment<DIM>(_strideU.dot(Eigen::Matrix<int,3,1>(x,y,z))*DIM)=force(Eigen::Matrix<int,3,1>(x,y,z),fIndex);
    sf.col(fIndex)=selfForce(fIndex);
  }
  return solveUnilevelGPU(f,sf,alpha0,m);
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::solveBilevel(const MatT& f,const MatDXT& sf,T beta0,T alpha0,T m) const {
  Vec x=initX();
  Vec xLast,xProj,xC=filterForward(x);
  std::vector<Vec,Eigen::aligned_allocator<Vec>> u(f.cols(),Vec::Zero(cols()));
  if(_D>0)
    for(int fIndex=0; fIndex<f.cols(); fIndex++)
      solveK(u[fIndex],xC,f,0);
  TBEG("Grand-Timing");
  for(int k=1,k2=0;; k++) {
    TBEG("Bilevel-Iteration-D="+std::to_string(_D));
    T alphak=alpha0/std::pow<T>(k,m);
    for(int fIndex=0; fIndex<f.cols(); fIndex++)
      if(abs(_D)>1)
        solveKKrylov(u[fIndex],xC,modifyF(f.col(fIndex),sf.col(fIndex),xC),beta0,abs(_D));
      else solveKGD(u[fIndex],xC,modifyF(f.col(fIndex),sf.col(fIndex),xC),beta0);
    //solveK(u,xC,f);
    xLast=x;
    xProj=Vec::Zero(colsFill());
    for(int fIndex=0; fIndex<f.cols(); fIndex++)
      xProj+=diffK(xC,u[fIndex],sf.col(fIndex));    //reuse xProj
    xProj/=f.cols();                                //average gradient
    xC=filterBackward(xProj);                       //reuse xC
    x=x+alphak*xC;
    Symmetry::applySymmetry(_symmetry,_x,_y,_z,_strideF,x);
    xProj=projXFMask(x);
    xProj.swap(x);
    xC=filterForward(x);
    TEND();
    if(!_writePath.empty() && (k%_writeInterval)==0) {
      const auto& xCPU=OUTPUT_FILTERED?xC:x;
      for(int fIndex=0; fIndex<f.cols(); fIndex++)
        writeVTK(_writeMul,&u[fIndex],&xCPU,_writePath+"/iter"+std::to_string(k2)+"_"+std::to_string(fIndex)+".vtk");
      writeVTKCellCenter(&xCPU,_writePath+"/iterC"+std::to_string(k2++)+".vtk");
    }
    if((k%std::abs(_reportInterval))==0) {
      T lowLevel=0;
      for(int fIndex=0; fIndex<f.cols(); fIndex++)
        lowLevel+=(mulK(xC,u[fIndex])-f.col(fIndex)).template lpNorm<NORM_TYPE>();
      T relDiff=(x-xLast).template lpNorm<NORM_TYPE>()/alphak;
      T relDiffCellwise=(NORM_TYPE==2?relDiff:(x-xLast).template lpNorm<2>()/alphak)/(_x*_y*_z);
      std::cout << "Iter=" << std::setw(7) << k <<
                " alpha=" << std::setw(15) << alphak <<
                " beta=" << std::setw(15) << beta0 <<
                " lowLevelErr=" << std::setw(15) << lowLevel <<
                " relDifference=" << std::setw(15) << relDiff <<
                " relDifferenceCellwise=" << std::setw(15) << relDiffCellwise <<
                " elapsedTime=" << std::setw(15) << TENDVPeek();
      if(_reportInterval<0) {
        T E=0;
        for(int fIndex=0; fIndex<f.cols(); fIndex++) {
          Vec uTmp=u[fIndex];
          solveK(uTmp,xC,f.col(fIndex));
          E+=uTmp.dot(f.col(fIndex))/2;
        }
        std::cout << " energy=" << std::setw(15) << E;
      }
      std::cout << std::endl;
      if((lowLevel<_tol && relDiff*alphak<_tolX) || k>=_maxIter)
        break;
    }
  }
  TENDV();
  return xC;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::solveBilevel
(std::function<VecDT(Eigen::Matrix<int,3,1>,int)> force,
 std::function<VecDT(int)> selfForce,int nCase,T beta0,T alpha0,T m) const {
  MatT f=MatT::Zero(cols(),nCase);
  MatDXT sf=MatDXT::Zero(DIM,nCase);
  for(int fIndex=0; fIndex<nCase; fIndex++) {
    for(int x=0; x<xU(); x++)
      for(int y=0; y<yU(); y++)
        for(int z=0; z<zU(); z++)
          f.col(fIndex).template segment<DIM>(_strideU.dot(Eigen::Matrix<int,3,1>(x,y,z))*DIM)=force(Eigen::Matrix<int,3,1>(x,y,z),fIndex);
    sf.col(fIndex)=selfForce(fIndex);
  }
  return solveBilevel(f,sf,beta0,alpha0,m);
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::solveBilevelGPU(const MatT& f,const MatDXT& sf,T beta0,T alpha0,T m) const {
  beginGPU();
  Vec xC=GPU::solveBilevel(f,sf,beta0,alpha0,m);
  endGPU();
  return xC;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::solveBilevelGPU
(std::function<VecDT(Eigen::Matrix<int,3,1>,int)> force,
 std::function<VecDT(int)> selfForce,int nCase,T beta0,T alpha0,T m) const {
  MatT f=MatT::Zero(cols(),nCase);
  MatDXT sf=MatDXT::Zero(DIM,nCase);
  for(int fIndex=0; fIndex<nCase; fIndex++) {
    for(int x=0; x<xU(); x++)
      for(int y=0; y<yU(); y++)
        for(int z=0; z<zU(); z++)
          f.col(fIndex).template segment<DIM>(_strideU.dot(Eigen::Matrix<int,3,1>(x,y,z))*DIM)=force(Eigen::Matrix<int,3,1>(x,y,z),fIndex);
    sf.col(fIndex)=selfForce(fIndex);
  }
  return solveBilevelGPU(f,sf,beta0,alpha0,m);
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::writeVTK(T mul,const Vec* u,const Vec* f,const std::string& path) const {
  std::vector<Eigen::Matrix<T,3,1>> vss;
  std::vector<Eigen::Matrix<int,8,1>> hss;
  std::vector<T> dss;
  for(int x=0; x<xU(); x++)
    for(int y=0; y<yU(); y++)
      for(int z=0; z<zU(); z++) {
        Eigen::Matrix<T,3,1> pt(x,y,z);
        if(u)
          pt.template segment<DIM>(0)+=u->template segment<DIM>(Eigen::Matrix<int,3,1>(x,y,z).dot(_strideU)*DIM)*mul;
        vss.push_back(pt);
      }
#define ID(X,Y,Z) _strideU.dot(Eigen::Matrix<int,3,1>(X,Y,Z))
  for(int x=0; x<_x; x++)
    for(int y=0; y<_y; y++)
      for(int z=0; z<_z; z++)
        if(DIM==2) {
          hss.push_back(Eigen::Matrix<int,8,1>(ID(x,y,z),
                                               ID(x+1,y,z),
                                               ID(x+1,y+1,z),
                                               ID(x,y+1,z),
                                               -1,-1,-1,-1));
          if(f) {
            T fVal=f->coeff(_strideF.dot(Eigen::Matrix<int,3,1>(x,y,z)));
            T fPow=std::pow(fVal,POWRHO);
            dss.push_back(fPow);
          }
        } else {
          hss.push_back(Eigen::Matrix<int,8,1>(ID(x,y,z),
                                               ID(x+1,y,z),
                                               ID(x,y+1,z),
                                               ID(x+1,y+1,z),
                                               ID(x,y,z+1),
                                               ID(x+1,y,z+1),
                                               ID(x,y+1,z+1),
                                               ID(x+1,y+1,z+1)));
          if(f) {
            T fVal=f->coeff(_strideF.dot(Eigen::Matrix<int,3,1>(x,y,z)));
            T fPow=std::pow(fVal,POWRHO);
            dss.push_back(fPow);
          }
        }
#undef ID
  VTKWriter<T> os("displacement",path,true);
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(hss.begin(),hss.end(),DIM==2?VTKWriter<T>::QUAD:VTKWriter<T>::VOXEL);
  if(f)
    os.appendCustomData("fill",dss.begin(),dss.end());
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::writeVTKCellCenter(const Vec* f,const std::string& path) const {
  std::vector<Eigen::Matrix<T,3,1>> vss;
  std::vector<Eigen::Matrix<int,8,1>> hss;
  std::vector<T> dss;
  for(int x=0; x<_x; x++)
    for(int y=0; y<_y; y++)
      for(int z=0; z<_z; z++) {
        vss.push_back(Eigen::Matrix<T,3,1>(x,y,z));
        if(f) {
          T fVal=f->coeff(_strideF.dot(Eigen::Matrix<int,3,1>(x,y,z)));
          if(_writeBoundary>0)
            if(x==0 || x==_x-1 || y==0 || y==_y-1 || (DIM==3 && (z==0 || z==_z-1)))
              fVal=_writeBoundary;
          T fPow=std::pow(fVal,POWRHO);
          dss.push_back(fPow);
        }
      }
#define ID(X,Y,Z) _strideF.dot(Eigen::Matrix<int,3,1>(X,Y,Z))
  for(int x=0; x<_x-1; x++)
    for(int y=0; y<_y-1; y++)
      for(int z=0; z<std::max<int>(_z-1,1); z++)
        if(DIM==2) {
          hss.push_back(Eigen::Matrix<int,8,1>(ID(x,y,z),
                                               ID(x+1,y,z),
                                               ID(x+1,y+1,z),
                                               ID(x,y+1,z),
                                               -1,-1,-1,-1));
        } else {
          hss.push_back(Eigen::Matrix<int,8,1>(ID(x,y,z),
                                               ID(x+1,y,z),
                                               ID(x,y+1,z),
                                               ID(x+1,y+1,z),
                                               ID(x,y,z+1),
                                               ID(x+1,y,z+1),
                                               ID(x,y+1,z+1),
                                               ID(x+1,y+1,z+1)));
        }
#undef ID
  VTKWriter<T> os("displacement",path,true);
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(hss.begin(),hss.end(),DIM==2?VTKWriter<T>::QUAD:VTKWriter<T>::VOXEL);
  if(f)
    os.appendCustomPointData("fill",dss.begin(),dss.end());
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::T FBTOSolver<DIM,POWRHO>::energy(const MatT& f,const MatDXT& sf,const Vec& fill) const {
  T ret=0;
  for(int fIndex=0; fIndex<f.cols(); fIndex++) {
    Vec u=Vec::Zero(cols());
    Vec modifiedF=modifyF(f.col(fIndex),sf.col(fIndex),fill);
    solveK(u,fill,modifiedF);
    ret+=u.dot(modifiedF)/2;
  }
  return ret/f.cols();
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::T FBTOSolver<DIM,POWRHO>::energy
(std::function<VecDT(Eigen::Matrix<int,3,1>,int)> force,
 std::function<VecDT(int)> selfForce,int nCase,const Vec& fill) const {
  MatT f=MatT::Zero(cols(),nCase);
  MatDXT sf=MatDXT::Zero(DIM,nCase);
  for(int fIndex=0; fIndex<nCase; fIndex++) {
    for(int x=0; x<xU(); x++)
      for(int y=0; y<yU(); y++)
        for(int z=0; z<zU(); z++)
          f.col(fIndex).template segment<DIM>(_strideU.dot(Eigen::Matrix<int,3,1>(x,y,z))*DIM)=force(Eigen::Matrix<int,3,1>(x,y,z),fIndex);
    sf.col(fIndex)=selfForce(fIndex);
  }
  return energy(f,sf,fill);
}
//helper
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::VecDDT FBTOSolver<DIM,POWRHO>::getBlk(const Vec& u,int x,int y,int z) const {
#define VD(v,I) v.template segment<DIM>(I*DIM)
  VecDDT ret=VecDDT::Zero();
  for(int d=0; d<(1<<DIM); d++) {
    Eigen::Matrix<int,3,1> id(x,y,z);
    if(d&1)
      id[0]++;
    if(d&2)
      id[1]++;
    if(d&4)
      id[2]++;
    if(_mask.find(id)==_mask.end())
      VD(ret,d)=u.template segment<DIM>(_strideU.dot(id)*DIM);
  }
  return ret;
#undef VD
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::addBlk(Vec& u,const VecDDT& ui,int x,int y,int z) const {
#define VD(v,I) v.template segment<DIM>(I*DIM)
  for(int d=0; d<(1<<DIM); d++) {
    Eigen::Matrix<int,3,1> id(x,y,z);
    if(d&1)
      id[0]++;
    if(d&2)
      id[1]++;
    if(d&4)
      id[2]++;
    if(_mask.find(id)==_mask.end())
      u.template segment<DIM>(_strideU.dot(id)*DIM)+=VD(ui,d);
  }
#undef VD
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::addBlk(STrips& trips,T f,int x,int y,int z) const {
  for(int d=0; d<(1<<DIM); d++) {
    Eigen::Matrix<int,3,1> id(x,y,z);
    if(d&1)
      id[0]++;
    if(d&2)
      id[1]++;
    if(d&4)
      id[2]++;
    if(_mask.find(id)!=_mask.end())
      continue;
    for(int d2=0; d2<(1<<DIM); d2++) {
      Eigen::Matrix<int,3,1> id2(x,y,z);
      if(d2&1)
        id2[0]++;
      if(d2&2)
        id2[1]++;
      if(d2&4)
        id2[2]++;
      if(_mask.find(id2)!=_mask.end())
        continue;
      //add blk
      addBlock(trips,_strideU.dot(id)*DIM,_strideU.dot(id2)*DIM,_K.template block<DIM,DIM>(d*DIM,d2*DIM)*f);
    }
  }
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::uncompactFMask(const Vec& u) const {
  if(!_fMask)
    return u;
  Vec ret=Vec::Zero(_fMask->size());
  for(int i=0,j=0; i<_fMask->size(); i++)
    if(!(*_fMask)[i])
      ret[i]=u[j++];
  return ret;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::compactFMask(const Vec& u) const {
  if(!_fMask)
    return u;
  Vec ret=Vec::Zero(_fMask->size()-_fMask->cast<int>().sum());
  for(int i=0,j=0; i<_fMask->size(); i++)
    if(!(*_fMask)[i])
      ret[j++]=u[i];
    else {
      ASSERT(u[i]==0)
    }
  return ret;
}
template <int DIM,int POWRHO>
typename FBTOSolver<DIM,POWRHO>::Vec FBTOSolver<DIM,POWRHO>::mask(const Vec& u) const {
  Vec ret=u;
  for(auto m:_mask)
    ret.template segment<DIM>(_strideU.dot(m)*DIM).setZero();
  return ret;
}
template <int DIM,int POWRHO>
void FBTOSolver<DIM,POWRHO>::randomizeMask() {
  _mask.clear();
  for(int x=0; x<xU(); x++)
    for(int y=0; y<yU(); y++)
      for(int z=0; z<zU(); z++)
        if(rand()/(T)RAND_MAX<0.5)
          _mask.insert(Eigen::Matrix<int,3,1>(x,y,z));
}
//instance
template class FBTOSolver<2,1>;
template class FBTOSolver<2,2>;
template class FBTOSolver<2,3>;
template class FBTOSolver<2,4>;
template class FBTOSolver<3,1>;
template class FBTOSolver<3,2>;
template class FBTOSolver<3,3>;
template class FBTOSolver<3,4>;
}
