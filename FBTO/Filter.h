#ifndef FILTER_H
#define FILTER_H

#include "Utils.h"

namespace FBTO {
template <int DIM>
class Filter {
 public:
  typedef SCALAR_TYPE T;
  DECL_MAT_VEC_MAP_TYPES_T
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  Filter(T thickness);
  void setFMask(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,std::shared_ptr<Eigen::Matrix<bool,-1,1>> fMask);
  template <int dim>
  void forwardDIM(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const Vec& u,Vec& Fu) const;
  template <int dim>
  void backwardDIM(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const Vec& DEDFu,Vec& DEDu) const;
  void forward(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const Vec& u,Vec& Fu);
  void backward(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const Vec& DEDFu,Vec& DEDu);
  void debug(int X,int Y,int Z);
  void debugGPU(int X,int Y,int Z);
  void beginGPU();
  void endGPU();
 private:
  T stencil(T r) const;
  std::shared_ptr<Eigen::Matrix<bool,-1,1>> _fMask;
  std::vector<T> _stencil,_stencilL,_stencilR;
  Vec _FuX,_FuY;
  int _hsize;
  SMatT _C[DIM];
};
}

#endif
