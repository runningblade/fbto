#ifndef SYMMETRY_H
#define SYMMETRY_H

#include "Utils.h"

namespace FBTO {
class Symmetry {
 public:
  typedef SCALAR_TYPE T;
  DECL_MAT_VEC_MAP_TYPES_T
  static void applySymmetry(const Eigen::Matrix<bool,3,1>& mask,int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,Vec& diffK);
  template <int dim>
  static void applySymmetry(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,Vec& diffK);
  static void applySymmetryX(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,Vec& diffK);
  static void applySymmetryY(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,Vec& diffK);
  static void applySymmetryZ(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,Vec& diffK);
  static void debugGPU(int X,int Y,int Z);
};
}

#endif
