#include "OCMSolver.h"
#include "FBTOGPU.cuh"
#include "Symmetry.cuh"
#include "Timing.cuh"
#include <vector>
#include <iostream>
#include <iomanip>

namespace FBTO {
template <int DIM,int POWRHO>
OCMSolver<DIM,POWRHO>::OCMSolver(T lambda,T mu,T minFill,T sumFillRel,int x,int y,int z)
  :_sol(lambda,mu,minFill,sumFillRel,x,y,z),_useGPU(true) {}
template <int DIM,int POWRHO>
const typename OCMSolver<DIM,POWRHO>::Vec& OCMSolver<DIM,POWRHO>::fill() const {
  return _fill;
}
template <int DIM,int POWRHO>
FBTOSolver<DIM,POWRHO>& OCMSolver<DIM,POWRHO>::sol() {
  return _sol;
}
template <int DIM,int POWRHO>
void OCMSolver<DIM,POWRHO>::solve(std::function<VecDT(Eigen::Matrix<int,3,1>)> force,T move,T tolBinarySearch) {
  _f=Vec::Zero(_sol.cols());
  for(int x=0; x<_sol.xU(); x++)
    for(int y=0; y<_sol.yU(); y++)
      for(int z=0; z<_sol.zU(); z++)
        _f.template segment<DIM>(_sol.strideU().dot(Eigen::Matrix<int,3,1>(x,y,z))*DIM)=force(Eigen::Matrix<int,3,1>(x,y,z));
  //main loop
  TBEG("Grand-Timing");
  initProblem();
  _sol.beginGPU();
  for(int iter=0; iter<_sol.maxIter(); iter++) {
    //compute gradient
    Vec g=gradient(),fillNew;
    //report
    if((iter%_sol.writeInterval())==0) {
      const auto& fillCPU=OUTPUT_FILTERED?_fillC:_fill;
      _sol.writeVTK(_sol.writeMul(),&_u,&fillCPU,_sol.writePath()+"/iter"+std::to_string(iter)+".vtk");
    }
    if((iter%std::abs(_sol.reportInterval()))==0) {
      T optimality=(_sol.projX(_fill-move*g)-_fill).template lpNorm<NORM_TYPE>()/move;
      std::cout << "nrEval=" << std::setw(5) << iter
                << " energy=" << std::setw(15) << _u.dot(_f)/2
                << " gNorm=" << std::setw(15) << g.cwiseAbs().maxCoeff()
                << " optimality=" << std::setw(15) << optimality
                << " elapsedTime=" << std::setw(15) << TENDVPeek() << std::endl;
    }
    //termination
    if(g.template lpNorm<NORM_TYPE>()<_sol.tol())
      break;
    //update fill
    T l1=0,l2=10000,lmid;
    while(l2-l1>tolBinarySearch) {
      lmid=(l1+l2)/2;
      fillNew=_fill;
      for(int i=0;i<_fill.size();i++)
        fillNew[i]*=sqrt(g[i]/lmid);
      fillNew=fillNew.cwiseMax(_fill-Vec::Constant(_fill.size(),move)).cwiseMax(_lbv);//lb
      fillNew=fillNew.cwiseMin(_fill+Vec::Constant(_fill.size(),move)).cwiseMin(_ubv);//ub
      if(fillNew.sum()>_sol.sumFill()) {
        l1=lmid;
      } else {
        l2=lmid;
      }
    }
    _fill=fillNew;
    //apply symmetry
    Symmetry::applySymmetry(_sol.symmetry(),_sol.x(),_sol.y(),_sol.z(),_sol.strideF(),_fill);
  }
  _sol.endGPU();
  TENDV();
}
//helper
template <int DIM,int POWRHO>
void OCMSolver<DIM,POWRHO>::initProblem() {
  //Variables
  std::vector<double> lbv,ubv,init;
  for(int x=0; x<_sol.x(); x++)
    for(int y=0; y<_sol.y(); y++)
      for(int z=0; z<_sol.z(); z++) {
        if(_sol.isFMasked(x,y,z)) {
          lbv.push_back(0);
          ubv.push_back(0);
          init.push_back(0);
        } else {
          lbv.push_back(_sol.minFill());
          ubv.push_back(1);
          init.push_back(_sol.sumFill()/_sol.nrValidCell());
        }
      }
  _lbv=Eigen::Map<const Vec>(lbv.data(),lbv.size());
  _ubv=Eigen::Map<const Vec>(ubv.data(),ubv.size());
  _fill=Eigen::Map<const Vec>(init.data(),init.size());
}
template <int DIM,int POWRHO>
typename OCMSolver<DIM,POWRHO>::Vec OCMSolver<DIM,POWRHO>::gradient() {
  _fillC=_sol.filterForward(_fill);
  _u.setZero(_f.rows());
  if(_useGPU)
    FBTO::GPU::solveK(_u,_fillC,_sol.modifyF(_f,VecDT::Zero(),_fillC),DEFAULT_CG_TOL);
  else _sol.solveK(_u,_fillC,_sol.modifyF(_f,VecDT::Zero(),_fillC),DEFAULT_CG_TOL);
  Vec xProj=_sol.diffK(_fillC,_u,VecDT::Zero(),false);
  Vec grad=_sol.filterBackward(xProj);
  //apply FMask
  OMP_PARALLEL_FOR_
  for(int x=0; x<_sol.x(); x++)
    for(int y=0; y<_sol.y(); y++)
      for(int z=0; z<_sol.z(); z++)
        if(_sol.isFMasked(x,y,z))
          grad[_sol.strideF().dot(Eigen::Matrix<int,3,1>(x,y,z))]=0;
  return grad;
}
//instance
template class OCMSolver<2,1>;
template class OCMSolver<2,2>;
template class OCMSolver<2,3>;
template class OCMSolver<2,4>;
template class OCMSolver<3,1>;
template class OCMSolver<3,2>;
template class OCMSolver<3,3>;
template class OCMSolver<3,4>;
}
