#ifndef TOSOLVER_H
#define TOSOLVER_H

#include "Utils.h"
#include "Filter.h"
#include "CholmodSolver.h"
#include <unordered_set>
#include <memory>

namespace FBTO {
template <int DIM,int POWRHO>
class FBTOSolver {
 public:
  typedef SCALAR_TYPE T;
  DECL_MAT_VEC_MAP_TYPES_T
  //Dense
  typedef Eigen::Matrix<T,DIM,1> VecDT;
  typedef Eigen::Matrix<T,DIM,DIM> MatDT;
  typedef Eigen::Matrix<T,DIM,-1> MatDXT;
  typedef Eigen::Matrix<T,DIM*(1<<DIM),1> VecDDT;
  typedef Eigen::Matrix<T,DIM*(1<<DIM),DIM*(1<<DIM)> MatDDT;
  //Sparse
  typedef Eigen::Triplet<T,int> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
#ifdef WITH_CHOLMOD
  typedef CholmodSolver SparseSolver;
#else
  typedef Eigen::SimplicialLLT<SMatT> SparseSolver;
#endif
  FBTOSolver(T lambda,T mu,T minFill,T sumFillRel,int x,int y,int z=1);
  virtual ~FBTOSolver();
  void setMask(std::function<bool(Eigen::Matrix<int,3,1>)> fix);
  void setFMask(std::function<bool(Eigen::Matrix<int,3,1>)> fix);
  void setFilter(std::shared_ptr<Filter<DIM>> filter);
  bool isFMasked(int x,int y,int z) const;
  bool isMasked(int x,int y,int z) const;
  Eigen::Matrix<int,3,1> strideF() const;
  Eigen::Matrix<int,3,1> strideU() const;
  int nrValidCell() const;
  T minFill() const;
  T sumFill() const;
  int x() const;
  int y() const;
  int z() const;
  int xU() const;
  int yU() const;
  int zU() const;
  T tol() const;
  T& tol();
  T tolX() const;
  T& tolX();
  int D() const;
  int& D();
  int maxIter() const;
  int& maxIter();
  int reportInterval() const;
  int& reportInterval();
  const Eigen::Matrix<bool,3,1>& symmetry() const;
  Eigen::Matrix<bool,3,1>& symmetry();
  const std::string& writePath() const;
  int writeInterval() const;
  T writeMul() const;
  void setWrite(const std::string& writePath,int writeInterval,T writeMul,T writeBoundary=-1);
  //multiply-K
  void debugBuildK();
  void debugBuildKGPU();
  Vec mulK(const Vec& fill,const Vec& b) const;
  Vec mulKBF(const Vec& fill,const Vec& b,SMatT* KRet=NULL) const;
  int cols() const;
  int colsFill() const;
  int solveK(Vec& u,const Vec& fill,const Vec& b,T tol=DEFAULT_CG_TOL) const;
  void solveKGD(Vec& u,const Vec& fill,const Vec& b,T beta) const;
  void solveKKrylov(Vec& u,const Vec& fill,const Vec& b,T beta,int n) const;
  void debugSolveKGPU();
  //differentiation
  Vec modifyF(const Vec& f,const VecDT& sf,const Vec& fill) const;
  void debugModifyFGPU();
  Vec diffK(const Vec& fill,const Vec& u,const VecDT& sf,bool preserveSum=DEFAULT_PRESERVE_SUM) const;
  //this is for ALM solver, which does not support multi-case or self force
  Vec diffK(const Vec& fill,const Vec& u,const Vec& u2,const VecDT& sf) const;
  void debugDiffKGPU(int nU);
  void debugDiffK(int nU,bool useSF);
  //projection
  Vec projX(const Vec& fill) const;
  Vec projXBF(const Vec& fill) const;
  Vec projXFMask(const Vec& fill) const;
  void debugProjX(const Vec* at) const;
  void debugProjXGPU(const Vec* at);
  //main routine
  void beginGPU() const;
  void endGPU() const;
  Vec initX() const;
  Vec filterForward(const Vec& x) const;
  Vec filterBackward(const Vec& diffCx) const;
  Vec solveUnilevel(const MatT& f,const MatDXT& sf,T alpha0,T m=3.0/4.0) const;
  Vec solveUnilevel
  (std::function<VecDT(Eigen::Matrix<int,3,1>,int)> force,
   std::function<VecDT(int)> selfForce,int nCase,T alpha0,T m=3.0/4.0) const;
  Vec solveUnilevelGPU(const MatT& f,const MatDXT& sf,T alpha0,T m=3.0/4.0) const;
  Vec solveUnilevelGPU
  (std::function<VecDT(Eigen::Matrix<int,3,1>,int)> force,
   std::function<VecDT(int)> selfForce,int nCase,T alpha0,T m=3.0/4.0) const;
  Vec solveBilevel(const MatT& f,const MatDXT& sf,T beta0,T alpha0,T m=3.0/4.0) const;
  Vec solveBilevel
  (std::function<VecDT(Eigen::Matrix<int,3,1>,int)> force,
   std::function<VecDT(int)> selfForce,int nCase,T beta0,T alpha0,T m=3.0/4.0) const;
  Vec solveBilevelGPU(const MatT& f,const MatDXT& sf,T beta0,T alpha0,T m=3.0/4.0) const;
  Vec solveBilevelGPU
  (std::function<VecDT(Eigen::Matrix<int,3,1>,int)> force,
   std::function<VecDT(int)> selfForce,int nCase,T beta0,T alpha0,T m=3.0/4.0) const;
  void writeVTK(T mul,const Vec* u,const Vec* f,const std::string& path) const;
  void writeVTKCellCenter(const Vec* f,const std::string& path) const;
  T energy(const MatT& f,const MatDXT& sf,const Vec& fill) const;
  T energy(std::function<VecDT(Eigen::Matrix<int,3,1>,int)> force,
           std::function<VecDT(int)> selfForce,int nCase,const Vec& fill) const;
 private:
  VecDDT getBlk(const Vec& u,int x,int y,int z) const;
  void addBlk(Vec& u,const VecDDT& ui,int x,int y,int z) const;
  void addBlk(STrips& trips,T f,int x,int y,int z) const;
  Vec uncompactFMask(const Vec& u) const;
  Vec compactFMask(const Vec& u) const;
  Vec mask(const Vec& u) const;
  void randomizeMask();
  //data
  std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash<int>> _mask;
  Eigen::Matrix<int,3,1> _strideF,_strideU;
  std::string _writePath;
  T _minFill,_sumFill,_tol,_tolX,_writeMul,_writeBoundary;
  int _x,_y,_z,_D,_maxIter,_reportInterval,_writeInterval;
  std::shared_ptr<Eigen::Matrix<bool,-1,1>> _fMask;
  std::shared_ptr<Filter<DIM>> _filter;
  Eigen::Matrix<bool,3,1> _symmetry;
  MatDDT _K;
  FILE* _fp;
};
}

#endif
