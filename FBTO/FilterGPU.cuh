#include <Eigen/Dense>
#include "Utils.h"

namespace FBTO {
namespace GPU {
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
typedef Eigen::Matrix<T,-1,1> Vec;
typedef Eigen::Matrix<T,-1,-1> Mat;
extern void initFilter(int dim,int hsize);
extern void finalizeFilter();
extern void assignStencil(const std::vector<T>& stencil,const std::vector<T>& stencilL,const std::vector<T>& stencilR);
extern void assignC(Eigen::SparseMatrix<T,0,int> C[3]);
extern void forward(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const Vec& u,Vec& Fu);
extern void backward(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const Vec& DEDFu,Vec& DEDu);
}
}
