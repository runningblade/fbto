#include "FilterGPU.cuh"
#include "FBTOGPU.cuh"
#include <thrust/sort.h>
#include <thrust/scan.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/inner_product.h>
#include "Timing.cuh"
#include <iomanip>
#include <memory>

namespace FBTO {
namespace GPU {
#define MAX_STENCIL_SIZE 65
__constant__ T _stencil[MAX_STENCIL_SIZE];
__constant__ T _stencilL[MAX_STENCIL_SIZE];
__constant__ T _stencilR[MAX_STENCIL_SIZE];
template <int dim,int BLK,int BLKX,int BLKY,int BLKZ>
__global__ void forwardDim(int X,int Y,int Z,Eigen::Matrix<int,3,1> stride,int hsize,const T* u,T* Fu) {
  __shared__ T uBlk[MAX_STENCIL_SIZE-1+BLK];
  int xoff=blockIdx.x*BLKX,x=xoff+threadIdx.x;
  int yoff=blockIdx.y*BLKY,y=yoff+threadIdx.y;
  int zoff=blockIdx.z*BLKZ,z=zoff+threadIdx.z;
  int tid[3]= {threadIdx.x,threadIdx.y,threadIdx.z};
  Eigen::Matrix<int,3,1> minC(0,0,0),maxC(X-1,Y-1,Z-1);
  Eigen::Matrix<int,3,1> id=Eigen::Matrix<int,3,1>(x,y,z)-Eigen::Matrix<int,3,1>::Unit(dim)*hsize;
  Eigen::Matrix<int,3,1> idClamp=id.cwiseMax(minC).cwiseMin(maxC);
  //load uBlk
  uBlk[tid[dim]]=u[stride.dot(idClamp)];
  __syncthreads();
  //convolve
  if(tid[dim]>=hsize && tid[dim]<hsize+BLK && id==idClamp) {
    T total=0;
    for(int dd=-hsize,i=0; dd<=hsize; dd++,i++)
      total+=uBlk[tid[dim]+dd]*_stencil[i];
    Fu[stride.dot(id)]=total;
  }
}
template <int dim,int BLK,int BLKX,int BLKY,int BLKZ>
__global__ void backwardDim(int X,int Y,int Z,Eigen::Matrix<int,3,1> stride,int hsize,const T* DEDFu,T* DEDu) {
  __shared__ T uBlk[MAX_STENCIL_SIZE-1+BLK];
  int xoff=blockIdx.x*BLKX,x=xoff+threadIdx.x;
  int yoff=blockIdx.y*BLKY,y=yoff+threadIdx.y;
  int zoff=blockIdx.z*BLKZ,z=zoff+threadIdx.z;
  int tid[3]= {threadIdx.x,threadIdx.y,threadIdx.z};
  Eigen::Matrix<int,3,1> minC(0,0,0),maxC(X-1,Y-1,Z-1);
  Eigen::Matrix<int,3,1> id=Eigen::Matrix<int,3,1>(x,y,z)-Eigen::Matrix<int,3,1>::Unit(dim)*hsize;
  Eigen::Matrix<int,3,1> idClamp=id.cwiseMax(minC).cwiseMin(maxC);
  //load uBlk
  uBlk[tid[dim]]=DEDFu[stride.dot(idClamp)];
  __syncthreads();
  //convolve
  if(tid[dim]>=hsize && tid[dim]<hsize+BLK && id==idClamp) {
    T total=0;
    if(id[dim]==0)
      for(int dd=-hsize,i=0; dd<=hsize; dd++,i++)
        total+=uBlk[tid[dim]+dd]*_stencilL[i];
    else if(id[dim]==maxC[dim])
      for(int dd=-hsize,i=0; dd<=hsize; dd++,i++)
        total+=uBlk[tid[dim]+dd]*_stencilR[i];
    else
      for(int dd=-hsize,i=0; dd<=hsize; dd++,i++)
        if(id[dim]+dd>=0 && id[dim]+dd<=maxC[dim])
          total+=uBlk[tid[dim]+dd]*_stencil[i];
    DEDu[stride.dot(id)]=total;
  }
}
__global__ void mulC(int n,const int* rowoff,const int* colId,const T* coef,const T* input,T* output) {
  int x=blockIdx.x*blockDim.x+threadIdx.x;
  if(x>=n)
    return;
  T ret=0;
  for(int i=rowoff[x],j=rowoff[x+1]; i<j; i++)
    ret+=input[colId[i]]*coef[i];
  output[x]=ret;
}
//data
int _hsize=0;
struct Sparse {
  Sparse(Eigen::SparseMatrix<T,Eigen::RowMajor,int> m) {
    m.makeCompressed();
    _rowoff.assign(m.outerIndexPtr(),m.outerIndexPtr()+m.cols()+1);
    _colId.assign(m.innerIndexPtr(),m.innerIndexPtr()+m.nonZeros());
    _coef.assign(m.valuePtr(),m.valuePtr()+m.nonZeros());
  }
  void multiply(const thrust::device_vector<T>& input,thrust::device_vector<T>& output) const {
#define BLK 32
    dim3 nrBlk(NUM_BLOCK(rows(),BLK),1,1),szBlk(BLK,1,1);
    mulC<<<nrBlk,szBlk>>>
    (rows(),
     thrust::raw_pointer_cast(_rowoff.data()),
     thrust::raw_pointer_cast(_colId.data()),
     thrust::raw_pointer_cast(_coef.data()),
     thrust::raw_pointer_cast(input.data()),
     thrust::raw_pointer_cast(output.data()));
#undef BLK
  }
  int rows() const {
    return _rowoff.size()-1;
  }
  thrust::device_vector<int> _rowoff,_colId;
  thrust::device_vector<T> _coef;
};
std::shared_ptr<thrust::device_vector<T>> _FuX,_FuY;
std::shared_ptr<Sparse> _C[3],_CT[3];
//method
bool hasFilter() {
  return _hsize>0;
}
void initFilter(int dim,int hsize) {
  DIM=dim;
  _hsize=hsize;
  ASSERT(hsize<=MAX_STENCIL_SIZE/2);
  _FuX.reset(new thrust::device_vector<T>());
  _FuY.reset(new thrust::device_vector<T>());
  for(int d=0; d<3; d++)
    _C[d]=NULL;
  for(int d=0; d<3; d++)
    _CT[d]=NULL;
}
void finalizeFilter() {
  _hsize=0;
  _FuX=NULL;
  _FuY=NULL;
  for(int d=0; d<3; d++)
    _C[d]=NULL;
  for(int d=0; d<3; d++)
    _CT[d]=NULL;
}
void assignStencil(const std::vector<T>& stencil,const std::vector<T>& stencilL,const std::vector<T>& stencilR) {
  cudaMemcpyToSymbol(_stencil,stencil.data(),sizeof(T)*stencil.size());
  cudaMemcpyToSymbol(_stencilL,stencilL.data(),sizeof(T)*stencilL.size());
  cudaMemcpyToSymbol(_stencilR,stencilR.data(),sizeof(T)*stencilR.size());
}
void assignC(Eigen::SparseMatrix<T,0,int> C[3]) {
  for(int d=0; d<DIM; d++) {
    _C[d].reset(new Sparse(C[d]));
    _CT[d].reset(new Sparse(C[d].transpose()));
  }
}
template <int dim>
void forwardDIM(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const thrust::device_vector<T>& u,thrust::device_vector<T>& Fu) {
#define BLK 32
  if(_C[dim]) {
    _C[dim]->multiply(u,Fu);
  } else {
    dim3 nrBlk,szBlk(1,1,1);
    if(dim==0) {
      nrBlk=dim3(NUM_BLOCK(X,BLK),Y,Z);
      szBlk.x=BLK+_hsize*2;
      forwardDim<dim,BLK,BLK,1,1><<<nrBlk,szBlk>>>
      (X,Y,Z,stride,_hsize,
       thrust::raw_pointer_cast(u.data()),
       thrust::raw_pointer_cast(Fu.data()));
    } else if(dim==1) {
      nrBlk=dim3(X,NUM_BLOCK(Y,BLK),Z);
      szBlk.y=BLK+_hsize*2;
      forwardDim<dim,BLK,1,BLK,1><<<nrBlk,szBlk>>>
      (X,Y,Z,stride,_hsize,
       thrust::raw_pointer_cast(u.data()),
       thrust::raw_pointer_cast(Fu.data()));
    } else if(dim==2) {
      nrBlk=dim3(X,Y,NUM_BLOCK(Z,BLK));
      szBlk.z=BLK+_hsize*2;
      forwardDim<dim,BLK,1,1,BLK><<<nrBlk,szBlk>>>
      (X,Y,Z,stride,_hsize,
       thrust::raw_pointer_cast(u.data()),
       thrust::raw_pointer_cast(Fu.data()));
    }
  }
#undef BLK
}
template <int dim>
void backwardDIM(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const thrust::device_vector<T>& u,thrust::device_vector<T>& Fu) {
#define BLK 32
  if(_C[dim]) {
    _CT[dim]->multiply(u,Fu);
  } else {
    dim3 nrBlk,szBlk(1,1,1);
    if(dim==0) {
      nrBlk=dim3(NUM_BLOCK(X,BLK),Y,Z);
      szBlk.x=BLK+_hsize*2;
      backwardDim<dim,BLK,BLK,1,1><<<nrBlk,szBlk>>>
      (X,Y,Z,stride,_hsize,
       thrust::raw_pointer_cast(u.data()),
       thrust::raw_pointer_cast(Fu.data()));
    } else if(dim==1) {
      nrBlk=dim3(X,NUM_BLOCK(Y,BLK),Z);
      szBlk.y=BLK+_hsize*2;
      backwardDim<dim,BLK,1,BLK,1><<<nrBlk,szBlk>>>
      (X,Y,Z,stride,_hsize,
       thrust::raw_pointer_cast(u.data()),
       thrust::raw_pointer_cast(Fu.data()));
    } else if(dim==2) {
      nrBlk=dim3(X,Y,NUM_BLOCK(Z,BLK));
      szBlk.z=BLK+_hsize*2;
      backwardDim<dim,BLK,1,1,BLK><<<nrBlk,szBlk>>>
      (X,Y,Z,stride,_hsize,
       thrust::raw_pointer_cast(u.data()),
       thrust::raw_pointer_cast(Fu.data()));
    }
  }
#undef BLK
}
void forward(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const thrust::device_vector<T>& u,thrust::device_vector<T>& Fu) {
  _FuX->resize(u.size());
  _FuY->resize(u.size());
  if(DIM==2) {
    forwardDIM<0>(X,Y,Z,stride,u,*_FuX);
    forwardDIM<1>(X,Y,Z,stride,*_FuX,Fu);
  } else {
    forwardDIM<0>(X,Y,Z,stride,u,*_FuX);
    forwardDIM<1>(X,Y,Z,stride,*_FuX,*_FuY);
    forwardDIM<2>(X,Y,Z,stride,*_FuY,Fu);
  }
}
void backward(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const thrust::device_vector<T>& DEDFu,thrust::device_vector<T>& DEDu) {
  _FuX->resize(DEDFu.size());
  _FuY->resize(DEDFu.size());
  if(DIM==2) {
    backwardDIM<1>(X,Y,Z,stride,DEDFu,*_FuX);
    backwardDIM<0>(X,Y,Z,stride,*_FuX,DEDu);
  } else {
    backwardDIM<2>(X,Y,Z,stride,DEDFu,*_FuY);
    backwardDIM<1>(X,Y,Z,stride,*_FuY,*_FuX);
    backwardDIM<0>(X,Y,Z,stride,*_FuX,DEDu);
  }
}
void forward(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const Vec& u,Vec& Fu) {
  thrust::device_vector<T> uGPU(u.begin(),u.end());
  thrust::device_vector<T> FuGPU(Fu.begin(),Fu.end());
  forward(X,Y,Z,stride,uGPU,FuGPU);
  thrust::copy(FuGPU.begin(),FuGPU.end(),Fu.begin());
}
void backward(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const Vec& DEDFu,Vec& DEDu) {
  thrust::device_vector<T> DEDFuGPU(DEDFu.begin(),DEDFu.end());
  thrust::device_vector<T> DEDuGPU(DEDu.begin(),DEDu.end());
  backward(X,Y,Z,stride,DEDFuGPU,DEDuGPU);
  thrust::copy(DEDuGPU.begin(),DEDuGPU.end(),DEDu.begin());
}
}
}
