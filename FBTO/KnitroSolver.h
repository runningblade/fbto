#ifndef KNITRO_SOLVER_H
#define KNITRO_SOLVER_H
#ifdef KNITRO_SUPPORT

#include "FBTO.h"
#include "KNSolver.h"
#include "KNProblem.h"

using namespace knitro;

namespace FBTO {
template <int DIM,int POWRHO>
class SQPSolver : public knitro::KNProblem {
 public:
  typedef SCALAR_TYPE T;
  DECL_MAT_VEC_MAP_TYPES_T
  //Dense
  typedef Eigen::Matrix<T,DIM,1> VecDT;
  typedef Eigen::Matrix<T,DIM,DIM> MatDT;
  typedef Eigen::Matrix<T,DIM*(1<<DIM),1> VecDDT;
  typedef Eigen::Matrix<T,DIM*(1<<DIM),DIM*(1<<DIM)> MatDDT;
  SQPSolver(T lambda,T mu,T minFill,T sumFillRel,int x,int y,int z=1);
  int solve(std::function<VecDT(Eigen::Matrix<int,3,1>)> force,Vec& xC);
  FBTOSolver<DIM,POWRHO>& sol();
 private:
  static int callbackEvalF(KN_context_ptr             kc,
                           CB_context_ptr             cb,
                           KN_eval_request_ptr const  evalRequest,
                           KN_eval_result_ptr  const  evalResult,
                           void              * const  userParams);
  static int callbackEvalG(KN_context_ptr             kc,
                           CB_context_ptr             cb,
                           KN_eval_request_ptr const  evalRequest,
                           KN_eval_result_ptr  const  evalResult,
                           void              * const  userParams);
  static int callbackEvalH(KN_context_ptr             kc,
                           CB_context_ptr             cb,
                           KN_eval_request_ptr const  evalRequest,
                           KN_eval_result_ptr  const  evalResult,
                           void              * const  userParams);
  static void computeVar(SQPSolver<DIM,POWRHO>* sol,const Vec& x);
  void initProblem();
  Vec _xV,_f,_u,_xC,_diff,_diffC;
  FBTOSolver<DIM,POWRHO> _sol;
};
}

#endif
#endif
