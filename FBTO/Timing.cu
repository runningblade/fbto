#include "Timing.cuh"
#include "Pragma.h"
#include <vector>
#include <chrono>

namespace FBTO {
bool __timing=true;
std::vector<char> __timingSetting;
std::vector<std::pair<std::string,std::chrono::time_point<std::chrono::steady_clock>>> __timer;

std::string replace_all(std::string str,const std::string& from,const std::string& to) {
  size_t start_pos=0;
  while((start_pos=str.find(from,start_pos))!=std::string::npos) {
    str.replace(start_pos,from.length(),to);
    start_pos+=to.length(); // Handles case where 'to' is a substring of 'from'
  }
  return str;
}
void saveTimingSetting() {
  __timingSetting.push_back(__timing);
}
void loadTimingSetting() {
  ASSERT_MSG(!__timingSetting.empty(),"loading timing setting with no setting available!")
  __timing=(bool)__timingSetting.back();
}
void enableTiming() {
  __timing=true;
}
void disableTiming() {
  __timing=false;
}

void TBEG() {
  TBEG("");
}
void TBEG(const std::string& name) {
  OMP_CRITICAL_ {
    __timer.push_back(make_pair(name,std::chrono::steady_clock::now()));
  }
}
void TEND(std::ostream& os) {
  OMP_CRITICAL_ {
    ASSERT_MSG(!__timer.empty(),"Incorrect calling of TEND without TBEG!")
    if(__timing) {
      std::chrono::time_point<std::chrono::steady_clock> pt=std::chrono::steady_clock::now();
      std::chrono::duration<double> diff=pt-__timer.back().second;
      os << __timer.back().first << ": " << diff.count();
    }
    __timer.pop_back();
  }
}
void TEND() {
  TEND(std::cout);
  OMP_CRITICAL_ {
    if(__timing)
      std::cout << std::endl;
  }
}
double TENDVPeek() {
  double ret;
  OMP_CRITICAL_ {
    ASSERT_MSG(!__timer.empty(),"Incorrect calling of TEND without TBEG!")
    std::chrono::time_point<std::chrono::steady_clock> pt=std::chrono::steady_clock::now();
    std::chrono::duration<double> diff=pt-__timer.back().second;
    ret=diff.count();
  }
  return ret;
}
double TENDV() {
  double ret;
  OMP_CRITICAL_ {
    ASSERT_MSG(!__timer.empty(),"Incorrect calling of TEND without TBEG!")
    std::chrono::time_point<std::chrono::steady_clock> pt=std::chrono::steady_clock::now();
    std::chrono::duration<double> diff=pt-__timer.back().second;
    ret=diff.count();
    __timer.pop_back();
  }
  return ret;
}
}
