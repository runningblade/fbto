#include "FBTOGPU.cuh"
#include <thrust/sort.h>
#include <thrust/scan.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/inner_product.h>
#include "Timing.cuh"
#include <iomanip>
#include <memory>

namespace FBTO {
namespace GPU {
extern void memoryCost();
struct VSSEntry {
  T _first;
  bool _second;
};
struct SAXPYFunctor {
  const T _a;
  SAXPYFunctor(T a):_a(a) {}
  __host__ __device__ T operator()(const T& x,const T& y) const {
    return x+_a*y;
  }
};
struct ScaleFunctor {
  const T _a;
  ScaleFunctor(T a):_a(a) {}
  __host__ __device__ T operator()(const T& x) const {
    return _a*x;
  }
};
struct AbsFunctor {
  __host__ __device__ T operator()(const T& x) const {
    return abs(x);
  }
};
struct AddFunctor {
  const T _a;
  AddFunctor(T a):_a(a) {}
  __host__ __device__ T operator()(const T& x) const {
    return x+_a;
  }
};
struct SortFunctor {
  __host__ __device__  bool operator()(const VSSEntry& a,const VSSEntry& b) const {
    return a._first<b._first;
  }
};
struct FillActiveFunctor {
  __host__ __device__  int operator()(const VSSEntry& a) const {
    return a._second?1:-1;
  }
};
struct ClampFillFunctor {
  const T _minFill,_lambda;
  ClampFillFunctor(T minFill,T lambda):_minFill(minFill),_lambda(lambda) {}
  __host__ __device__ T operator()(const T& x) const {
    return min(max(x+_lambda,_minFill),T(1));
  }
};
template <int TYPE>
T lpNorm(const thrust::device_vector<T>& v);
template <>
T lpNorm<1>(const thrust::device_vector<T>& v) {
  return thrust::transform_reduce(v.begin(),v.end(),AbsFunctor(),T(0),thrust::plus<T>());
}
template <>
T lpNorm<2>(const thrust::device_vector<T>& v) {
  return sqrt(thrust::inner_product(v.begin(),v.end(),v.begin(),T(0)));
}
template <>
T lpNorm<Eigen::Infinity>(const thrust::device_vector<T>& v) {
  return thrust::transform_reduce(v.begin(),v.end(),AbsFunctor(),T(0),thrust::maximum<T>());
}
__constant__ T _KLocal[24*24];
template <int BLK2D>
__global__ void mulK2D(int X,int Y,T powRho,Eigen::Matrix<int,3,1> strideF,Eigen::Matrix<int,3,1> strideU,
                       const char* mask,const T* fill,const T* b,T* Kb) {
  __shared__ Vec2T bBlk[BLK2D+2][BLK2D+2];
  __shared__ T     fBlk[BLK2D+2][BLK2D+2];
  int xoff=blockIdx.x*BLK2D,x=xoff+threadIdx.x-1;
  int yoff=blockIdx.y*BLK2D,y=yoff+threadIdx.y-1;
  int offU=strideU[0]*x+strideU[1]*y;
  int offF=strideF[0]*x+strideF[1]*y;
  //load bBlk/fBlk
  if(x>=0 && x<=X && y>=0 && y<=Y)
    bBlk[threadIdx.x][threadIdx.y]=(mask[offU]==1)?Vec2T::Zero():Vec2T(Eigen::Map<const Vec2T>(b+offU*2));
  if(x>=0 && x<X && y>=0 && y<Y)
    fBlk[threadIdx.x][threadIdx.y]=pow(fill[offF],powRho);
  __syncthreads();
  //compute Kb
  if( threadIdx.x>0 && threadIdx.x<BLK2D+1 &&
      threadIdx.y>0 && threadIdx.y<BLK2D+1 &&
      x<=X && y<=Y) {
    Vec2T ret=Vec2T::Zero();
    if(mask[offU]==0) {
      Eigen::Matrix<T,8,1> RHS;
      Eigen::Map<Eigen::Matrix<T,24,24>> KLocal(_KLocal);
      static const int offx[4]= {-1, 0,-1, 0};
      static const int offy[4]= {-1,-1, 0, 0};
      static const int offK[4]= { 6, 4, 2, 0};
      for(int i=0; i<4; i++)
        if(x+offx[i]>=0 && x+offx[i]<X && y+offy[i]>=0 && y+offy[i]<Y) {
          RHS.segment<2>(0)=bBlk[threadIdx.x+offx[i]+0][threadIdx.y+offy[i]+0];
          RHS.segment<2>(2)=bBlk[threadIdx.x+offx[i]+1][threadIdx.y+offy[i]+0];
          RHS.segment<2>(4)=bBlk[threadIdx.x+offx[i]+0][threadIdx.y+offy[i]+1];
          RHS.segment<2>(6)=bBlk[threadIdx.x+offx[i]+1][threadIdx.y+offy[i]+1];
          ret+=KLocal.block<2,8>(offK[i],0)*RHS*fBlk[threadIdx.x+offx[i]+0][threadIdx.y+offy[i]+0];
        }
    }
    Eigen::Map<Vec2T>(Kb+offU*2)=ret;
  }
}
template <int BLK2D>
__global__ void diffK2D(int X,int Y,T powRho,Eigen::Matrix<int,3,1> strideF,Eigen::Matrix<int,3,1> strideU,
                        const char* mask,const T* fill,const T* u,Eigen::Matrix<T,8,1> sf,T* diff) {
  __shared__ Vec2T uBlk[BLK2D+1][BLK2D+1];
  int xoff=blockIdx.x*BLK2D,x=xoff+threadIdx.x;
  int yoff=blockIdx.y*BLK2D,y=yoff+threadIdx.y;
  int offU=strideU[0]*x+strideU[1]*y;
  int offF=strideF[0]*x+strideF[1]*y;
  //load uBlk
  if(x>=0 && x<=X && y>=0 && y<=Y)
    uBlk[threadIdx.x][threadIdx.y]=(mask[offU]==1)?Vec2T::Zero():Vec2T(Eigen::Map<const Vec2T>(u+offU*2));
  __syncthreads();
  //compute diff
  if( threadIdx.x<BLK2D &&
      threadIdx.y<BLK2D &&
      x<X && y<Y) {
    Eigen::Matrix<T,8,1> RHS;
    Eigen::Map<Eigen::Matrix<T,24,24>> KLocal(_KLocal);
    for(int i=0; i<4; i++) {
      RHS.segment<2>(0)=uBlk[threadIdx.x+0][threadIdx.y+0];
      RHS.segment<2>(2)=uBlk[threadIdx.x+1][threadIdx.y+0];
      RHS.segment<2>(4)=uBlk[threadIdx.x+0][threadIdx.y+1];
      RHS.segment<2>(6)=uBlk[threadIdx.x+1][threadIdx.y+1];
    }
    diff[offF]+=(RHS.dot(KLocal.block<8,8>(0,0)*RHS)/2-RHS.dot(sf))*pow(fill[offF],powRho-1)*powRho;
  }
}
template <int BLK2D>
__global__ void modifyF2D(int X,int Y,T powRho,Eigen::Matrix<int,3,1> strideF,Eigen::Matrix<int,3,1> strideU,
                          const char* mask,const T* fill,const T* force,Vec2T sf,T* modifiedF) {
  __shared__ T     fBlk[BLK2D+2][BLK2D+2];
  int xoff=blockIdx.x*BLK2D,x=xoff+threadIdx.x-1;
  int yoff=blockIdx.y*BLK2D,y=yoff+threadIdx.y-1;
  int offU=strideU[0]*x+strideU[1]*y;
  int offF=strideF[0]*x+strideF[1]*y;
  //load bBlk/fBlk
  if(x>=0 && x<X && y>=0 && y<Y)
    fBlk[threadIdx.x][threadIdx.y]=pow(fill[offF],powRho);
  __syncthreads();
  //compute Kb
  if( threadIdx.x>0 && threadIdx.x<BLK2D+1 &&
      threadIdx.y>0 && threadIdx.y<BLK2D+1 &&
      x<=X && y<=Y) {
    T ret=0;
    if(mask[offU]==0) {
      static const int offx[4]= {-1, 0,-1, 0};
      static const int offy[4]= {-1,-1, 0, 0};
      for(int i=0; i<4; i++)
        if(x+offx[i]>=0 && x+offx[i]<X && y+offy[i]>=0 && y+offy[i]<Y)
          ret+=fBlk[threadIdx.x+offx[i]+0][threadIdx.y+offy[i]+0];
    }
    Eigen::Map<Vec2T>(modifiedF+offU*2)=Eigen::Map<const Vec2T>(force+offU*2)+sf*ret;
  }
}
template <int BLK3D>
__global__ void mulK3D(int X,int Y,int Z,T powRho,Eigen::Matrix<int,3,1> strideF,Eigen::Matrix<int,3,1> strideU,
                       const char* mask,const T* fill,const T* b,T* Kb) {
  __shared__ Vec3T bBlk[BLK3D+2][BLK3D+2][BLK3D+2];
  __shared__ T     fBlk[BLK3D+2][BLK3D+2][BLK3D+2];
  int xoff=blockIdx.x*BLK3D,x=xoff+threadIdx.x-1;
  int yoff=blockIdx.y*BLK3D,y=yoff+threadIdx.y-1;
  int zoff=blockIdx.z*BLK3D,z=zoff+threadIdx.z-1;
  int offU=strideU[0]*x+strideU[1]*y+strideU[2]*z;
  int offF=strideF[0]*x+strideF[1]*y+strideF[2]*z;
  //load bBlk/fBlk
  if(x>=0 && x<=X && y>=0 && y<=Y && z>=0 && z<=Z)
    bBlk[threadIdx.x][threadIdx.y][threadIdx.z]=(mask[offU]==1)?Vec3T::Zero():Vec3T(Eigen::Map<const Vec3T>(b+offU*3));
  if(x>=0 && x<X && y>=0 && y<Y && z>=0 && z<Z)
    fBlk[threadIdx.x][threadIdx.y][threadIdx.z]=pow(fill[offF],powRho);
  __syncthreads();
  //compute Kb
  if( threadIdx.x>0 && threadIdx.x<BLK3D+1 &&
      threadIdx.y>0 && threadIdx.y<BLK3D+1 &&
      threadIdx.z>0 && threadIdx.z<BLK3D+1 &&
      x<=X && y<=Y && z<=Z) {
    Vec3T ret=Vec3T::Zero();
    if(mask[offU]==0) {
      Eigen::Matrix<T,24,1> RHS;
      Eigen::Map<Eigen::Matrix<T,24,24>> KLocal(_KLocal);
      static const int offx[8]= {-1, 0,-1, 0,-1, 0,-1, 0};
      static const int offy[8]= {-1,-1, 0, 0,-1,-1, 0, 0};
      static const int offz[8]= {-1,-1,-1,-1, 0, 0, 0, 0};
      static const int offK[8]= {21,18,15,12, 9, 6, 3, 0};
      for(int i=0; i<8; i++)
        if(x+offx[i]>=0 && x+offx[i]<X && y+offy[i]>=0 && y+offy[i]<Y && z+offz[i]>=0 && z+offz[i]<Z) {
          RHS.segment<3>(0 )=bBlk[threadIdx.x+offx[i]+0][threadIdx.y+offy[i]+0][threadIdx.z+offz[i]+0];
          RHS.segment<3>(3 )=bBlk[threadIdx.x+offx[i]+1][threadIdx.y+offy[i]+0][threadIdx.z+offz[i]+0];
          RHS.segment<3>(6 )=bBlk[threadIdx.x+offx[i]+0][threadIdx.y+offy[i]+1][threadIdx.z+offz[i]+0];
          RHS.segment<3>(9 )=bBlk[threadIdx.x+offx[i]+1][threadIdx.y+offy[i]+1][threadIdx.z+offz[i]+0];
          RHS.segment<3>(12)=bBlk[threadIdx.x+offx[i]+0][threadIdx.y+offy[i]+0][threadIdx.z+offz[i]+1];
          RHS.segment<3>(15)=bBlk[threadIdx.x+offx[i]+1][threadIdx.y+offy[i]+0][threadIdx.z+offz[i]+1];
          RHS.segment<3>(18)=bBlk[threadIdx.x+offx[i]+0][threadIdx.y+offy[i]+1][threadIdx.z+offz[i]+1];
          RHS.segment<3>(21)=bBlk[threadIdx.x+offx[i]+1][threadIdx.y+offy[i]+1][threadIdx.z+offz[i]+1];
          ret+=KLocal.block<3,24>(offK[i],0)*RHS*fBlk[threadIdx.x+offx[i]+0][threadIdx.y+offy[i]+0][threadIdx.z+offz[i]+0];
        }
    }
    Eigen::Map<Vec3T>(Kb+offU*3)=ret;
  }
}
template <int BLK3D>
__global__ void diffK3D(int X,int Y,int Z,T powRho,Eigen::Matrix<int,3,1> strideF,Eigen::Matrix<int,3,1> strideU,
                        const char* mask,const T* fill,const T* u,Eigen::Matrix<T,24,1> sf,T* diff) {
  __shared__ Vec3T uBlk[BLK3D+1][BLK3D+1][BLK3D+1];
  int xoff=blockIdx.x*BLK3D,x=xoff+threadIdx.x;
  int yoff=blockIdx.y*BLK3D,y=yoff+threadIdx.y;
  int zoff=blockIdx.z*BLK3D,z=zoff+threadIdx.z;
  int offU=strideU[0]*x+strideU[1]*y+strideU[2]*z;
  int offF=strideF[0]*x+strideF[1]*y+strideF[2]*z;
  //load uBlk
  if(x>=0 && x<=X && y>=0 && y<=Y && z>=0 && z<=Z)
    uBlk[threadIdx.x][threadIdx.y][threadIdx.z]=(mask[offU]==1)?Vec3T::Zero():Vec3T(Eigen::Map<const Vec3T>(u+offU*3));
  __syncthreads();
  //compute diff
  if( threadIdx.x<BLK3D &&
      threadIdx.y<BLK3D &&
      threadIdx.z<BLK3D &&
      x<X && y<Y && z<Z) {
    Eigen::Matrix<T,24,1> RHS;
    Eigen::Map<Eigen::Matrix<T,24,24>> KLocal(_KLocal);
    for(int i=0; i<8; i++) {
      RHS.segment<3>(0 )=uBlk[threadIdx.x+0][threadIdx.y+0][threadIdx.z+0];
      RHS.segment<3>(3 )=uBlk[threadIdx.x+1][threadIdx.y+0][threadIdx.z+0];
      RHS.segment<3>(6 )=uBlk[threadIdx.x+0][threadIdx.y+1][threadIdx.z+0];
      RHS.segment<3>(9 )=uBlk[threadIdx.x+1][threadIdx.y+1][threadIdx.z+0];
      RHS.segment<3>(12)=uBlk[threadIdx.x+0][threadIdx.y+0][threadIdx.z+1];
      RHS.segment<3>(15)=uBlk[threadIdx.x+1][threadIdx.y+0][threadIdx.z+1];
      RHS.segment<3>(18)=uBlk[threadIdx.x+0][threadIdx.y+1][threadIdx.z+1];
      RHS.segment<3>(21)=uBlk[threadIdx.x+1][threadIdx.y+1][threadIdx.z+1];
    }
    diff[offF]+=(RHS.dot(KLocal*RHS)/2-RHS.dot(sf))*pow(fill[offF],powRho-1)*powRho;
  }
}
template <int BLK3D>
__global__ void modifyF3D(int X,int Y,int Z,T powRho,Eigen::Matrix<int,3,1> strideF,Eigen::Matrix<int,3,1> strideU,
                          const char* mask,const T* fill,const T* force,Vec3T sf,T* modifiedF) {
  __shared__ T     fBlk[BLK3D+2][BLK3D+2][BLK3D+2];
  int xoff=blockIdx.x*BLK3D,x=xoff+threadIdx.x-1;
  int yoff=blockIdx.y*BLK3D,y=yoff+threadIdx.y-1;
  int zoff=blockIdx.z*BLK3D,z=zoff+threadIdx.z-1;
  int offU=strideU[0]*x+strideU[1]*y+strideU[2]*z;
  int offF=strideF[0]*x+strideF[1]*y+strideF[2]*z;
  //load bBlk/fBlk
  if(x>=0 && x<X && y>=0 && y<Y && z>=0 && z<Z)
    fBlk[threadIdx.x][threadIdx.y][threadIdx.z]=pow(fill[offF],powRho);
  __syncthreads();
  //compute Kb
  if( threadIdx.x>0 && threadIdx.x<BLK3D+1 &&
      threadIdx.y>0 && threadIdx.y<BLK3D+1 &&
      threadIdx.z>0 && threadIdx.z<BLK3D+1 &&
      x<=X && y<=Y && z<=Z) {
    T ret=0;
    if(mask[offU]==0) {
      static const int offx[8]= {-1, 0,-1, 0,-1, 0,-1, 0};
      static const int offy[8]= {-1,-1, 0, 0,-1,-1, 0, 0};
      static const int offz[8]= {-1,-1,-1,-1, 0, 0, 0, 0};
      for(int i=0; i<8; i++)
        if(x+offx[i]>=0 && x+offx[i]<X && y+offy[i]>=0 && y+offy[i]<Y && z+offz[i]>=0 && z+offz[i]<Z)
          ret+=fBlk[threadIdx.x+offx[i]+0][threadIdx.y+offy[i]+0][threadIdx.z+offz[i]+0];
    }
    Eigen::Map<Vec3T>(modifiedF+offU*3)=Eigen::Map<const Vec3T>(force+offU*3)+sf*ret;
  }
}
__global__ void projXFillVss(T minFill,int N,const T* fill,VSSEntry* vss) {
  int off=blockIdx.x*blockDim.x+threadIdx.x;
  if(off>=N)
    return;
  T f=fill[off];
  vss[off*2+0]._first=minFill-f;
  vss[off*2+0]._second=true;
  vss[off*2+1]._first=1-f;
  vss[off*2+1]._second=false;
}
__global__ void projXComputeTotal(int N,T* total,const int* active,const VSSEntry* vss) {
  int off=blockIdx.x*blockDim.x+threadIdx.x;
  if(off==0 || off>=N)
    return;
  else total[off-1]=active[off]*(vss[off]._first-vss[off-1]._first);
}
__global__ void projXComputeLambda(T sumFill,int N,T* lambda,const T* total,const int* active,const VSSEntry* vss) {
  int off=blockIdx.x*blockDim.x+threadIdx.x;
  if(off==0 || off>=N)
    return;
  if(total[off-1]<sumFill && total[off]>=sumFill)
    lambda[0]=(sumFill-total[off])/active[off]+vss[off]._first;
}
__global__ void reindex(int n,const int* index,const T* input,T* output) {
  int x=blockIdx.x*blockDim.x+threadIdx.x;
  if(x>=n)
    return;
  output[x]=input[index[x]];
}
__global__ void redistribute(int n,const int* index,const T* input,T* output) {
  int x=blockIdx.x*blockDim.x+threadIdx.x;
  if(x>=n)
    return;
  output[index[x]]=input[x];
}
//parameter
int DIM,POWRHO;
Eigen::Matrix<int,3,1> _strideF,_strideU;
std::string _writePath;
T _minFill,_sumFill,_tol,_tolX,_writeMul;
int _x,_y,_z,_D,_maxIter,_reportInterval,_writeInterval;
Eigen::Matrix<bool,3,1> _symmetry;
//data
std::shared_ptr<thrust::device_vector<VSSEntry>> _vss;
std::shared_ptr<thrust::device_vector<int>> _active;
std::shared_ptr<thrust::device_vector<char>> _mask;
std::vector<std::shared_ptr<thrust::device_vector<T>>> _Kry;
std::shared_ptr<thrust::device_vector<T>> _r,_Ku,_p,_total,_lambda;
std::function<void(T,const Vec*,const Vec*,const std::string&,int,int)> _cb;
std::function<void(Vec&,const Vec&,const Vec&)> _cbSol;
//compact
std::shared_ptr<thrust::device_vector<T>> _fCompact;
std::shared_ptr<thrust::device_vector<T>> _fCompactOut;
std::shared_ptr<thrust::device_vector<int>> _compactIndex;
void compactFMask(thrust::device_vector<T>& out,const thrust::device_vector<T>& in) {
#define BLK 32
  ASSERT(_compactIndex)
  dim3 nrBlk(NUM_BLOCK(out.size(),BLK),1,1);
  dim3 szBlk(BLK,1,1);
  reindex<<<nrBlk,szBlk>>>
  (out.size(),
   thrust::raw_pointer_cast(_compactIndex->data()),
   thrust::raw_pointer_cast(in.data()),
   thrust::raw_pointer_cast(out.data()));
#undef BLK
}
void uncompactFMask(thrust::device_vector<T>& out,const thrust::device_vector<T>& in) {
  thrust::fill(out.begin(),out.end(),T(0));
#define BLK 32
  ASSERT(_compactIndex)
  dim3 nrBlk(NUM_BLOCK(in.size(),BLK),1,1);
  dim3 szBlk(BLK,1,1);
  redistribute<<<nrBlk,szBlk>>>
  (in.size(),
   thrust::raw_pointer_cast(_compactIndex->data()),
   thrust::raw_pointer_cast(in.data()),
   thrust::raw_pointer_cast(out.data()));
#undef BLK

}
//method
int xU() {
  return _x+1;
}
int yU() {
  return _y+1;
}
int zU() {
  return DIM==2?1:_z+1;
}
void init(int dim,int powRho,T minFill,T sumFill,int x,int y,int z,int D,int maxIter,int reportInterval,T tol,T tolX,const Eigen::Matrix<bool,3,1>& symmetry) {
  memoryCost();
  DIM=dim;
  POWRHO=powRho;
  _minFill=minFill;
  _sumFill=sumFill;
  _x=x;
  _y=y;
  _z=z;
  _D=D;
  ASSERT((z==1)+(DIM!=2)==1);
  _strideF=Eigen::Matrix<int,3,1>(y*z,z,1);
  _strideU=Eigen::Matrix<int,3,1>(yU()*zU(),zU(),1);
  _maxIter=maxIter;
  _reportInterval=reportInterval;
  _tol=tol;
  _tolX=tolX;
  _symmetry=symmetry;
  //data
  _fCompact=_fCompactOut=NULL;
  _compactIndex=NULL;
  _vss.reset(new thrust::device_vector<VSSEntry>(x*y*z*2));
  _active.reset(new thrust::device_vector<int>(x*y*z*2));
  _mask.reset(new thrust::device_vector<char>(xU()*yU()*zU()));
  _r.reset(new thrust::device_vector<T>(xU()*yU()*zU()*DIM));
  _Ku.reset(new thrust::device_vector<T>(xU()*yU()*zU()*DIM));
  _p.reset(new thrust::device_vector<T>(xU()*yU()*zU()*DIM));
  _total.reset(new thrust::device_vector<T>(x*y*z*2));
  _lambda.reset(new thrust::device_vector<T>(1));
}
void initWrite(const std::string& writePath,int writeInterval,T writeMul,
               std::function<void(T,const Vec*,const Vec*,const std::string&,int,int)> cb) {
  _writePath=writePath;
  _writeInterval=writeInterval;
  _writeMul=writeMul;
  _cb=cb;
}
void setCBSolver(std::function<void(Vec&,const Vec&,const Vec&)> cbSol) {
  _cbSol=cbSol;
}
void finalize() {
  _fCompact=_fCompactOut=NULL;
  _compactIndex=NULL;
  _vss=NULL;
  _active=NULL;
  _mask=NULL;
  _Kry.clear();
  _r=NULL;
  _Ku=NULL;
  _p=NULL;
  _total=NULL;
  _lambda=NULL;
}
void assignK(const MatT& K) {
  Eigen::Matrix<T,24,24> KLocal=Eigen::Matrix<T,24,24>::Zero();
  KLocal.block(0,0,K.rows(),K.cols())=K;
  cudaMemcpyToSymbol(_KLocal,KLocal.data(),sizeof(T)*24*24);
}
void setMask(const std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash<int>>& fix,std::shared_ptr<Eigen::Matrix<bool,-1,1>> fMask) {
  _mask->assign(xU()*yU()*zU(),0);
  for(auto m:fix)
    (*_mask)[m.dot(_strideU)]=1;
  //fMask
  if(!fMask) {
    _fCompact=_fCompactOut=NULL;
    _compactIndex=NULL;
  } else {
    _fCompact.reset(new thrust::device_vector<T>(fMask->size()-fMask->cast<int>().sum()));
    _fCompactOut.reset(new thrust::device_vector<T>(fMask->size()-fMask->cast<int>().sum()));
    _compactIndex.reset(new thrust::device_vector<int>(fMask->size()-fMask->cast<int>().sum()));
    for(int i=0,j=0; i<fMask->size(); i++)
      if(!(*fMask)[i])
        (*_compactIndex)[j++]=i;
  }
}
void mulK(thrust::device_vector<T>& Kb,const thrust::device_vector<T>& fill,const thrust::device_vector<T>& b,int offb=0,int offKb=0) {
#define BLK2D 2
#define BLK3D 2
  if(DIM==2) {
    dim3 nrBlk(NUM_BLOCK(_x+1,BLK2D),NUM_BLOCK(_y+1,BLK2D),1);
    dim3 szBlk(BLK2D+2,BLK2D+2,1);
    mulK2D<BLK2D><<<nrBlk,szBlk>>>
    (_x,_y,POWRHO,_strideF,_strideU,
     thrust::raw_pointer_cast(_mask->data()),
     thrust::raw_pointer_cast(fill.data()),
     thrust::raw_pointer_cast(b.data())+offb,
     thrust::raw_pointer_cast(Kb.data())+offKb);
  } else {
    dim3 nrBlk(NUM_BLOCK(_x+1,BLK3D),NUM_BLOCK(_y+1,BLK3D),NUM_BLOCK(_z+1,BLK3D));
    dim3 szBlk(BLK3D+2,BLK3D+2,BLK3D+2);
    mulK3D<BLK3D><<<nrBlk,szBlk>>>
    (_x,_y,_z,POWRHO,_strideF,_strideU,
     thrust::raw_pointer_cast(_mask->data()),
     thrust::raw_pointer_cast(fill.data()),
     thrust::raw_pointer_cast(b.data())+offb,
     thrust::raw_pointer_cast(Kb.data())+offKb);
  }
#undef BLK2D
#undef BLK3D
}
Vec mulK(const Vec& fill,const Vec& b) {
  thrust::device_vector<T> fillGPU(fill.begin(),fill.end());
  thrust::device_vector<T> bGPU(b.begin(),b.end()),KbGPU(b.size());
  mulK(KbGPU,fillGPU,bGPU);

  Vec Kb;
  Kb.resize(KbGPU.size());
  thrust::copy(KbGPU.begin(),KbGPU.end(),Kb.begin());
  return Kb;
}
int solveK(thrust::device_vector<T>& u,const thrust::device_vector<T>& fill,const thrust::device_vector<T>& b,T tol,int iters=10E4) {
  if(tol<=0) {
    Vec uCPU=Vec::Zero(u.size());
    Vec fillCPU=Vec::Zero(fill.size());
    thrust::copy(fill.begin(),fill.end(),fillCPU.begin());
    Vec bCPU=Vec::Zero(b.size());
    thrust::copy(b.begin(),b.end(),bCPU.begin());
    _cbSol(uCPU,fillCPU,bCPU);
    thrust::copy(uCPU.begin(),uCPU.end(),u.begin());
    return -1;
  } else {
    mulK(*_Ku,fill,u);//Ku=K*u
    thrust::transform(b.begin(),b.end(),_Ku->begin(),_r->begin(),thrust::minus<T>());//r=b-K*u
    thrust::copy(_r->begin(),_r->end(),_p->begin());//p=r
    T rTr=thrust::inner_product(_r->begin(),_r->end(),_r->begin(),T(0));//rTr=<r,r>
    if(rTr<tol)
      return 0;
    int i=0;
    while(i<iters) {
      mulK(*_Ku,fill,*_p);//Ku=K*p
      T pTKp=thrust::inner_product(_p->begin(),_p->end(),_Ku->begin(),T(0));//pTKp=<p,K*p>
      T alpha=rTr/pTKp;//alpha=<r,r>/<p,K*p>
      thrust::transform(u.begin(),u.end(),_p->begin(),u.begin(),SAXPYFunctor(alpha));//u=u+alpha*p
      thrust::transform(_r->begin(),_r->end(),_Ku->begin(),_r->begin(),SAXPYFunctor(-alpha));//r=r-alpha*K*p
      T rTrNew=thrust::inner_product(_r->begin(),_r->end(),_r->begin(),T(0));//rTrNew=<r,r>
      if(rTrNew<tol)
        break;
      T beta=rTrNew/rTr;//beta=<rNew,rNew>/<r,r>
      thrust::transform(_r->begin(),_r->end(),_p->begin(),_p->begin(),SAXPYFunctor(beta));//p=r+beta*p
      rTr=rTrNew;
      i++;
    }
    return i;
  }
}
int solveK(Vec& u,const Vec& fill,const Vec& b,T tol,int iters) {
  thrust::device_vector<T> uGPU(u.begin(),u.end());
  thrust::device_vector<T> fillGPU(fill.begin(),fill.end());
  thrust::device_vector<T> bGPU(b.begin(),b.end());

  int i=solveK(uGPU,fillGPU,bGPU,tol,iters);
  u.resize(uGPU.size());
  thrust::copy(uGPU.begin(),uGPU.end(),u.begin());
  return i;
}
void solveKGD(thrust::device_vector<T>& u,const thrust::device_vector<T>& fill,const thrust::device_vector<T>& b,T beta) {
  mulK(*_Ku,fill,u);
  thrust::transform(_Ku->begin(),_Ku->end(),b.begin(),_Ku->begin(),thrust::minus<T>());
  thrust::transform(u.begin(),u.end(),_Ku->begin(),u.begin(),SAXPYFunctor(-beta));
}
void solveKGD(Vec& u,const Vec& fill,const Vec& b,T beta) {
  thrust::device_vector<T> uGPU(u.begin(),u.end());
  thrust::device_vector<T> fillGPU(fill.begin(),fill.end());
  thrust::device_vector<T> bGPU(b.begin(),b.end());

  solveKGD(uGPU,fillGPU,bGPU,beta);
  u.resize(uGPU.size());
  thrust::copy(uGPU.begin(),uGPU.end(),u.begin());
}
void solveKKrylov(thrust::device_vector<T>& u,const thrust::device_vector<T>& fill,const thrust::device_vector<T>& b,T beta,int n) {
  _Kry.resize(n*2);
  for(int i=0; i<n*2; i++)
    if(!_Kry[i] || _Kry[i]->size()!=u.size())
      _Kry[i].reset(new thrust::device_vector<T>(u.size()));
  mulK(*_Ku,fill,u);//Ku=K*u
  thrust::transform(_Ku->begin(),_Ku->end(),b.begin(),_r->begin(),thrust::minus<T>());//r=Ku-b
  thrust::copy(_r->begin(),_r->end(),_p->begin());//p=r
  //std::cout << lpNorm<2>(*_r) << " ";//for debug
  Vec coef=Vec::Zero(n);
  for(int i=0; i<n; i++) { //build krylov subspace
    mulK(*_Kry[i],fill,i==0?*_r:*_Kry[i-1]);
    coef[i]=1/lpNorm<2>(i==0?*_r:*_Kry[i-1]);
    thrust::transform(_Kry[i]->begin(),_Kry[i]->end(),_Kry[i]->begin(),ScaleFunctor(coef[i]));
    thrust::copy(_Kry[i]->begin(),_Kry[i]->end(),_Kry[i+n]->begin());
  }
  //compute householder QR
  Vec d=Vec::Zero(n);
  for(int j=0; j<n; j++) {
    T s=sqrt(thrust::inner_product(_Kry[j]->begin()+j,_Kry[j]->end(),_Kry[j]->begin()+j,T(0)));
    T ajj=(*_Kry[j])[j];
    d[j]=ajj>0?-s:s;
    T fak=sqrt(s*(s+abs(ajj)));
    (*_Kry[j])[j]=ajj-d[j];
    thrust::transform(_Kry[j]->begin()+j,_Kry[j]->end(),_Kry[j]->begin()+j,ScaleFunctor(1/fak));
    for(int i=j+1; i<n; i++) {
      s=thrust::inner_product(_Kry[j]->begin()+j,_Kry[j]->end(),_Kry[i]->begin()+j,T(0));
      thrust::transform(_Kry[i]->begin()+j,_Kry[i]->end(),_Kry[j]->begin()+j,_Kry[i]->begin()+j,SAXPYFunctor(-s));
    }
  }
  //mul Q^T
  for(int j=0; j<n; j++) {
    T s=thrust::inner_product(_Kry[j]->begin()+j,_Kry[j]->end(),_r->begin()+j,T(0));
    thrust::transform(_r->begin()+j,_r->end(),_Kry[j]->begin()+j,_r->begin()+j,SAXPYFunctor(-s));
  }
  //solve R (CPU)
  Vec c=Vec::Zero(n),tmpKry=Vec::Zero(n);
  thrust::copy(_r->begin(),_r->begin()+n,c.begin());
  for(int j=n-1; j>=0; j--) {
    c[j]/=d[j];
    thrust::copy(_Kry[j]->begin(),_Kry[j]->begin()+j,tmpKry.begin());
    c.head(j)-=tmpKry.head(j)*c[j];
  }
  //apply
  for(int i=0; i<n; i++)
    thrust::transform(u.begin(),u.end(),(i==0?_p:_Kry[i-1+n])->begin(),u.begin(),SAXPYFunctor(-beta*c[i]*coef[i]));
  //for debug
  //mulK(*_Ku,fill,u);//Ku=K*u//for debug
  //thrust::transform(_Ku->begin(),_Ku->end(),b.begin(),_r->begin(),thrust::minus<T>());//r=Ku-b//for debug
  //std::cout << lpNorm<2>(*_r) << std::endl;//for debug
}
void solveKKrylov(Vec& u,const Vec& fill,const Vec& b,T beta,int n) {
  thrust::device_vector<T> uGPU(u.begin(),u.end());
  thrust::device_vector<T> fillGPU(fill.begin(),fill.end());
  thrust::device_vector<T> bGPU(b.begin(),b.end());

  solveKKrylov(uGPU,fillGPU,bGPU,beta,n);
  u.resize(uGPU.size());
  thrust::copy(uGPU.begin(),uGPU.end(),u.begin());
}
void modifyF(const thrust::device_vector<T>& force,const Vec& sf,const thrust::device_vector<T>& fill,thrust::device_vector<T>& modifiedF) {
#define BLK2D 6
#define BLK3D 6
  if(DIM==2) {
    dim3 nrBlk(NUM_BLOCK(_x+1,BLK2D),NUM_BLOCK(_y+1,BLK2D),1);
    dim3 szBlk(BLK2D+2,BLK2D+2,1);
    modifyF2D<BLK2D><<<nrBlk,szBlk>>>
    (_x,_y,POWRHO,_strideF,_strideU,
     thrust::raw_pointer_cast(_mask->data()),
     thrust::raw_pointer_cast(fill.data()),
     thrust::raw_pointer_cast(force.data()),
     Vec2T(sf),
     thrust::raw_pointer_cast(modifiedF.data()));
  } else {
    dim3 nrBlk(NUM_BLOCK(_x+1,BLK3D),NUM_BLOCK(_y+1,BLK3D),NUM_BLOCK(_z+1,BLK3D));
    dim3 szBlk(BLK3D+2,BLK3D+2,BLK3D+2);
    modifyF3D<BLK3D><<<nrBlk,szBlk>>>
    (_x,_y,_z,POWRHO,_strideF,_strideU,
     thrust::raw_pointer_cast(_mask->data()),
     thrust::raw_pointer_cast(fill.data()),
     thrust::raw_pointer_cast(force.data()),
     Vec3T(sf),
     thrust::raw_pointer_cast(modifiedF.data()));
  }
#undef BLK2D
#undef BLK3D
}
Vec modifyF(const Vec& force,const Vec& sf,const Vec& fill) {
  thrust::device_vector<T> forceGPU(force.begin(),force.end());
  thrust::device_vector<T> modifiedFGPU(force.begin(),force.end());
  thrust::device_vector<T> fillGPU(fill.begin(),fill.end());
  modifyF(forceGPU,sf,fillGPU,modifiedFGPU);

  Vec modifiedF;
  modifiedF.resize(modifiedFGPU.size());
  thrust::copy(modifiedFGPU.begin(),modifiedFGPU.end(),modifiedF.begin());
  return modifiedF;
}
void diffK(thrust::device_vector<T>& diff,const thrust::device_vector<T>& fill,const std::vector<thrust::device_vector<T>>& u,const MatT& sf,bool preserveSum) {
#define BLK2D 7
#define BLK3D 7
  thrust::fill(diff.begin(),diff.end(),0);
  for(int fIndex=0; fIndex<u.size(); fIndex++)
    if(DIM==2) {
      dim3 nrBlk(NUM_BLOCK(_x+1,BLK2D),NUM_BLOCK(_y+1,BLK2D),1);
      dim3 szBlk(BLK2D+1,BLK2D+1,1);
      Eigen::Matrix<T,8,1> sf2D;
      for(int i=0;i<4;i++)
          sf2D.template segment<2>(i*2)=sf.col(fIndex);
      diffK2D<BLK2D><<<nrBlk,szBlk>>>
      (_x,_y,POWRHO,_strideF,_strideU,
       thrust::raw_pointer_cast(_mask->data()),
       thrust::raw_pointer_cast(fill.data()),
       thrust::raw_pointer_cast(u[fIndex].data()),
       sf2D,
       thrust::raw_pointer_cast(diff.data()));
    } else {
      dim3 nrBlk(NUM_BLOCK(_x+1,BLK3D),NUM_BLOCK(_y+1,BLK3D),NUM_BLOCK(_z+1,BLK3D));
      dim3 szBlk(BLK3D+1,BLK3D+1,BLK3D+1);
      Eigen::Matrix<T,24,1> sf3D;
      for(int i=0;i<8;i++)
          sf3D.template segment<3>(i*3)=sf.col(fIndex);
      diffK3D<BLK3D><<<nrBlk,szBlk>>>
      (_x,_y,_z,POWRHO,_strideF,_strideU,
       thrust::raw_pointer_cast(_mask->data()),
       thrust::raw_pointer_cast(fill.data()),
       thrust::raw_pointer_cast(u[fIndex].data()),
       sf3D,
       thrust::raw_pointer_cast(diff.data()));
    }
  ScaleFunctor op(1.0/u.size());
  thrust::transform(diff.begin(),diff.end(),diff.begin(),op);
#undef BLK2D
#undef BLK3D
  if(preserveSum) {
    T sumFill=thrust::reduce(fill.begin(),fill.end(),T(0),thrust::plus<T>());
    if(sumFill<_sumFill-1) {
      if(!_fCompact) {
        T mean=thrust::reduce(diff.begin(),diff.end(),T(0),thrust::plus<T>())/(T)diff.size();
        thrust::transform(diff.begin(),diff.end(),diff.begin(),AddFunctor(-mean));
      } else {
        compactFMask(*_fCompact,diff);
        T mean=thrust::reduce(_fCompact->begin(),_fCompact->end(),T(0),thrust::plus<T>())/(T)_fCompact->size();
        thrust::transform(_fCompact->begin(),_fCompact->end(),_fCompact->begin(),AddFunctor(-mean));
        uncompactFMask(diff,*_fCompact);
      }
    }
  }
}
Vec diffK(const Vec& fill,const std::vector<Vec>& u,const MatT& sf,bool preserveSum) {
  thrust::device_vector<T> fillGPU(fill.begin(),fill.end());
  thrust::device_vector<T> diffGPU(fill.size());
  std::vector<thrust::device_vector<T>> uGPU(u.size());
  for(int fIndex=0; fIndex<u.size(); fIndex++)
    uGPU[fIndex].assign(u[fIndex].begin(),u[fIndex].end());
  diffK(diffGPU,fillGPU,uGPU,sf,preserveSum);

  Vec diff;
  diff.resize(diffGPU.size());
  thrust::copy(diffGPU.begin(),diffGPU.end(),diff.begin());
  return diff;
}
void projX(thrust::device_vector<T>& fillProj,const thrust::device_vector<T>& fill) {
#define BLK 256
  {
    dim3 nrBlk(NUM_BLOCK(fill.size(),BLK),1,1);
    dim3 szBlk(BLK,1,1);
    projXFillVss<<<nrBlk,szBlk>>>
    (_minFill,fill.size(),
     thrust::raw_pointer_cast(fill.data()),
     thrust::raw_pointer_cast(_vss->data()));
  }
  thrust::stable_sort(_vss->begin(),_vss->begin()+fill.size()*2,SortFunctor());
  thrust::transform(_vss->begin(),_vss->begin()+fill.size()*2,_active->begin(),FillActiveFunctor());
  thrust::exclusive_scan(_active->begin(),_active->begin()+fill.size()*2,_active->begin(),int(0));
  //for debug only
  //for(int i=1; i<(int)_active->size(); i++)
  //  std::cout << "activeGPU[" << i << "]=" << (*_active)[i] << std::endl;
  {
    dim3 nrBlk(NUM_BLOCK(fill.size()*2,BLK),1,1);
    dim3 szBlk(BLK,1,1);
    projXComputeTotal<<<nrBlk,szBlk>>>
    (fill.size()*2,
     thrust::raw_pointer_cast(_total->data()),
     thrust::raw_pointer_cast(_active->data()),
     thrust::raw_pointer_cast(_vss->data()));
  }
  thrust::exclusive_scan(_total->begin(),_total->begin()+fill.size()*2,_total->begin(),T(_minFill*fill.size()));
  //for(int i=1; i<(int)_total->size(); i++)
  //  std::cout << "totalGPU[" << i << "]=" << (*_total)[i] << std::endl;
  {
    (*_lambda)[0]=0;
    dim3 nrBlk(NUM_BLOCK(fill.size()*2,BLK),1,1);
    dim3 szBlk(BLK,1,1);
    projXComputeLambda<<<nrBlk,szBlk>>>
    (_sumFill,fill.size()*2,
     thrust::raw_pointer_cast(_lambda->data()),
     thrust::raw_pointer_cast(_total->data()),
     thrust::raw_pointer_cast(_active->data()),
     thrust::raw_pointer_cast(_vss->data()));
    //std::cout << "lambdaGPU=" << (*_lambda)[0] << std::endl;
  }
  thrust::transform(fill.begin(),fill.end(),fillProj.begin(),ClampFillFunctor(_minFill,(*_lambda)[0]));
#undef BLK
}
void projXFMask(thrust::device_vector<T>& fillProj,const thrust::device_vector<T>& fill) {
  if(!_fCompact)
    projX(fillProj,fill);
  else {
    compactFMask(*_fCompact,fill);
    projX(*_fCompactOut,*_fCompact);
    uncompactFMask(fillProj,*_fCompactOut);
  }
}
Vec projX(const Vec& fill) {
  thrust::device_vector<T> fillGPU(fill.begin(),fill.end());
  thrust::device_vector<T> fillProjGPU(fill.size());

  Vec fillProj;
  fillProj.resize(fill.size());
  projX(fillProjGPU,fillGPU);
  thrust::copy(fillProjGPU.begin(),fillProjGPU.end(),fillProj.begin());
  return fillProj;
}
void initX(thrust::device_vector<T>& f) {
  if(!_fCompact)
    thrust::fill(f.begin(),f.end(),_sumFill/f.size());
  else {
    compactFMask(*_fCompact,f);
    thrust::fill(_fCompact->begin(),_fCompact->end(),_sumFill/_fCompact->size());
    uncompactFMask(f,*_fCompact);
  }
}
extern bool hasFilter();
extern void forward(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const thrust::device_vector<T>& u,thrust::device_vector<T>& Fu);
extern void backward(int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,const thrust::device_vector<T>& DEDFu,thrust::device_vector<T>& DEDu);
extern void applySymmetry(const Eigen::Matrix<bool,3,1>& mask,int X,int Y,int Z,const Eigen::Matrix<int,3,1>& stride,thrust::device_vector<T>& diffK);
void filterForward(const thrust::device_vector<T>& u,thrust::device_vector<T>& Fu) {
  if(hasFilter())
    forward(_x,_y,_z,_strideF,u,Fu);
  else thrust::copy(u.begin(),u.end(),Fu.begin());
}
void filterBackward(const thrust::device_vector<T>& DEDFu,thrust::device_vector<T>& DEDu) {
  if(hasFilter())
    backward(_x,_y,_z,_strideF,DEDFu,DEDu);
  else thrust::copy(DEDFu.begin(),DEDFu.end(),DEDu.begin());
}
void solveUnilevel(thrust::device_vector<T>& x,const std::vector<thrust::device_vector<T>>& f,const MatT& sf,T alpha0,T m) {
  thrust::device_vector<T> fModified(xU()*yU()*zU()*DIM);
  std::vector<thrust::device_vector<T>> u(f.size());
  for(int fIndex=0; fIndex<f.size(); fIndex++) {
    u[fIndex].resize(xU()*yU()*zU()*DIM);
    thrust::fill(u[fIndex].begin(),u[fIndex].end(),0);
  }
  thrust::device_vector<T> xLast(_x*_y*_z),xProj(_x*_y*_z),xC(_x*_y*_z);
  initX(x);
  filterForward(x,xC);
  memoryCost();
  TBEG("Grand-Timing");
  for(int k=1,k2=0;; k++) {
    TBEG("UnilevelGPU-Iteration-D="+std::to_string(_D));
    T alphak=alpha0/std::pow<T>(k,m);
    int iters=0;
    for(int fIndex=0; fIndex<f.size(); fIndex++)
      if(sf.size()==0)
        solveK(u[fIndex],xC,f[fIndex],DEFAULT_CG_TOL);
      else {
        modifyF(f[fIndex],sf.col(fIndex),xC,fModified);
        solveK(u[fIndex],xC,fModified,DEFAULT_CG_TOL);
      }
    thrust::copy(x.begin(),x.end(),xLast.begin());
    diffK(xProj,xC,u,sf,DEFAULT_PRESERVE_SUM);  //xProj=diffK(xC,u,true)
    filterBackward(xProj,xC);  //xC=filterBackward(xProj)
    thrust::transform(x.begin(),x.end(),xC.begin(),x.begin(),SAXPYFunctor(alphak)); //x=x+alpha*xC
    applySymmetry(_symmetry,_x,_y,_z,_strideF,x);
    projXFMask(xProj,x); //xProj=projX(x)
    xProj.swap(x);  //xProj.swap(x)
    filterForward(x,xC);  //xC=filterForward(x)
    TEND();
    if(!_writePath.empty() && (k%_writeInterval)==0) {
      Vec uCPU=Vec::Zero(u[0].size());
      Vec xCPU=Vec::Zero(xC.size());
      const auto& xGPU=OUTPUT_FILTERED?xC:x;
      thrust::copy(xGPU.begin(),xGPU.end(),xCPU.begin());
      for(int fIndex=0; fIndex<f.size(); fIndex++) {
        thrust::copy(u[fIndex].begin(),u[fIndex].end(),uCPU.begin());
        _cb(_writeMul,&uCPU,&xCPU,_writePath,k2,fIndex);
      }
      k2++;
    }
    if((k%std::abs(_reportInterval))==0) {
      //residual
      //mulK(*_Ku,xC,u);  //_Ku=mulK(xC,u)
      //thrust::transform(_Ku->begin(),_Ku->end(),f.begin(),_p->begin(),thrust::minus<T>());  //_p=_Ku-f
      //T rNorm=lpNorm<NORM_TYPE>(*_p);
      //stopping criterion: diffNorm
      thrust::transform(x.begin(),x.end(),xLast.begin(),xProj.begin(),thrust::minus<T>());  //xProj=x-xLast
      T relDiff=lpNorm<NORM_TYPE>(xProj)/alphak;
      T relDiffCellwise=(NORM_TYPE==2?relDiff:lpNorm<2>(xProj)/alphak)/(_x*_y*_z);
      std::cout << "Iter=" << std::setw(7) << k <<
                " alpha=" << std::setw(15) << alphak <<
                " lowLevelIter=" << std::setw(15) << (iters<0?std::string("Direct"):std::to_string(iters)) <<
                " relDifference=" << std::setw(15) << relDiff <<
                " relDifferenceCellwise=" << std::setw(15) << relDiffCellwise <<
                " elapsedTime=" << std::setw(15) << TENDVPeek();
      if(_reportInterval<0) {
        T energy=0;
        for(int fIndex=0; fIndex<f.size(); fIndex++) {
          solveK(*_p,xC,f[fIndex],0);
          energy+=thrust::inner_product(f[fIndex].begin(),f[fIndex].end(),_p->begin(),T(0));
        }
        std::cout << " energy=" << std::setw(15) << energy/2;
      }
      std::cout << std::endl;
      if(relDiff*alphak<_tolX || k>=_maxIter)
        break;
    }
  }
  TENDV();
  thrust::copy(xC.begin(),xC.end(),x.begin());
}
Vec solveUnilevel(const MatT& f,const MatT& sf,T alpha0,T m) {
  std::vector<thrust::device_vector<T>> fGPU(f.cols());
  for(int fIndex=0; fIndex<f.cols(); fIndex++)
    fGPU[fIndex].assign(f.col(fIndex).begin(),f.col(fIndex).end());
  thrust::device_vector<T> xGPU(_x*_y*_z);
  solveUnilevel(xGPU,fGPU,sf,alpha0,m);

  Vec x;
  x.resize(xGPU.size());
  thrust::copy(xGPU.begin(),xGPU.end(),x.begin());
  return x;
}
void solveBilevel(thrust::device_vector<T>& x,const std::vector<thrust::device_vector<T>>& f,const MatT& sf,T beta0,T alpha0,T m) {
  thrust::device_vector<T> fModified(xU()*yU()*zU()*DIM);
  std::vector<thrust::device_vector<T>> u(f.size());
  for(int fIndex=0; fIndex<f.size(); fIndex++) {
    u[fIndex].resize(xU()*yU()*zU()*DIM);
    thrust::fill(u[fIndex].begin(),u[fIndex].end(),0);
  }
  thrust::device_vector<T> xLast(_x*_y*_z),xProj(_x*_y*_z),xC(_x*_y*_z);
  initX(x);
  filterForward(x,xC);
  memoryCost();
  if(_D>0)
    for(int fIndex=0; fIndex<f.size(); fIndex++)
      solveK(u[fIndex],xC,f[fIndex],DEFAULT_CG_TOL_INIT);
  TBEG("Grand-Timing");
  for(int k=1,k2=0;; k++) {
    TBEG("BilevelGPU-Iteration-D="+std::to_string(_D));
    T alphak=alpha0/std::pow<T>(k,m);
    TBEG("BilevelGPU-Solve");
    for(int fIndex=0; fIndex<f.size(); fIndex++)
      if(sf.size()==0) {
        if(abs(_D)>1)
          solveKKrylov(u[fIndex],xC,f[fIndex],beta0,abs(_D));
        else solveKGD(u[fIndex],xC,f[fIndex],beta0);
      } else {
        modifyF(f[fIndex],sf.col(fIndex),xC,fModified);
        if(abs(_D)>1)
          solveKKrylov(u[fIndex],xC,fModified,beta0,abs(_D));
        else solveKGD(u[fIndex],xC,fModified,beta0);
      }
    //solveK(u,xC,f);
    TEND();
    thrust::copy(x.begin(),x.end(),xLast.begin());
    diffK(xProj,xC,u,sf,DEFAULT_PRESERVE_SUM);  //xProj=diffK(xC,u,true)
    filterBackward(xProj,xC);  //xC=filterBackward(xProj)
    thrust::transform(x.begin(),x.end(),xC.begin(),x.begin(),SAXPYFunctor(alphak)); //x=x+alpha*xC
    applySymmetry(_symmetry,_x,_y,_z,_strideF,x);
    projXFMask(xProj,x); //xProj=projX(x)
    xProj.swap(x);  //xProj.swap(x)
    filterForward(x,xC);  //xC=filterForward(x)
    TEND();
    if(!_writePath.empty() && (k%_writeInterval)==0) {
      Vec uCPU=Vec::Zero(u[0].size());
      Vec xCPU=Vec::Zero(xC.size());
      const auto& xGPU=OUTPUT_FILTERED?xC:x;
      thrust::copy(xGPU.begin(),xGPU.end(),xCPU.begin());
      for(int fIndex=0; fIndex<f.size(); fIndex++) {
        thrust::copy(u[fIndex].begin(),u[fIndex].end(),uCPU.begin());
        _cb(_writeMul,&uCPU,&xCPU,_writePath,k2,fIndex);
      }
      k2++;
    }
    if((k%std::abs(_reportInterval))==0) {
      //residual
      T lowLevel=0;
      for(int fIndex=0; fIndex<f.size(); fIndex++) {
        mulK(*_Ku,xC,u[fIndex]);  //_Ku=mulK(xC,u)
        thrust::transform(_Ku->begin(),_Ku->end(),f[fIndex].begin(),_p->begin(),thrust::minus<T>());  //_p=_Ku-f
        lowLevel+=lpNorm<NORM_TYPE>(*_p);
      }
      //stopping criterion: diffNorm
      thrust::transform(x.begin(),x.end(),xLast.begin(),xProj.begin(),thrust::minus<T>());  //xProj=x-xLast
      T relDiff=lpNorm<NORM_TYPE>(xProj)/alphak;
      T relDiffCellwise=(NORM_TYPE==2?relDiff:lpNorm<2>(xProj)/alphak)/(_x*_y*_z);
      std::cout << "Iter=" << std::setw(7) << k <<
                " alpha=" << std::setw(15) << alphak <<
                " beta=" << std::setw(15) << beta0 <<
                " lowLevelErr=" << std::setw(15) << lowLevel <<
                " relDifference=" << std::setw(15) << relDiff <<
                " relDifferenceCellwise=" << std::setw(15) << relDiffCellwise <<
                " elapsedTime=" << std::setw(15) << TENDVPeek();
      if(_reportInterval<0) {
        T energy=0;
        for(int fIndex=0; fIndex<f.size(); fIndex++) {
          solveK(*_p,xC,f[fIndex],0);
          energy+=thrust::inner_product(f[fIndex].begin(),f[fIndex].end(),_p->begin(),T(0));
        }
        std::cout << " energy=" << std::setw(15) << energy/2;
      }
      std::cout << std::endl;
      if((lowLevel<_tol && relDiff*alphak<_tolX) || k>=_maxIter)
        break;
    }
  }
  TENDV();
  thrust::copy(xC.begin(),xC.end(),x.begin());
}
Vec solveBilevel(const MatT& f,const MatT& sf,T beta0,T alpha0,T m) {
  std::vector<thrust::device_vector<T>> fGPU(f.cols());
  for(int fIndex=0; fIndex<f.cols(); fIndex++)
    fGPU[fIndex].assign(f.col(fIndex).begin(),f.col(fIndex).end());
  thrust::device_vector<T> xGPU(_x*_y*_z);
  solveBilevel(xGPU,fGPU,sf,beta0,alpha0,m);

  Vec x;
  x.resize(xGPU.size());
  thrust::copy(xGPU.begin(),xGPU.end(),x.begin());
  return x;
}
}
}
