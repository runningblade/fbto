import matplotlib.pyplot as plt
import pickle,copy,math
import numpy as np
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['font.size'] = 15

from operator import sub
def get_aspect(ax):
    # Total figure size
    figW, figH = ax.get_figure().get_size_inches()
    # Axis size on figure
    _, _, w, h = ax.get_position().bounds
    # Ratio of display units
    disp_ratio = (figH * h) / (figW * w)
    # Ratio of data units
    # Negative over negative because of the order of subtraction
    data_ratio = sub(*ax.get_ylim()) / sub(*ax.get_xlim())
    return disp_ratio / data_ratio

def parseError(path,nrCell):
    time=[]
    lowLevel=[]
    highLevel=[]
    energy=[]
    file=open(path,'r')
    lines=file.readlines()
    for line in lines:
        terms=[t.replace('\n','') for t in line.split(' ') if t!='']
        for i in range(len(terms)):
            if terms[i]=='elapsedTime=':
                time.append(float(terms[i+1]))
            if terms[i]=='lowLevelErr=':
                lowLevel.append(float(terms[i+1])/math.sqrt(nrCell))
            if terms[i]=='relDifferenceCellwise=':
                highLevel.append(float(terms[i+1]))
            if terms[i]=='energy=':
                energy.append(float(terms[i+1]))
    return time,lowLevel,highLevel,energy

if __name__=='__main__':
    time,lowLevel,highLevel,energy=parseError('LShapeCompareBilevel2D/LShapeBilevel2DEnergy/output.txt',320*320)
    time,_,_,_=parseError('LShapeCompareBilevel2D/LShapeBilevel2D/output.txt',320*320)
    
    time2,lowLevel2,highLevel2,energy2=parseError('LShapeCompareBilevel2D/LShapeUnilevel2DEnergy/output.txt',320*320)
    time2,_,_,_=parseError('LShapeCompareBilevel2D/LShapeUnilevel2D/output.txt',320*320)
    
    #plot low- and high-level error
    plt.ylabel('$\log(\Xi_k), \log(\Delta_k^v)$')#'Log of Error Metric')
    plt.xlabel('#Iteration($\\times10^3$)')
    plt.plot([i/1000 for i,t in enumerate(time)],[math.log(e) for e in lowLevel],label='Low-Level ($\Xi_k$)')
    plt.plot([i/1000 for i,t in enumerate(time)],[math.log(e) for e in highLevel],label='High-Level ($\Delta_k^v$)')
    plt.grid()
    plt.legend()
    plt.yticks(np.arange(-8,-2,step=1))
    plt.axes().set_aspect(get_aspect(plt.axes())*0.5)
    plt.savefig('LShapeError.pdf',bbox_inches='tight')
    plt.show()
    
    #a hack of alignment
    delta=time2[0]-time[0]
    time2=[t-delta for t in time2]
    #plot log of energy
    plt.ylabel('$\log[l(v_k)]$')
    plt.xlabel('Elapsed Time (s)')
    plt.plot(time,[math.log(e) for e in energy],label='Bilevel')
    plt.plot(time2,[math.log(e) for e in energy2],label='Single-Level (PGD)')
    plt.grid()
    plt.legend()
    plt.yticks(np.arange(7,15,step=1))
    plt.axes().set_aspect(get_aspect(plt.axes())*0.5)
    plt.savefig('LShapeTime.pdf',bbox_inches='tight')
    plt.show()
