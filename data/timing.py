import matplotlib.pyplot as plt
import pickle,copy,math
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['font.size'] = 15

def parseCost(path):
    blkSize=[]
    blkCPU=[]
    blkGPU=[]
    CPUCost=[]
    GPUCost=[]
    file=open(path,'r')
    lines=file.readlines()
    for line in lines:
        if line.startswith('BlockSize='):
            if len(CPUCost)>0:
                blkCPU.append(sum(CPUCost)/len(CPUCost))
                blkGPU.append(sum(GPUCost)/len(GPUCost))
            blkSize.append(int(line[len('BlockSize='):]))
            CPUCost=[]
            GPUCost=[]
        elif line.startswith('Bilevel-Iteration-D=-20: '):
            CPUCost.append(float(line[len('Bilevel-Iteration-D=-20: '):]))
        elif line.startswith('BilevelGPU-Iteration-D=-20: '):
            GPUCost.append(float(line[len('BilevelGPU-Iteration-D=-20: '):]))
    if len(CPUCost)>0:
        blkCPU.append(sum(CPUCost)/len(CPUCost))
        blkGPU.append(sum(GPUCost)/len(GPUCost))
    return blkSize,blkCPU,blkGPU

if __name__=='__main__':
    blkSize,CPUCost,GPUCost=parseCost('timing.txt')
    blkSize=[math.log(blk)/math.log(2) for blk in blkSize]
    speedup=[CPU/GPU for CPU,GPU in zip(CPUCost,GPUCost)]
    
    fig,ax1=plt.subplots()
    
    ax1.set_ylabel('Iterative Cost (s)')
    ax1.set_xlabel('$\log_2 E/2$')
    ax1.plot(blkSize,CPUCost,marker='o',label='CPU (6-Core Parallel)')
    ax1.plot(blkSize,GPUCost,marker='o',label='GPU (1280-Core Cuda)')
    ax1.legend()
    ax1.grid()
    
    ax2 = ax1.twinx()
    ax2.set_ylabel('Speedup',color='tab:red')
    ax2.plot(blkSize,speedup,marker='o',label='Speedup',color='tab:red')
    ax2.tick_params(axis='y',labelcolor='tab:red')
    plt.savefig('timing.pdf',bbox_inches='tight')
    plt.show()
