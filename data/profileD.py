import matplotlib.pyplot as plt
import pickle,copy,math
import numpy as np
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['font.size'] = 15

from operator import sub
def get_aspect(ax):
    # Total figure size
    figW, figH = ax.get_figure().get_size_inches()
    # Axis size on figure
    _, _, w, h = ax.get_position().bounds
    # Ratio of display units
    disp_ratio = (figH * h) / (figW * w)
    # Ratio of data units
    # Negative over negative because of the order of subtraction
    data_ratio = sub(*ax.get_ylim()) / sub(*ax.get_xlim())
    return disp_ratio / data_ratio

def parseError(path,nrCell):
    time=[]
    lowLevel=[]
    highLevel=[]
    energy=[]
    file=open(path,'r')
    lines=file.readlines()
    for line in lines:
        terms=[t.replace('\n','') for t in line.split(' ') if t!='']
        for i in range(len(terms)):
            if terms[i]=='elapsedTime=':
                time.append(float(terms[i+1]))
            if terms[i]=='lowLevelErr=':
                lowLevel.append(float(terms[i+1])/math.sqrt(nrCell))
            if terms[i]=='relDifferenceCellwise=':
                highLevel.append(float(terms[i+1]))
            if terms[i]=='energy=':
                energy.append(float(terms[i+1]))
    return time,lowLevel,highLevel,energy

def parseCantilever(dCons=None):
    dss=[]
    ass=[]
    ess=[]
    tss=[]
    #setting
    alpha=[0.04,0.08,0.16,0.32,0.64]
    D=[5,10,15,20,25]
    ID=9
    if dCons is not None:
        D=[dCons]
    #read
    for d in D:
        for a in alpha:
            aStr=str(a)
            while len(aStr)<8:
                aStr+='0'
            name='CantileverCompareBilevel2D/CantileverBilevel2D('+str(d)+')('+aStr+')/output.txt'
            time,lowLevel,highLevel,energy=parseError(name,64*128)
            #add
            dss.append(d)
            ass.append(a)
            ess.append(energy[ID])
            tss.append(time[ID])
    return dss,ass,ess,tss

if __name__=='__main__':
    plt.ylabel('Final $\log[l(v_k)]$')
    plt.xlabel('$\log_2[\\alpha_0]$')
    #dCons=5
    dss,ass,ess,tss=parseCantilever(dCons=5)
    plt.plot([math.log(a) for a in ass],[math.log(e) for e in ess],'-o',label='$D=5$')
    #dCons=5
    dss,ass,ess,tss=parseCantilever(dCons=10)
    plt.plot([math.log(a) for a in ass],[math.log(e) for e in ess],'-o',label='$D=10$')
    #dCons=15
    dss,ass,ess,tss=parseCantilever(dCons=15)
    plt.plot([math.log(a) for a in ass],[math.log(e) for e in ess],'-o',label='$D=15$')
    #dCons=20
    dss,ass,ess,tss=parseCantilever(dCons=20)
    plt.plot([math.log(a) for a in ass],[math.log(e) for e in ess],'-o',label='$D=20$')
    #dCons=25
    #dss,ass,ess,tss=parseCantilever(dCons=25)
    #plt.plot([math.log(a) for a in ass],[math.log(e) for e in ess],'-o',label='$D=25$')
    plt.grid()
    plt.legend(loc='upper left',ncol=1)
    plt.yticks(np.arange(8,15,step=1))
    plt.axes().set_aspect(get_aspect(plt.axes())*0.7)
    plt.savefig('CompareD.pdf',bbox_inches='tight')
    plt.show()
