import matplotlib.pyplot as plt
import pickle,copy,math
import numpy as np

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)

from operator import sub
def get_aspect(ax):
    # Total figure size
    figW, figH = ax.get_figure().get_size_inches()
    # Axis size on figure
    _, _, w, h = ax.get_position().bounds
    # Ratio of display units
    disp_ratio = (figH * h) / (figW * w)
    # Ratio of data units
    # Negative over negative because of the order of subtraction
    data_ratio = sub(*ax.get_ylim()) / sub(*ax.get_xlim())
    return disp_ratio / data_ratio

def parseError(path,nrCell):
    time=[]
    lowLevel=[]
    highLevel=[]
    energy=[]
    file=open(path,'r')
    lines=file.readlines()
    for line in lines:
        terms=[t.replace('\n','') for t in line.split(' ') if t!='']
        for i in range(len(terms)):
            if terms[i]=='elapsedTime=':
                time.append(float(terms[i+1]))
            if terms[i]=='lowLevelErr=':
                lowLevel.append(float(terms[i+1])/math.sqrt(nrCell))
            if terms[i]=='relDifferenceCellwise=':
                highLevel.append(float(terms[i+1]))
            if terms[i]=='energy=':
                energy.append(float(terms[i+1]))
    return time,lowLevel,highLevel,energy

if __name__=='__main__':
    time,lowLevel,highLevel,energy=parseError('BridgeBilevel2DC/output.txt',192*576)
    time2,lowLevel2,highLevel2,energy2=parseError('BridgeUnilevel2DC/output.txt',192*576)
    time=[t/time[-1]*len(time)*100*0.020 for t in time]
    
    #plot log of energy
    plt.ylabel('$\log[l(v_k)]$')
    plt.xlabel('Elapsed Time (s)')
    plt.plot(time,[math.log(e) for e in energy],label='Bilevel')
    plt.plot(time2,[math.log(e) for e in energy2],label='Single-Level (PGD)')
    plt.legend(loc='upper right')
    plt.grid()
    plt.xlim(0,200)
    plt.xticks(np.arange(0,201,step=50))
    plt.yticks(np.arange(9,13,step=1))
    plt.axes().set_aspect(get_aspect(plt.axes())*0.5)
    plt.savefig('BridgeCTime.pdf',bbox_inches='tight')
    plt.show()