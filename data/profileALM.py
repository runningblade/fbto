import matplotlib.pyplot as plt
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage,
                                  AnnotationBbox)
import pickle,copy,math
import numpy as np
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['font.size'] = 15

from operator import sub
def get_aspect(ax):
    # Total figure size
    figW, figH = ax.get_figure().get_size_inches()
    # Axis size on figure
    _, _, w, h = ax.get_position().bounds
    # Ratio of display units
    disp_ratio = (figH * h) / (figW * w)
    # Ratio of data units
    # Negative over negative because of the order of subtraction
    data_ratio = sub(*ax.get_ylim()) / sub(*ax.get_xlim())
    return disp_ratio / data_ratio

def parseFBTOError(path,nrCell):
    time=[]
    lowLevel=[]
    highLevel=[]
    energy=[]
    file=open(path,'r')
    lines=file.readlines()
    for line in lines:
        terms=[t.replace('\n','') for t in line.split(' ') if t!='']
        for i in range(len(terms)):
            if terms[i]=='elapsedTime=':
                time.append(float(terms[i+1]))
            if terms[i]=='lowLevelErr=':
                lowLevel.append(float(terms[i+1])/math.sqrt(nrCell))
            if terms[i]=='relDifferenceCellwise=':
                highLevel.append(float(terms[i+1]))
            if terms[i]=='energy=':
                energy.append(float(terms[i+1]))
    return time,lowLevel,highLevel,energy

def parseSQPError(path,nrCell):
    energy=[]
    file=open(path,'r')
    lines=file.readlines()
    for line in lines:
        terms=[t.replace('\n','') for t in line.split(' ') if t!='']
        if terms[0].isnumeric():
            energy.append(float(terms[1]))
        if terms[0]=="Total" and terms[1]=="program":
            totalTime=float(terms[5])
    return [(id+1)*totalTime/len(energy) for id,_ in enumerate(energy)],energy

def parseALMError(path):
    time=[]
    energy=[]
    file=open(path,'r')
    lines=file.readlines()
    for line in lines:
        terms=[t.replace('\n','') for t in line.split(' ') if t!='']
        for i in range(len(terms)):
            if terms[i]=='energy=':
                energy.append(float(terms[i+1]))
            if terms[i]=='ALM-Solve:':
                totalTime=float(terms[i+1])
    return [totalTime*id/len(energy) for id,_ in enumerate(energy)],energy

if __name__=='__main__':
    RES=2
    time,lowLevel,highLevel,energy=parseFBTOError("BridgeCompareSQP/BridgeFBTOBilevel2DB("+str(RES)+")/output.txt",32*RES*64*RES)
    timeALM,energyALM=parseALMError("BridgeCompareSQP/BridgeALMBilevel2DB("+str(1)+")/output.txt")
    ax=plt.axes()
    
    #draw ALM
    fracs=[0.01,0.5,0.9]
    offsets=[(100,-10),(80,110),(-20,50)]
    names=['BridgeALM1.png','BridgeALM2.png','BridgeALM4.png']
    for frac,offset,name in zip(fracs,offsets,names):
        id=int(len(timeALM)*frac)
        xy=(timeALM[id],math.log(energyALM[id]))
        ax.plot(xy[0],xy[1],".r")
        # Annotate the 1st position with a text box ('Test 1')
        ALMImg=plt.imread(name)
        ALMImg=ALMImg[50:ALMImg.shape[0]-50,200:ALMImg.shape[1]-200,:]
        imagebox=OffsetImage(ALMImg,zoom=0.1)
        imagebox.image.axes=ax
        ab=AnnotationBbox(imagebox,xy,
                          xybox=offset,
                          xycoords='data',
                          boxcoords="offset points",
                          arrowprops=dict(arrowstyle="->"))
        ax.add_artist(ab)
    
    #draw plot
    ax.set_ylabel('$\log[l(v_k)]$')
    ax.set_xlabel('Elapsed Time (s)')
    #ax.plot([t for t in time]   ,[math.log(e) for e in energy]   ,label='FBTO')
    ax.plot([t for t in timeALM],[math.log(e) for e in energyALM],label='ALM')
    ax.grid()
    ax.set_ylim(5,11)
    #ax.legend(loc='upper left',ncol=2)
    plt.axes().set_aspect(get_aspect(plt.axes())*0.7)
    plt.savefig('ALM.pdf',bbox_inches='tight')
    plt.show()
