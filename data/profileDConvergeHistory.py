import matplotlib.pyplot as plt
import pickle,copy,math
import numpy as np
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['font.size'] = 15

from operator import sub
def get_aspect(ax):
    # Total figure size
    figW, figH = ax.get_figure().get_size_inches()
    # Axis size on figure
    _, _, w, h = ax.get_position().bounds
    # Ratio of display units
    disp_ratio = (figH * h) / (figW * w)
    # Ratio of data units
    # Negative over negative because of the order of subtraction
    data_ratio = sub(*ax.get_ylim()) / sub(*ax.get_xlim())
    return disp_ratio / data_ratio

def parseError(path,nrCell):
    time=[]
    lowLevel=[]
    highLevel=[]
    energy=[]
    file=open(path,'r')
    lines=file.readlines()
    for line in lines:
        terms=[t.replace('\n','') for t in line.split(' ') if t!='']
        for i in range(len(terms)):
            if terms[i]=='elapsedTime=':
                time.append(float(terms[i+1]))
            if terms[i]=='lowLevelErr=':
                lowLevel.append(float(terms[i+1])/math.sqrt(nrCell))
            if terms[i]=='relDifferenceCellwise=':
                highLevel.append(float(terms[i+1]))
            if terms[i]=='energy=':
                energy.append(float(terms[i+1]))
    return time,lowLevel,highLevel,energy

if __name__=='__main__':
    time, lowLevel, highLevel, energy =parseError('CantileverCompareDBilevel2D/CantileverBilevel2D(0)(0.001000)/output.txt',64*128)
    time2,lowLevel2,highLevel2,energy2=parseError('CantileverCompareDBilevel2D/CantileverBilevel2D(5)(0.250000)/output.txt',64*128)
    time3,lowLevel3,highLevel3,energy3=parseError('CantileverCompareDBilevel2D/CantileverBilevel2D(20)(0.250000)/output.txt',64*128)
    
    #a hack of alignment
    delta=time2[0]-time[0]
    time2=[t-delta for t in time2]
    #plot log of energy
    plt.ylabel('$\log[l(v_k)]$')
    plt.xlabel('Elapsed Time (s)')
    plt.plot(time,[math.log(e) for e in energy]  ,label='Algorithm 1, $\\alpha_0$=0.001')
    plt.plot(time2,[math.log(e) for e in energy2],label='Algorithm 3, D=5, $\\alpha_0$=0.25')
    plt.plot(time3,[math.log(e) for e in energy3],label='Algorithm 3, D=20, $\\alpha_0$=0.25')
    plt.grid()
    plt.legend()
    plt.xlim(0,600)
    plt.yticks(np.arange(8,17,step=1))
    plt.axes().set_aspect(get_aspect(plt.axes())*0.5)
    plt.savefig('CantileverD.pdf',bbox_inches='tight')
    plt.show()
