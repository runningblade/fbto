import matplotlib.pyplot as plt
import pickle,copy,math
import numpy as np
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['font.size'] = 15

from operator import sub
def get_aspect(ax):
    # Total figure size
    figW, figH = ax.get_figure().get_size_inches()
    # Axis size on figure
    _, _, w, h = ax.get_position().bounds
    # Ratio of display units
    disp_ratio = (figH * h) / (figW * w)
    # Ratio of data units
    # Negative over negative because of the order of subtraction
    data_ratio = sub(*ax.get_ylim()) / sub(*ax.get_xlim())
    return disp_ratio / data_ratio

def parseError(path,nrCell):
    time=[]
    lowLevel=[]
    highLevel=[]
    energy=[]
    file=open(path,'r')
    lines=file.readlines()
    for line in lines:
        terms=[t.replace('\n','') for t in line.split(' ') if t!='']
        for i in range(len(terms)):
            if terms[i]=='elapsedTime=':
                time.append(float(terms[i+1]))
            if terms[i]=='lowLevelErr=':
                lowLevel.append(float(terms[i+1])/math.sqrt(nrCell))
            if terms[i]=='relDifferenceCellwise=':
                highLevel.append(float(terms[i+1]))
            if terms[i]=='energy=':
                energy.append(float(terms[i+1]))
    return time,lowLevel,highLevel,energy

if __name__=='__main__':
    time1, lowLevel1, highLevel1, energy1 =parseError('TeaserCompareBilevel2D/TeaserCompareBilevel2D(1)/output.txt', 256*256)
    time2, lowLevel2, highLevel2, energy2 =parseError('TeaserCompareBilevel2D/TeaserCompareBilevel2D(2)/output.txt', 256*256)
    time3, lowLevel3, highLevel3, energy3 =parseError('TeaserCompareBilevel2D/TeaserCompareBilevel2D(3)/output.txt', 256*256)
    time4, lowLevel4, highLevel4, energy4 =parseError('TeaserCompareBilevel2D/TeaserCompareBilevel2D(4)/output.txt', 256*256)
    time5, lowLevel5, highLevel5, energy5 =parseError('TeaserCompareBilevel2D/TeaserCompareBilevel2D(5)/output.txt', 256*256)
    time6, lowLevel6, highLevel6, energy6 =parseError('TeaserCompareBilevel2D/TeaserCompareBilevel2D(6)/output.txt', 256*256)
    time7, lowLevel7, highLevel7, energy7 =parseError('TeaserCompareBilevel2D/TeaserCompareBilevel2D(7)/output.txt', 256*256)
    time8, lowLevel8, highLevel8, energy8 =parseError('TeaserCompareBilevel2D/TeaserCompareBilevel2D(8)/output.txt', 256*256)
    time9, lowLevel9, highLevel9, energy9 =parseError('TeaserCompareBilevel2D/TeaserCompareBilevel2D(9)/output.txt', 256*256)
    time10,lowLevel10,highLevel10,energy10=parseError('TeaserCompareBilevel2D/TeaserCompareBilevel2D(10)/output.txt',256*256)
    
    #plot low- and high-level error
    plt.ylabel('$\log[l(v_k)]$')
    plt.xlabel('#Iteration($\\times10^4$)')
    plt.plot ([i/100 for i,t in enumerate(time1)],[math.log(e) for e in energy1 ],label='$\\alpha_0=1\\times10^{-3}$')
    #plt.plot([i/100 for i,t in enumerate(time1)],[math.log(e) for e in energy2 ],label='$\\alpha_0=2\\times10^{-3}$')
    #plt.plot([i/100 for i,t in enumerate(time1)],[math.log(e) for e in energy3 ],label='$\\alpha_0=3\\times10^{-3}$')
    #plt.plot([i/100 for i,t in enumerate(time1)],[math.log(e) for e in energy4 ],label='$\\alpha_0=4\\times10^{-3}$')
    plt.plot ([i/100 for i,t in enumerate(time1)],[math.log(e) for e in energy5 ],label='$\\alpha_0=5\\times10^{-3}$')
    #plt.plot([i/100 for i,t in enumerate(time1)],[math.log(e) for e in energy6 ],label='$\\alpha_0=6\\times10^{-3}$')
    #plt.plot([i/100 for i,t in enumerate(time1)],[math.log(e) for e in energy7 ],label='$\\alpha_0=7\\times10^{-3}$')
    #plt.plot([i/100 for i,t in enumerate(time1)],[math.log(e) for e in energy8 ],label='$\\alpha_0=8\\times10^{-3}$')
    #plt.plot([i/100 for i,t in enumerate(time1)],[math.log(e) for e in energy9 ],label='$\\alpha_0=9\\times10^{-3}$')
    plt.plot ([i/100 for i,t in enumerate(time1)],[math.log(e) for e in energy10],label='$\\alpha_0=10\\times10^{-3}$')
    plt.grid()
    plt.legend()
    plt.yticks(np.arange(10,14,step=1))
    plt.axes().set_aspect(get_aspect(plt.axes())*0.5)
    plt.savefig('CompareAlpha.pdf',bbox_inches='tight')
    plt.show()
