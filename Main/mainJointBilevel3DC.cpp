#include <FBTO/Timing.cuh>
#include <FBTO/FBTO.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define FRANGE 3
#define RES 2

int main() {
  disableTiming();
  FBTOSolver<3,3> sol3(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.5,32*RES,32*RES,64*RES);
  sol3.setMask([&](Eigen::Matrix<int,3,1> id) {
    return (id[0]<FRANGE*RES || id[0]>sol3.x()-FRANGE*RES) && (id[1]<FRANGE*RES || id[1]>sol3.y()-FRANGE*RES) && id[2]==0;
  });
  sol3.tolX()=0;
  sol3.maxIter()=1e6;
  sol3.reportInterval()=100;
  sol3.setFilter(std::shared_ptr<Filter<3>>(new Filter<3>(3.5)));
  sol3.setWrite("JointBilevel3DC",100,0,sol3.minFill());
  sol3.solveBilevelGPU([&](Eigen::Matrix<int,3,1> id,int)->Vec3T {
    if(id[0]>=sol3.x()/2-FRANGE*RES && id[0]<=sol3.x()/2+FRANGE*RES)
      if(id[1]>=sol3.y()/2-FRANGE*RES && id[1]<=sol3.y()/2+FRANGE*RES)
        if(id[2]==sol3.z())
          return Vec3T(0,-1,0);
    return Vec3T::Zero();
  },[&](int)->Vec3T{
    return Vec3T::Zero();
  },1,1,0.05,0.5);
  return 0;
}
