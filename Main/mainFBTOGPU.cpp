#include <FBTO/FBTO.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define MAX_BLK 5
#define SOL_BLK 8

int main() {
  //2D
  for(int d=2; d<=MAX_BLK; d++) {
    FBTOSolver<2,3> sol2(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.5,d,d);
    sol2.debugBuildK();
    sol2.debugBuildKGPU();
    sol2.debugModifyFGPU();
    sol2.debugDiffKGPU(3);
    sol2.debugDiffK(3,true);
    sol2.debugDiffK(3,false);
    sol2.debugProjXGPU(NULL);
  }
  FBTOSolver<2,3> sol2(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.5,SOL_BLK,SOL_BLK);
  sol2.debugSolveKGPU();
  //3D
  for(int d=2; d<=MAX_BLK; d++) {
    FBTOSolver<3,3> sol3(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.5,d,d,d);
    sol3.debugBuildK();
    sol3.debugBuildKGPU();
    sol3.debugModifyFGPU();
    sol3.debugDiffKGPU(3);
    sol3.debugDiffK(3,true);
    sol3.debugDiffK(3,false);
    sol3.debugProjXGPU(NULL);
  }
  FBTOSolver<3,3> sol3(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.5,SOL_BLK,SOL_BLK,SOL_BLK);
  sol3.debugSolveKGPU();
  return 0;
}
