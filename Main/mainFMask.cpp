#include <FBTO/FBTO.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define BLKX 20
#define BLKY 30
#define BLKZ 40

int main() {
  {
    FBTOSolver<2,3> sol2(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.5,BLKX,BLKY);
    //init
    sol2.setFMask([&](const Eigen::Matrix<int,3,1> id) {
      return (id[0]>BLKX/2 && id[1]>BLKY/2);
    });
    Vec x=sol2.initX();
    sol2.writeVTK(0,NULL,&x,"initX2D.vtk");
    //filter
    sol2.setFilter(std::shared_ptr<Filter<2>>(new Filter<2>(3.5)));
    Vec xC=sol2.filterForward(x);
    sol2.writeVTK(0,NULL,&xC,"initX2DFiltered.vtk");
  }
  {
    FBTOSolver<3,3> sol3(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.5,BLKX,BLKY,BLKZ);
    //init
    sol3.setFMask([&](const Eigen::Matrix<int,3,1> id) {
      return (id[0]>BLKX/2 && id[1]>BLKY/2 && id[2]>BLKZ/2);
    });
    Vec x=sol3.initX();
    sol3.writeVTK(0,NULL,&x,"initX3D.vtk");
    //filter
    sol3.setFilter(std::shared_ptr<Filter<3>>(new Filter<3>(3.5)));
    Vec xC=sol3.filterForward(x);
    sol3.writeVTK(0,NULL,&xC,"initX3DFiltered.vtk");
  }
  return 0;
}
