#include <FBTO/HouseholderQR.h>
#include <FBTO/Utils.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T

int main() {
  MatT A=MatT::Random(100,10),A2=A;
  Vec b=Vec::Random(100),b2=b;
  solveQR(A,b);
  solveRef(A2,b2);
  std::cout << "QR=" << b.norm() << " QRErr=" << (b-b2).norm() << std::endl;
  return 0;
}
