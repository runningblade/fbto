#include <FBTO/Timing.cuh>
#include <FBTO/Filter.h>
#include <FBTO/FBTO.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define FRANGE 3
#define RES 2

int main() {
  disableTiming();
  FBTOSolver<2,3> sol2(lambda(1.0,0.49),mu(1.0,0.49),0.1,0.4,32*RES,64*RES);
  sol2.setMask([&](Eigen::Matrix<int,3,1> id) {
    return (id[0]<FRANGE*RES || id[0]>sol2.x()-FRANGE*RES) && id[1]==0;
  });
  sol2.maxIter()=10e4;
  sol2.reportInterval()=-100;
  sol2.setFilter(std::shared_ptr<Filter<2>>(new Filter<2>(3.5)));
  sol2.setWrite("ArmShort2D4DirSelfWeight",100,0);
  sol2.solveBilevelGPU([&](Eigen::Matrix<int,3,1> id,int iCase)->Vec2T {
    if(id[0]>=sol2.x()/2-FRANGE*RES && id[0]<=sol2.x()/2+FRANGE*RES)
      if(id[1]==sol2.y())
        return (iCase==0 ? Vec2T(1,0) : iCase==1 ? Vec2T(0,1) : iCase==2 ? Vec2T(1,1) : Vec2T(-1,1)).normalized();
    return Vec2T::Zero();
  },[&](int iCase)->Vec2T {
    return (iCase==0 ? Vec2T(1,0) : iCase==1 ? Vec2T(0,1) : iCase==2 ? Vec2T(1,1) : Vec2T(-1,1)).normalized()*10/(32*RES*32*RES);
  },4,1,0.005);
  return 0;
}
