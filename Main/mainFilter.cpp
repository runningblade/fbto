#include <FBTO/Filter.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define BLKX 15
#define BLKY 20
#define BLKZ 25

int main() {
  for(int it=0; it<1; it++) {
    {
      Filter<2> f(4.5);
      f.debug(BLKX,BLKY,1);
      f.debugGPU(BLKX,BLKY,1);

      std::shared_ptr<Eigen::Matrix<bool,-1,1>> fMask(new Eigen::Matrix<bool,-1,1>(BLKX*BLKY));
      for(int i=0; i<fMask->size(); i++)
        (*fMask)[i]=rand()<RAND_MAX/2;
      Eigen::Matrix<int,3,1> stride(BLKY,1,1);
      f.setFMask(BLKX,BLKY,1,stride,fMask);
      f.debugGPU(BLKX,BLKY,1);
    }
    {
      Filter<3> f(4.5);
      f.debug(BLKX,BLKY,BLKZ);
      f.debugGPU(BLKX,BLKY,BLKZ);

      std::shared_ptr<Eigen::Matrix<bool,-1,1>> fMask(new Eigen::Matrix<bool,-1,1>(BLKX*BLKY*BLKZ));
      for(int i=0; i<fMask->size(); i++)
        (*fMask)[i]=rand()<RAND_MAX/2;
      Eigen::Matrix<int,3,1> stride(BLKY*BLKZ,BLKZ,1);
      f.setFMask(BLKX,BLKY,BLKZ,stride,fMask);
      f.debugGPU(BLKX,BLKY,BLKZ);
    }
  }
  return 0;
}
