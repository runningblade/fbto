#include <FBTO/Timing.cuh>
#include <FBTO/Filter.h>
#include <FBTO/FBTO.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define FRANGET 16
#define FRANGE 1
#define RES 2

int main() {
  disableTiming();
  FBTOSolver<2,3> sol2(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.3,80*RES,100*RES);
  sol2.setMask([&](Eigen::Matrix<int,3,1> id) {
    return id[1]==0 && (id[0]>=sol2.x()/2-FRANGE*RES && id[0]<=sol2.x()/2+FRANGE*RES);
  });
  sol2.D()=30;
  sol2.maxIter()=5e4;
  sol2.reportInterval()=100;
  sol2.symmetry()[0]=true;
  sol2.setFilter(std::shared_ptr<Filter<2>>(new Filter<2>(3.5)));
  sol2.setWrite("D",100,0);
  sol2.solveBilevelGPU([&](Eigen::Matrix<int,3,1> id,int)->Vec2T {
    if(id[1]==sol2.y() && (id[0]>=sol2.x()/2-FRANGET*RES && id[0]<=sol2.x()/2+FRANGET*RES))
      return Vec2T(1,0)/std::sqrt(T(FRANGET*RES));
    return Vec2T::Zero();
  },[&](int)->Vec2T {
    return Vec2T::Zero();
  },1,1,0.001);
  return 0;
}
