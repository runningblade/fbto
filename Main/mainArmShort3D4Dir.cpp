#include <FBTO/Timing.cuh>
#include <FBTO/Filter.h>
#include <FBTO/FBTO.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define FRANGE 3
#define RES 2

int main() {
  disableTiming();
  FBTOSolver<3,3> sol3(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.3,32*RES,64*RES,32*RES);
  sol3.setMask([&](Eigen::Matrix<int,3,1> id) {
    return id[1]==0 && (id[0]<FRANGE*RES || id[0]>sol3.x()-FRANGE*RES) && (id[2]<FRANGE*RES || id[2]>sol3.z()-FRANGE*RES);
  });
  sol3.tolX()=0;
  sol3.maxIter()=5e4;
  sol3.reportInterval()=100;
  sol3.symmetry()[2]=true;
  sol3.setFilter(std::shared_ptr<Filter<3>>(new Filter<3>(3.5)));
  sol3.setWrite("ArmShort3D4Dir",100,0,sol3.minFill());
  sol3.solveBilevelGPU([&](Eigen::Matrix<int,3,1> id,int iCase)->Vec3T {
    if(id[0]<sol3.x()/10)
      if(id[1]==sol3.y())
        if(id[2]>=sol3.z()/2-FRANGE*RES && id[2]<=sol3.z()/2+FRANGE*RES)
          return (iCase==0 ? Vec3T(1,1,1) : iCase==1 ? Vec3T(-1,1,1) :
                  iCase==2 ? Vec3T(1,1,-1) : Vec3T(-1,1,-1)).normalized();
    return Vec3T::Zero();
  },[&](int)->Vec3T{
    return Vec3T::Zero();
  },4,1,0.05,0.5);
  return 0;
}
