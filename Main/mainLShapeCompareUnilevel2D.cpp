#include <FBTO/Timing.cuh>
#include <FBTO/Filter.h>
#include <FBTO/FBTO.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define BILEVEL
#define UNILEVEL
#define RES 4

int main() {
  disableTiming();
  FBTOSolver<2,4> sol2(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.5,80*RES,80*RES);
  sol2.setMask([&](Eigen::Matrix<int,3,1> id) {
    return id[1]==sol2.y();
  });
  sol2.setFMask([&](Eigen::Matrix<int,3,1> id) {
    return id[0]>=sol2.x()*0.4 && id[1]>=sol2.y()*0.4;
  });
  sol2.reportInterval()=1;
  sol2.setFilter(std::shared_ptr<Filter<2>>(new Filter<2>(3.5)));
  //bilevel
#ifdef BILEVEL
  sol2.maxIter()=5e3;
  sol2.setWrite("LShapeBilevel2D",10,0);
  Vec f1=sol2.solveBilevelGPU([&](Eigen::Matrix<int,3,1> id,int)->Vec2T {
    if(id[0]==sol2.x() && id[1]>=sol2.y()*0.18 && id[1]<=sol2.y()*0.22)
      return Vec2T(0,-1)/std::sqrt(T(sol2.y()*0.04));
    return Vec2T(0,0);
  },[&](int)->Vec2T {
    return Vec2T::Zero();
  },1,1,0.05);
  sol2.writeVTK(0,NULL,&f1,"LShapeBilevel2D/final.vtk");
#endif
  //unilevel
#ifdef UNILEVEL
  sol2.maxIter()=2e2;
  sol2.setWrite("LShapeUnilevel2D",10,0);
  Vec f2=sol2.solveUnilevelGPU([&](Eigen::Matrix<int,3,1> id,int)->Vec2T {
    if(id[0]==sol2.x() && id[1]>=sol2.y()*0.18 && id[1]<=sol2.y()*0.22)
      return Vec2T(0,-1)/std::sqrt(T(sol2.y()*0.04));
    return Vec2T(0,0);
  },[&](int)->Vec2T {
    return Vec2T::Zero();
  },1,0.05);
  sol2.writeVTK(0,NULL,&f2,"LShapeUnilevel2D/final.vtk");
#endif
  return 0;
}
