#include <FBTO/FBTO.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T

int main() {
  int blockSizes[]= {32,64,128,256,512,1024};
  for(auto blkSz: blockSizes) {
    FBTOSolver<2,3> sol2(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.5,blkSz,blkSz);
    sol2.setMask([&](Eigen::Matrix<int,3,1> id) {
      return id[1]==0 && (id[0]==sol2.x() || id[0]==0);
    });
    sol2.D()=-20;
    sol2.tol()=100;
    sol2.maxIter()=100;
    sol2.reportInterval()=1000;
    std::cout << "BlockSize=" << blkSz << std::endl;
    sol2.solveBilevel([&](Eigen::Matrix<int,3,1> id,int)->Vec2T {
#define FRANGE 3
      if(id[0]>=sol2.x()/2-FRANGE && id[0]<=sol2.x()/2+FRANGE)
        if(id[1]==sol2.y()-1)
          return Vec2T(0,-1);
      return Vec2T::Zero();
    },[&](int)->Vec2T{
      return Vec2T::Zero();
    },1,1,0.3);
    sol2.solveBilevelGPU([&](Eigen::Matrix<int,3,1> id,int)->Vec2T {
      if(id[0]>=sol2.x()/2-FRANGE && id[0]<=sol2.x()/2+FRANGE)
        if(id[1]==sol2.y()-1)
          return Vec2T(0,-1);
      return Vec2T::Zero();
    },[&](int)->Vec2T{
      return Vec2T::Zero();
    },1,1,0.3);
  }
  return 0;
}
