#include <FBTO/KnitroSolver.h>
#include <FBTO/Timing.cuh>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define FRANGE 3
#define SQP
#define FBTO

int main() {
  int RESs[]= {1,2};
  for(auto RES:RESs) {
#ifdef SQP
    {
      Vec xC;
      enableTiming();
      SQPSolver<2,3> sol2(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.3,32*RES,64*RES);
      sol2.sol().setMask([&](Eigen::Matrix<int,3,1> id) {
        return id[0]==0 && (id[1]>=sol2.sol().y()/2-FRANGE*RES && id[1]<=sol2.sol().y()/2+FRANGE*RES);
      });
      sol2.sol().maxIter()=5e4;
      sol2.sol().reportInterval()=100;
      sol2.sol().symmetry()[1]=true;
      sol2.sol().setFilter(std::shared_ptr<Filter<2>>(new Filter<2>(3.5)));
      sol2.sol().setWrite("BridgeSQPBilevel2DB("+std::to_string(RES)+")",100,0);
      sol2.solve([&](Eigen::Matrix<int,3,1> id)->Vec2T {
        if(id[0]==sol2.sol().x()) {
          int segmentLen=sol2.sol().x()/10;
          int segmentOff=id[1]%segmentLen;
          if((segmentOff-segmentLen/2)<segmentLen/8)
            return Vec2T(1,0)/std::sqrt(T(64*RES/4));
        }
        return Vec2T::Zero();
      },xC);
      sol2.sol().writeVTK(0,NULL,&xC,"BridgeSQPBilevel2DB("+std::to_string(RES)+")/final.vtk");
    }
#endif
#ifdef FBTO
    {
      Vec xC;
      disableTiming();
      FBTOSolver<2,3> sol2(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.3,32*RES,64*RES);
      sol2.setMask([&](Eigen::Matrix<int,3,1> id) {
        return id[0]==0 && (id[1]>=sol2.y()/2-FRANGE*RES && id[1]<=sol2.y()/2+FRANGE*RES);
      });
      sol2.tol()=0;
      sol2.maxIter()=5e4;
      sol2.reportInterval()=-1000;
      sol2.symmetry()[1]=true;
      sol2.setFilter(std::shared_ptr<Filter<2>>(new Filter<2>(3.5)));
      sol2.setWrite("BridgeFBTOBilevel2DB("+std::to_string(RES)+")",sol2.maxIter()+1,0);
      xC=sol2.solveBilevelGPU([&](Eigen::Matrix<int,3,1> id,int)->Vec2T {
        if(id[0]==sol2.x()) {
          int segmentLen=sol2.x()/10;
          int segmentOff=id[1]%segmentLen;
          if((segmentOff-segmentLen/2)<segmentLen/8)
            return Vec2T(1,0)/std::sqrt(T(64*RES/4));
        }
        return Vec2T::Zero();
      },[&](int)->Vec2T {
        return Vec2T::Zero();
      },1,1,0.25);
      sol2.writeVTK(0,NULL,&xC,"BridgeFBTOBilevel2DB("+std::to_string(RES)+")/final.vtk");
    }
#endif
  }
  return 0;
}
