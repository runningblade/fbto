#include <FBTO/FBTOEnergy.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T

int main() {
  FBTOEnergy<2> e2(1000,1000,3);
  FBTOEnergy<3> e3(1000,1000,3);
  std::cout << Eigen::SelfAdjointEigenSolver<MatT>(e2.Hessian()).eigenvalues().transpose() << std::endl;
  std::cout << Eigen::SelfAdjointEigenSolver<MatT>(e3.Hessian()).eigenvalues().transpose() << std::endl;
  return 0;
}
