#include <FBTO/ALMSolver.h>
#include <FBTO/Timing.cuh>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define FRANGE 3
//#define DEBUG_EVAL
int main() {
  int RESs[]= {1};
  for(auto RES:RESs) {
    Vec u,xC;
    enableTiming();
    ALMSolver<2,3> sol2(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.3,32*RES,64*RES);
    sol2.sol().setMask([&](Eigen::Matrix<int,3,1> id) {
      return id[0]==0 && (id[1]>=sol2.sol().y()/2-FRANGE*RES && id[1]<=sol2.sol().y()/2+FRANGE*RES);
    });
    sol2.sol().tol()=1e-3;
    sol2.sol().maxIter()=5e6;
    sol2.sol().reportInterval()=1000;
    sol2.sol().symmetry()[1]=true;
    sol2.sol().setFilter(std::shared_ptr<Filter<2>>(new Filter<2>(3.5)));
#ifdef DEBUG_EVAL
    sol2.debugEval([&](Eigen::Matrix<int,3,1> id)->Vec2T {
      if(id[0]==sol2.sol().x()) {
        int segmentLen=sol2.sol().x()/10;
        int segmentOff=id[1]%segmentLen;
        if((segmentOff-segmentLen/2)<segmentLen/8)
          return Vec2T(1,0)/std::sqrt(T(64*RES/4));
      }
      return Vec2T::Zero();
    });
#endif
    sol2.sol().setWrite("BridgeALMBilevel2DB("+std::to_string(RES)+")",1000,0);
    sol2.solve([&](Eigen::Matrix<int,3,1> id)->Vec2T {
      if(id[0]==sol2.sol().x()) {
        int segmentLen=sol2.sol().x()/10;
        int segmentOff=id[1]%segmentLen;
        if((segmentOff-segmentLen/2)<segmentLen/8)
          return Vec2T(1,0)/std::sqrt(T(64*RES/4));
      }
      return Vec2T::Zero();
    },u,xC);
    sol2.sol().writeVTK(0,NULL,&xC,"BridgeSQPBilevel2DB("+std::to_string(RES)+")/final.vtk");
  }
  return 0;
}
