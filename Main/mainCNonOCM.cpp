#include <FBTO/Timing.cuh>
#include <FBTO/Filter.h>
#include <FBTO/FBTO.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define FRANGE 3
#define RES 6

int main() {
  disableTiming();
  FBTOSolver<2,3> sol2(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.4,32*RES,64*RES);
  sol2.setMask([&](Eigen::Matrix<int,3,1> id) {
    return id[0]==0 && (id[1]>=sol2.y()/2-FRANGE*RES && id[1]<=sol2.y()/2+FRANGE*RES);
  });
  sol2.tolX()=1e-6;
  sol2.maxIter()=1.1e4;
  sol2.reportInterval()=-100;
  sol2.symmetry()[1]=true;
  sol2.setFilter(std::shared_ptr<Filter<2>>(new Filter<2>(3.5)));
  sol2.setWrite("CNonOCM",100,0);
  sol2.solveBilevelGPU([&](Eigen::Matrix<int,3,1> id,int)->Vec2T {
    if(id[0]==sol2.x())
      if(abs(id[1]-sol2.y()/2)<sol2.y()/2)
        return Vec2T(1,0)/std::sqrt(T(64*RES/4));
    return Vec2T::Zero();
  },[&](int)->Vec2T {
    return Vec2T::Zero();
  },1,1,0.25);
  return 0;
}
