#include <FBTO/Timing.cuh>
#include <FBTO/Filter.h>
#include <FBTO/FBTO.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define FRANGE 3
#define RES 1

int main() {
  disableTiming();
  FBTOSolver<3,3> sol3(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.5,32*RES,96*RES,32*RES);
  sol3.setMask([&](Eigen::Matrix<int,3,1> id) {
    return id[0]==0 && (id[1]<FRANGE*RES || id[1]>sol3.y()-FRANGE*RES);
  });
  sol3.tolX()=0;
  sol3.maxIter()=5e4;
  sol3.reportInterval()=100;
  sol3.setFilter(std::shared_ptr<Filter<3>>(new Filter<3>(7.0/4*RES)));
  sol3.setWrite("BridgeBilevel3DA",100,0,sol3.minFill());
  sol3.solveBilevelGPU([&](Eigen::Matrix<int,3,1> id,int)->Vec3T {
    if(id[0]==0 &&
        id[1]>=sol3.y()/2-FRANGE*RES && id[1]<=sol3.y()/2+FRANGE*RES &&
        id[2]>=sol3.z()/2-FRANGE*RES && id[2]<=sol3.z()/2+FRANGE*RES)
      return Vec3T(1,0,0);
    return Vec3T::Zero();
  },[&](int)->Vec3T{
    return Vec3T::Zero();
  },1,1,0.25);
  return 0;
}
