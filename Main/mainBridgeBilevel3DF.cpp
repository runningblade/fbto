#include <FBTO/Timing.cuh>
#include <FBTO/Filter.h>
#include <FBTO/FBTO.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define FRANGE 4
#define RES 2
#define BD 2*RES

int main() {
  disableTiming();
  FBTOSolver<3,3> sol3(lambda(1.0,0.25),mu(1.0,0.25),0.2,0.4,32*RES,96*RES,32*RES);
  sol3.setMask([&](Eigen::Matrix<int,3,1> id) {
    return id[0]==0 &&
           ((id[1]>=BD && id[1]<FRANGE*RES) || (id[1]<sol3.y()-BD && id[1]>sol3.y()-FRANGE*RES)) &&
           (id[2]>=BD && id[2]<sol3.z()-BD);
  });
  sol3.tolX()=0;
  sol3.maxIter()=5e4;
  sol3.reportInterval()=100;
  sol3.symmetry()[1]=true;
  sol3.setFilter(std::shared_ptr<Filter<3>>(new Filter<3>(7/2*RES)));
  sol3.setWrite("BridgeBilevel3DF",100,0,sol3.minFill());
  sol3.solveBilevelGPU([&](Eigen::Matrix<int,3,1> id,int)->Vec3T {
    if(id[0]>=sol3.x())
      if(id[1]>=BD && id[1]<sol3.y()-BD)
        if(id[2]>=BD && id[2]<sol3.z()-BD)
          return Vec3T(1,0,0)/std::sqrt(T(sol3.x()));
    return Vec3T::Zero();
  },[&](int)->Vec3T{
    return Vec3T::Zero();
  },1,1,0.025,0.5);
  return 0;
}
