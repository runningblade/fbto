#include <FBTO/Timing.cuh>
#include <FBTO/Filter.h>
#include <FBTO/FBTO.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define FRANGE 3
#define RES 4

int main() {
  disableTiming();
  FBTOSolver<2,4> sol2(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.5,32*RES,64*RES);
  sol2.setMask([&](Eigen::Matrix<int,3,1> id) {
    return id[1]==0;
  });
  sol2.tolX()=1e-6;
  sol2.maxIter()=10e4;
  sol2.reportInterval()=100;
  sol2.setFilter(std::shared_ptr<Filter<2>>(new Filter<2>(7.0)));
  sol2.setWrite("G",100,0);
  sol2.solveBilevelGPU([&](Eigen::Matrix<int,3,1> id,int)->Vec2T {
    if(id[0]<sol2.x()/10)
      if(id[1]==sol2.y())
        return Vec2T(1,0);
    return Vec2T::Zero();
  },[&](int)->Vec2T {
    return Vec2T::Zero();
  },1,1,0.05);
  return 0;
}
