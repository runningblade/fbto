#include <FBTO/Timing.cuh>
#include <FBTO/Filter.h>
#include <FBTO/FBTO.h>
#include <iostream>

using namespace FBTO;
typedef SCALAR_TYPE T;
DECL_MAT_VEC_MAP_TYPES_T
#define FRANGE 3
#define RES 2

int main() {
  disableTiming();
  FBTOSolver<2,3> sol2(lambda(1.0,0.25),mu(1.0,0.25),0.1,0.3,32*RES,64*RES);
  sol2.setMask([&](Eigen::Matrix<int,3,1> id) {
    return id[1]==0;
  });
  sol2.maxIter()=2e4;
  sol2.reportInterval()=-1000;
  sol2.setFilter(std::shared_ptr<Filter<2>>(new Filter<2>(7.0)));
  T alphas[]= {0.04,0.08,0.16,0.32,0.64};
  for(int D=5; D<=20; D+=5) {
    for(auto alpha:alphas) {
      sol2.D()=D;
      sol2.setWrite("CantileverBilevel2D("+std::to_string(D)+")("+std::to_string(alpha)+")",1000,0);
      sol2.solveBilevelGPU([&](Eigen::Matrix<int,3,1> id,int)->Vec2T {
        if(id[0]<sol2.x()/10)
          if(id[1]==sol2.y())
            return Vec2T(1,0);
        return Vec2T::Zero();
      },[&](int)->Vec2T {
        return Vec2T::Zero();
      },1,1,alpha);
    }
  }
  return 0;
}
