# Cholmod lib usually requires linking to a blas and lapack library.
# It is up to the user of this module to find a BLAS and link to it.

include(FindPackageHandleStandardArgs)

find_path(CHOLMOD_INCLUDES
	NAMES
	cholmod.h
	PATHS
		$ENV{CHOLMODDIR}
		${INCLUDE_INSTALL_DIR}
		/usr/include/suitesparse
		D:/libs/vcpkg/packages/suitesparse_x64-windows/include/suitesparse
		PATH_SUFFIXES
		suitesparse
		ufsparse
)

if (WIN32)
find_library(CHOLMOD_LIBRARIES 
	libcholmod.lib
	PATHS 
		$ENV{CHOLMODDIR} 
		${LIB_INSTALL_DIR}
		/usr/lib/x86_64-linux-gnu
		D:/libs/vcpkg/packages/suitesparse_x64-windows/lib)
else()
find_library(CHOLMOD_LIBRARIES 
	cholmod 
	PATHS 
		$ENV{CHOLMODDIR} 
		${LIB_INSTALL_DIR}
		/usr/lib/x86_64-linux-gnu
		D:/libs/vcpkg/packages/suitesparse_x64-windows/lib)
endif()		

#message(${CHOLMOD_INCLUDES})
#message(${CHOLMOD_LIBRARIES})

find_package_handle_standard_args(CHOLMOD DEFAULT_MSG
                                  CHOLMOD_INCLUDES CHOLMOD_LIBRARIES)

#message(${CHOLMOD_FOUND})

#if (CHOLMOD_FOUND)
#	set(CHOLMOD_LIBRARIES ${CHOLMOD_LIBRARY})
#	set(CHOLMOD_INCLUDE_DIRS ${CHOLMOD_INCLUDE_DIR})
#	message("find cholmod")
#endif()

mark_as_advanced(CHOLMOD_INCLUDES CHOLMOD_LIBRARIES)# AMD_LIBRARY COLAMD_LIBRARY SUITESPARSE_LIBRARY CAMD_LIBRARY CCOLAMD_LIBRARY CHOLMOD_METIS_LIBRARY)
